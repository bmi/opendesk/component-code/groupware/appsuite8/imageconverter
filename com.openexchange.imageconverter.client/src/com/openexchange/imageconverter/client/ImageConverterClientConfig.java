/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.client;

import static com.openexchange.imageconverter.client.ImageConverterClient.LOG;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import com.openexchange.annotation.NonNull;
import com.openexchange.annotation.Nullable;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.Interests;
import com.openexchange.config.Reloadables;

/**
 * {@link ImageConverterClientConfig}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public final class ImageConverterClientConfig {

    final private static String REMOTE_TEST_URL = "file:///ImageConverterClient_TestURL";

    final private static String IMAGECONVERTER_CLIENT_TEMP_DIRNAME = "oxiccli";

    /**
     * Config items
     */

    final private static String CONFIGITEM_REMOTE_IMAGECONVERTER_URL = "com.openexchange.imageconverter.client.remoteImageConverterUrl";
    final private static String CONFIGITEM_SPOOL_PATH = "com.openexchange.imageconverter.client.spoolPath";


    /**
     * All config items
     */
    final private static String[] ALL_CONFIGITEMS = {
        CONFIGITEM_REMOTE_IMAGECONVERTER_URL,
        CONFIGITEM_SPOOL_PATH
    };

    /**
     * Reloadable config items
     */
    final private static String[] RELOADABLE_CONFIGITEMS = {
        CONFIGITEM_REMOTE_IMAGECONVERTER_URL
    };


    /**
     * Initializes a new {@link ImageConverterClientConfig}.
     */
    @SuppressWarnings("unused")
    private ImageConverterClientConfig() {
        // not to be used
    }

    /**
     * Initializes a new {@link ImageConverterClientConfig}.
     * @param configService
     */
    public ImageConverterClientConfig(@NonNull final ConfigurationService configService) {
        super();

        implReadConfig(configService);
    }

    /**
     *
     */
    public void shutdown() {
        if (m_running.compareAndSet(true, false)) {
            FileUtils.deleteQuietly(m_spoolDir);
        }
    }

    /**
     * @return
     */
    public boolean isValid() {
        return (null != m_imageConverterServerURL);
    }

    /**
     * @return
     */
    @Nullable public URL getImageConverterServerURL() {
        return m_imageConverterServerURL;
    }

    /**
     * @return
     */
    @NonNull public File getSpoolDir() {
        return (m_running.get() && (null != m_spoolDir) ? m_spoolDir : FileUtils.getTempDirectory());
    }

    /* (non-Javadoc)
     * @see com.openexchange.config.Reloadable#reloadConfiguration(com.openexchange.config.ConfigurationService)
     */
    public void reloadConfiguration(ConfigurationService configService) {
        LOG.info("IC client is reloading config item(s): {}", Arrays.toString(RELOADABLE_CONFIGITEMS));

        implReadConfig(configService, RELOADABLE_CONFIGITEMS);
    }

    /* (non-Javadoc)
     * @see com.openexchange.config.Reloadable#getInterests()
     */
    public Interests getInterests() {
        return Reloadables.interestsForProperties(RELOADABLE_CONFIGITEMS);
    }

    // - Implementation --------------------------------------------------------

    /**
     *
     */
    private synchronized void implReadConfig(final ConfigurationService configService, final String... configPropertyNames) {
        final Set<String> itemsToRead = new HashSet<>();
        String curValue = null;

        itemsToRead.addAll(
            Arrays.asList(ArrayUtils.isEmpty(configPropertyNames) ?
                ALL_CONFIGITEMS :
                    configPropertyNames));

        if (itemsToRead.contains(CONFIGITEM_REMOTE_IMAGECONVERTER_URL)) {
            if (isNotEmpty(curValue= configService.getProperty(CONFIGITEM_REMOTE_IMAGECONVERTER_URL))) {
                try {
                    m_imageConverterServerURL = new URL(curValue);
                } catch (@SuppressWarnings("unused") Exception e) {
                    m_imageConverterServerURL = null;
                }
            }

            if ((null == m_imageConverterServerURL) || (m_imageConverterServerURL.toString().length() < 1)) {
                LOG.error("IC client remoteImageConverterUrl property is empty or not set at all. Client is not able to perform conversions until a valid Url is set! {}", m_imageConverterServerURL);
            }
        }

        if (itemsToRead.contains(CONFIGITEM_SPOOL_PATH)) {
            final File configSpoolDir = new File(configService.getProperty(CONFIGITEM_SPOOL_PATH, "/tmp"));

            if (null != configSpoolDir) {
                File targetDir = new File(configSpoolDir, IMAGECONVERTER_CLIENT_TEMP_DIRNAME);

                // use or create given spool directory, use /tmp as fallback parent directory
                if ((targetDir.exists() && targetDir.canWrite() && targetDir.isDirectory()) ||
                    targetDir.mkdirs() || (targetDir = new File("/tmp", IMAGECONVERTER_CLIENT_TEMP_DIRNAME)).mkdirs()) {

                    try {
                        FileUtils.cleanDirectory(m_spoolDir = targetDir);
                    } catch (@SuppressWarnings("unused") IOException e) {
                        LOG.error("IC client is not able to delete spool directory when shut down: {}", m_spoolDir.toString());
                    }
                }
             }
        }
    }

    // - Members ---------------------------------------------------------------

    final private AtomicBoolean m_running = new AtomicBoolean(true);

    private URL m_imageConverterServerURL = null;

    private File m_spoolDir = new File("/var/spool/open-xchange/imageconverter-client");
}
