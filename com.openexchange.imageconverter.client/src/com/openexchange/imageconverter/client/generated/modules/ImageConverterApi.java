/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.client.generated.modules;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.GenericType;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.imageconverter.client.generated.invoker.ApiClient;
import com.openexchange.imageconverter.client.generated.invoker.ApiException;
import com.openexchange.imageconverter.client.generated.invoker.ApiResponse;
import com.openexchange.imageconverter.client.generated.invoker.Configuration;
import com.openexchange.imageconverter.client.generated.invoker.Pair;


public class ImageConverterApi {
  private ApiClient apiClient;

  public ImageConverterApi() {
    this(Configuration.getDefaultApiClient());
  }

  public ImageConverterApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * cacheAndGetImage
   * Creating a new sequence of preview images for a source image under a certain image key, if not available, and/or retrieving the image with the given image key and best matching request format.
   * @param imageKey The key of the image group. (required)
   * @param requestFormat The image format for which a best matching image is to be returned. (required)
   * @param sourceImage The source image file for which the preview images are to be created. (required)
   * @param context The context of the image group, if specified. (optional, default to )
   * @return File
   * @throws ApiException if fails to make API call
   */
  public File cacheAndGetImage(String imageKey, String requestFormat, File sourceImage, String context) throws ApiException {
    return cacheAndGetImageWithHttpInfo(imageKey, requestFormat, sourceImage, context).getData();
      }

  /**
   * cacheAndGetImage
   * Creating a new sequence of preview images for a source image under a certain image key, if not available, and/or retrieving the image with the given image key and best matching request format.
   * @param imageKey The key of the image group. (required)
   * @param requestFormat The image format for which a best matching image is to be returned. (required)
   * @param sourceImage The source image file for which the preview images are to be created. (required)
   * @param context The context of the image group, if specified. (optional, default to )
   * @return ApiResponse&lt;File&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<File> cacheAndGetImageWithHttpInfo(String imageKey, String requestFormat, File sourceImage, String context) throws ApiException {
    Object localVarPostBody = null;

    // verify the required parameter 'imageKey' is set
    if (imageKey == null) {
      throw new ApiException(400, "Missing the required parameter 'imageKey' when calling cacheAndGetImage");
    }

    // verify the required parameter 'requestFormat' is set
    if (requestFormat == null) {
      throw new ApiException(400, "Missing the required parameter 'requestFormat' when calling cacheAndGetImage");
    }

    // verify the required parameter 'sourceImage' is set
    if (sourceImage == null) {
      throw new ApiException(400, "Missing the required parameter 'sourceImage' when calling cacheAndGetImage");
    }

    // create path and map variables
    String localVarPath = "/cacheAndGetImage/{imageKey}/{requestFormat}"
      .replaceAll("\\{" + "imageKey" + "\\}", apiClient.escapeString(imageKey.toString()))
      .replaceAll("\\{" + "requestFormat" + "\\}", apiClient.escapeString(requestFormat.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<>();
    Map<String, String> localVarHeaderParams = new HashMap<>();
    Map<String, Object> localVarFormParams = new HashMap<>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "context", context));


    if (sourceImage != null)
      localVarFormParams.put("sourceImage", sourceImage);

    final String[] localVarAccepts = {
      "multipart/form-data"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "multipart/form-data"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<File> localVarReturnType = new GenericType<>() {};
    return apiClient.invokeAPI(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
  }
  /**
   * cacheAndGetImageAndMetadata
   * Creating a new sequence of preview images for a source image under a certain image key, if not available, and/or retrieving the image with the given image key and best matching request format as image/_* MultipartEntity as well as the image metadata as application/json Multipart entity within the Multipart/form-data response
   * @param imageKey The key of the image group. (required)
   * @param requestFormat The image format for which a best matching image is to be returned. (required)
   * @param sourceImage The source image file for which the preview images are to be created. (required)
   * @param context The context of the image group, if specified. (optional, default to )
   * @return File
   * @throws ApiException if fails to make API call
   */
  public File cacheAndGetImageAndMetadata(String imageKey, String requestFormat, File sourceImage, String context) throws ApiException {
    return cacheAndGetImageAndMetadataWithHttpInfo(imageKey, requestFormat, sourceImage, context).getData();
  }

  /**
   * cacheAndGetImageAndMetadata
   * Creating a new sequence of preview images for a source image under a certain image key, if not available, and/or retrieving the image with the given image key and best matching request format as image/_* MultipartEntity as well as the image metadata as application/json Multipart entity within the Multipart/form-data response
   * @param imageKey The key of the image group. (required)
   * @param requestFormat The image format for which a best matching image is to be returned. (required)
   * @param sourceImage The source image file for which the preview images are to be created. (required)
   * @param context The context of the image group, if specified. (optional, default to )
   * @return ApiResponse&lt;File&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<File> cacheAndGetImageAndMetadataWithHttpInfo(String imageKey, String requestFormat, File sourceImage, String context) throws ApiException {
    Object localVarPostBody = null;

    // verify the required parameter 'imageKey' is set
    if (imageKey == null) {
      throw new ApiException(400, "Missing the required parameter 'imageKey' when calling cacheAndGetImageAndMetadata");
    }

    // verify the required parameter 'requestFormat' is set
    if (requestFormat == null) {
      throw new ApiException(400, "Missing the required parameter 'requestFormat' when calling cacheAndGetImageAndMetadata");
    }

    // verify the required parameter 'sourceImage' is set
    if (sourceImage == null) {
      throw new ApiException(400, "Missing the required parameter 'sourceImage' when calling cacheAndGetImageAndMetadata");
    }

    // create path and map variables
    String localVarPath = "/cacheAndGetImageAndMetadata/{imageKey}/{requestFormat}"
      .replaceAll("\\{" + "imageKey" + "\\}", apiClient.escapeString(imageKey.toString()))
      .replaceAll("\\{" + "requestFormat" + "\\}", apiClient.escapeString(requestFormat.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<>();
    Map<String, String> localVarHeaderParams = new HashMap<>();
    Map<String, Object> localVarFormParams = new HashMap<>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "context", context));


    if (sourceImage != null)
      localVarFormParams.put("sourceImage", sourceImage);

    final String[] localVarAccepts = {
      "multipart/form-data"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "multipart/form-data"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<File> localVarReturnType = new GenericType<>() {};
    return apiClient.invokeAPI(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
  }
  /**
   * cacheImage
   * Creating a new sequence of preview images for a source image under a certain image key.
   * @param imageKey The key of the image group. (required)
   * @param sourceImage The source image file for which the preview images are to be created. (required)
   * @param context The context of the image group, if specified. (optional, default to )
   * @return ResponseGeneral
   * @throws ApiException if fails to make API call
   */
  public JSONObject cacheImage(String imageKey, File sourceImage, String context) throws ApiException {
    try {
        return new JSONObject(cacheImageWithHttpInfo(imageKey, sourceImage, context).getData());
    } catch (JSONException | ApiException e) {
        throw new ApiException(e);
    }
  }

  /**
   * cacheImage
   * Creating a new sequence of preview images for a source image under a certain image key.
   * @param imageKey The key of the image group. (required)
   * @param sourceImage The source image file for which the preview images are to be created. (required)
   * @param context The context of the image group, if specified. (optional, default to )
   * @return ApiResponse&lt;ResponseGeneral&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> cacheImageWithHttpInfo(String imageKey, File sourceImage, String context) throws ApiException {
    Object localVarPostBody = null;

    // verify the required parameter 'imageKey' is set
    if (imageKey == null) {
      throw new ApiException(400, "Missing the required parameter 'imageKey' when calling cacheImage");
    }

    // verify the required parameter 'sourceImage' is set
    if (sourceImage == null) {
      throw new ApiException(400, "Missing the required parameter 'sourceImage' when calling cacheImage");
    }

    // create path and map variables
    String localVarPath = "/cacheImage/{imageKey}"
      .replaceAll("\\{" + "imageKey" + "\\}", apiClient.escapeString(imageKey.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<>();
    Map<String, String> localVarHeaderParams = new HashMap<>();
    Map<String, Object> localVarFormParams = new HashMap<>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "context", context));


    if (sourceImage != null)
      localVarFormParams.put("sourceImage", sourceImage);

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "multipart/form-data"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<String> localVarReturnType = new GenericType<>() {};
    return apiClient.invokeAPI(localVarPath, "PUT", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
  }
  /**
   * clearImages
   * Removing all images from the Image Server. This call leaves an Image Server with no images left after a succesful request!
   * @param context The context of the image group, if specified. (optional, default to )
   * @return ResponseGeneral
   * @throws ApiException if fails to make API call
   */
  public JSONObject clearImages(String context) throws ApiException {
    try {
        return new JSONObject(clearImagesWithHttpInfo(context).getData());
    } catch (JSONException | ApiException e) {
        throw new ApiException(e);
    }
  }

  /**
   * clearImages
   * Removing all images from the Image Server. This call leaves an Image Server with no images left after a succesful request!
   * @param context The context of the image group, if specified. (optional, default to )
   * @return ApiResponse&lt;ResponseGeneral&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> clearImagesWithHttpInfo(String context) throws ApiException {
    Object localVarPostBody = null;

    // create path and map variables
    String localVarPath = "/clearImages";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<>();
    Map<String, String> localVarHeaderParams = new HashMap<>();
    Map<String, Object> localVarFormParams = new HashMap<>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "context", context));



    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {

    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "basicAuth" };

    GenericType<String> localVarReturnType = new GenericType<>() {};
    return apiClient.invokeAPI(localVarPath, "DELETE", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * clearImagesByKey
   * Removing all images matching the specified key from the Image Server.
   * @param imageKey The key of the image group. (required)
   * @return ResponseGeneral
   * @throws ApiException if fails to make API call
   */
  public JSONObject clearImagesByKey(String imageKey) throws ApiException {
    try {
        return new JSONObject(clearImagesByKeyWithHttpInfo(imageKey).getData());
    } catch (JSONException | ApiException e) {
        throw new ApiException(e);
    }
  }

  /**
   * clearImagesByKey
   * Removing all images matching the specified key from the Image Server.
   * @param imageKey The key of the image group. (required)
   * @return ApiResponse&lt;ResponseGeneral&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> clearImagesByKeyWithHttpInfo(String imageKey) throws ApiException {
    Object localVarPostBody = null;

    // verify the required parameter 'imageKey' is set
    if (imageKey == null) {
      throw new ApiException(400, "Missing the required parameter 'imageKey' when calling clearImagesByKey");
    }

    // create path and map variables
    String localVarPath = "/clearImagesByKey/{imageKey}"
      .replaceAll("\\{" + "imageKey" + "\\}", apiClient.escapeString(imageKey.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<>();
    Map<String, String> localVarHeaderParams = new HashMap<>();
    Map<String, Object> localVarFormParams = new HashMap<>();




    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {

    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "basicAuth" };

    GenericType<String> localVarReturnType = new GenericType<>() {};
    return apiClient.invokeAPI(localVarPath, "DELETE", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * getImage
   * Retrieving the image with the given image key and best matching request format.
   * @param imageKey The key of the image group. (required)
   * @param requestFormat The image format for which a best matching image is to be returned. (required)
   * @param context The context of the image group, if specified. (optional, default to )
   * @return File
   * @throws ApiException if fails to make API call
   */
  public File getImage(String imageKey, String requestFormat, String context) throws ApiException {
    return getImageWithHttpInfo(imageKey, requestFormat, context).getData();
      }

  /**
   * getImage
   * Retrieving the image with the given image key and best matching request format.
   * @param imageKey The key of the image group. (required)
   * @param requestFormat The image format for which a best matching image is to be returned. (required)
   * @param context The context of the image group, if specified. (optional, default to )
   * @return ApiResponse&lt;File&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<File> getImageWithHttpInfo(String imageKey, String requestFormat, String context) throws ApiException {
    Object localVarPostBody = null;

    // verify the required parameter 'imageKey' is set
    if (imageKey == null) {
      throw new ApiException(400, "Missing the required parameter 'imageKey' when calling getImage");
    }

    // verify the required parameter 'requestFormat' is set
    if (requestFormat == null) {
      throw new ApiException(400, "Missing the required parameter 'requestFormat' when calling getImage");
    }

    // create path and map variables
    String localVarPath = "/getImage/{imageKey}/{requestFormat}"
      .replaceAll("\\{" + "imageKey" + "\\}", apiClient.escapeString(imageKey.toString()))
      .replaceAll("\\{" + "requestFormat" + "\\}", apiClient.escapeString(requestFormat.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<>();
    Map<String, String> localVarHeaderParams = new HashMap<>();
    Map<String, Object> localVarFormParams = new HashMap<>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "context", context));



    final String[] localVarAccepts = {
      "multipart/form-data"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {

    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<File> localVarReturnType = new GenericType<>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * getImageAndMetadata
   * Retrieving the image with the given image key and best matching request format as image/_* MultipartEntity as well as the image metadata as application/json Multipart entity within the mMultipart/form-data response
   * @param imageKey The key of the image group. (required)
   * @param requestFormat The image format for which a best matching image is to be returned. (required)
   * @param context The context of the image group, if specified. (optional, default to )
   * @return File
   * @throws ApiException if fails to make API call
   */
  public File getImageAndMetadata(String imageKey, String requestFormat, String context) throws ApiException {
    return getImageAndMetadataWithHttpInfo(imageKey, requestFormat, context).getData();
  }

  /**
   * getImageAndMetadata
   * Retrieving the image with the given image key and best matching request format as image/_* MultipartEntity as well as the image metadata as application/json Multipart entity within the mMultipart/form-data response
   * @param imageKey The key of the image group. (required)
   * @param requestFormat The image format for which a best matching image is to be returned. (required)
   * @param context The context of the image group, if specified. (optional, default to )
   * @return ApiResponse&lt;File&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<File> getImageAndMetadataWithHttpInfo(String imageKey, String requestFormat, String context) throws ApiException {
    Object localVarPostBody = null;

    // verify the required parameter 'imageKey' is set
    if (imageKey == null) {
      throw new ApiException(400, "Missing the required parameter 'imageKey' when calling getImageAndMetadata");
    }

    // verify the required parameter 'requestFormat' is set
    if (requestFormat == null) {
      throw new ApiException(400, "Missing the required parameter 'requestFormat' when calling getImageAndMetadata");
    }

    // create path and map variables
    String localVarPath = "/getImageAndMetadata/{imageKey}/{requestFormat}"
      .replaceAll("\\{" + "imageKey" + "\\}", apiClient.escapeString(imageKey.toString()))
      .replaceAll("\\{" + "requestFormat" + "\\}", apiClient.escapeString(requestFormat.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<>();
    Map<String, String> localVarHeaderParams = new HashMap<>();
    Map<String, Object> localVarFormParams = new HashMap<>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "context", context));



    final String[] localVarAccepts = {
      "multipart/form-data"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {

    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<File> localVarReturnType = new GenericType<>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
  }
  /**
   * getKeyCount
   * Retrieving either the number of all keys or the number of keys that match the given context from the Image Server.
   * @param context The context of the image group, if specified. (optional, default to )
   * @return KeyCount
   * @throws ApiException if fails to make API call
   */
  public JSONObject getKeyCount(String context) throws ApiException {
    try {
        return new JSONObject(getKeyCountWithHttpInfo(context).getData());
    } catch (ApiException | JSONException e) {
        throw new ApiException(e);
    }
  }

  /**
   * getKeyCount
   * Retrieving either the number of all keys or the number of keys that match the given context from the Image Server.
   * @param context The context of the image group, if specified. (optional, default to )
   * @return ApiResponse&lt;KeyCount&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getKeyCountWithHttpInfo(String context) throws ApiException {
    Object localVarPostBody = null;

    // create path and map variables
    String localVarPath = "/getKeyCount";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<>();
    Map<String, String> localVarHeaderParams = new HashMap<>();
    Map<String, Object> localVarFormParams = new HashMap<>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "context", context));



    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {

    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<String> localVarReturnType = new GenericType<>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * getKeys
   * Retrieving either all keys or the keys that match the given context from the Image Server.
   * @param context The context of the image group, if specified. (optional, default to )
   * @return Keys
   * @throws ApiException if fails to make API call
   */
  public JSONObject getKeys(String context) throws ApiException {
    try {
        return new JSONObject(getKeysWithHttpInfo(context).getData());
    } catch (JSONException | ApiException e) {
        throw new ApiException(e);
    }
  }

  /**
   * getKeys
   * Retrieving either all keys or the keys that match the given context from the Image Server.
   * @param context The context of the image group, if specified. (optional, default to )
   * @return ApiResponse&lt;Keys&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getKeysWithHttpInfo(String context) throws ApiException {
    Object localVarPostBody = null;

    // create path and map variables
    String localVarPath = "/getKeys";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<>();
    Map<String, String> localVarHeaderParams = new HashMap<>();
    Map<String, Object> localVarFormParams = new HashMap<>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "context", context));



    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {

    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "basicAuth" };

    GenericType<String> localVarReturnType = new GenericType<>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
  }
  /**
   * getMetadata
   * Retrieving the image metadata with the given image key.
   * @param imageKey The key of the image group. (required)
   * @param context The context of the image group, if specified. (optional, default to )
   * @return File
   * @throws ApiException if fails to make API call
   */
  public File getMetadata(String imageKey, String context) throws ApiException {
    return getMetadataWithHttpInfo(imageKey, context).getData();
  }

  /**
   * getMetadata
   * Retrieving the image metadata with the given image key.
   * @param imageKey The key of the image group. (required)
   * @param context The context of the image group, if specified. (optional, default to )
   * @return ApiResponse&lt;File&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<File> getMetadataWithHttpInfo(String imageKey, String context) throws ApiException {
    Object localVarPostBody = null;

    // verify the required parameter 'imageKey' is set
    if (imageKey == null) {
      throw new ApiException(400, "Missing the required parameter 'imageKey' when calling getMetadata");
    }

    // create path and map variables
    String localVarPath = "/getMetadata/{imageKey}"
      .replaceAll("\\{" + "imageKey" + "\\}", apiClient.escapeString(imageKey.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<>();
    Map<String, String> localVarHeaderParams = new HashMap<>();
    Map<String, Object> localVarFormParams = new HashMap<>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "context", context));



    final String[] localVarAccepts = {
      "multipart/form-data"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {

    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<File> localVarReturnType = new GenericType<>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
      }
  /**
   * status
   * Getting the current status of the Image Server as text/html response.
   * @param metrics Set to \&quot;yes\&quot;, if a metrics summary should be returned. (optional, default to false)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String status(String metrics) throws ApiException {
    return statusWithHttpInfo(metrics).getData();
  }

  /**
   * status
   * Getting the current status of the Image Server as text/html response.
   * @param metrics Set to \&quot;yes\&quot;, if a metrics summary should be returned. (optional, default to false)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> statusWithHttpInfo(String metrics) throws ApiException {
    Object localVarPostBody = null;

    // create path and map variables
    String localVarPath = "/status";

    // query params
    List<Pair> localVarQueryParams = new ArrayList<>();
    Map<String, String> localVarHeaderParams = new HashMap<>();
    Map<String, Object> localVarFormParams = new HashMap<>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "metrics", metrics));



    final String[] localVarAccepts = {
      "text/html", "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {

    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] {  };

    GenericType<String> localVarReturnType = new GenericType<>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
  }
}
