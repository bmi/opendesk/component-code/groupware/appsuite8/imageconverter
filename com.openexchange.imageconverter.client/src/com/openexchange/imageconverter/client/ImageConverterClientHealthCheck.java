package com.openexchange.imageconverter.client;

import java.net.URL;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;
import com.openexchange.annotation.NonNull;

/**
 * {@link ImageConverterClientHealthCheck }
 *
 * @since v8.0.0
 */
public class ImageConverterClientHealthCheck implements HealthCheck {

    /**
     * IC_CLIENT_HEALTH_CHECK
     */
    private static final String IC_CLIENT_HEALTH_CHECK = "ImageConverterClientHealthCheck";

    /**
     * IC_CLIENT_REMOTEURL
     */
    final private static String IC_CLIENT_REMOTEURL = "remoteImageConverterUrl";

    /**
     * DC_CLIENT_REMOTEURL_NOT_AVAILABLE
     */
    final private static String IC_CLIENT_REMOTEURL_NOT_AVAILABLE = "n/a";

    /**
     * IC_SERVICE_AVAILABLE
     */
    final private static String IC_SERVICE_AVAILABLE = "serviceAvailable";

    /**
     * Initializes a new {@link DCHealthCheck}.
     * @param client
     */
    public ImageConverterClientHealthCheck(@NonNull ImageConverterClient client) {
        m_client = client;
    }

    /**
     *
     */
    @Override
    public HealthCheckResponse call() {
        final ImageConverterClientConfig config = m_client.getConfig();
        final URL remoteURL = config.getImageConverterServerURL();
        final HealthCheckResponseBuilder healthResponseBuilder = HealthCheckResponse.builder().
            name(IC_CLIENT_HEALTH_CHECK).
            withData(IC_CLIENT_REMOTEURL, (null != remoteURL) ? remoteURL.toString() : IC_CLIENT_REMOTEURL_NOT_AVAILABLE).
            withData(IC_SERVICE_AVAILABLE, m_client.isRemoteServiceAvailable());

        // UP/DOWN status is set to UP in every case!
        healthResponseBuilder.up();

        return healthResponseBuilder.build();
    }

    // - Members ---------------------------------------------------------------

    @NonNull final private ImageConverterClient m_client;
}
