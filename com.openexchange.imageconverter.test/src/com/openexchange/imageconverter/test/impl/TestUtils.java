/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.test.impl;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.annotation.NonNull;
import com.openexchange.exception.OXException;

/**
 * {@link TestUtils}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public class TestUtils {

    /**
     * Initializes a new {@link TestUtils}.
     */
    private TestUtils() {
        super();
    }

    // - Public API ------------------------------------------------------------

    public static void startTest(final File testLogFile) {
        if (null != testLogFile) {
            try {
                TEST_WRITER = new PrintWriter(Files.newOutputStream(testLogFile.toPath(), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND));
                TEST_WRITER.println();
                TEST_WRITER.println("***** TEST STARTED - " + (new Date()).toString() + " *****");
                TEST_WRITER.println();
                TEST_WRITER.flush();
            } catch (IOException e) {
                logExcp(e);
            }
        }
    }

    /**
     *
     */
    public static void testFinished() {
        if (null != TEST_WRITER) {
            TEST_WRITER.println();
            TEST_WRITER.println("***** TEST FINISHED - " + (new Date()).toString() + " *****");
            TEST_WRITER.flush();
            IOUtils.closeQuietly(TEST_WRITER);
            TEST_WRITER = null;
        }
    }

    /**
     * @param filename
     * @return
     * @throws OXException
     */
    public static byte[] getBundleResourceBuffer(@NonNull String filename, @NonNull Class<?> clazz) throws OXException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        byte[] resourceBuffer = null;

        if (null == loader) {
            loader = clazz.getClassLoader();
        }

        final URL url = loader.getResource(filename);

        if (null != url) {
            URLConnection connection = null;

            try {
                connection = url.openConnection();

                if (null != connection) {
                    connection.connect();

                    try (InputStream resourceInputStm = connection.getInputStream()) {
                        if (null != resourceInputStm) {
                            resourceBuffer = IOUtils.toByteArray(resourceInputStm);
                        }
                    }
                }
            } catch (IOException e) {
                logExcp(e);
            }
        }

        return resourceBuffer;
    }

    // - Logging ---------------------------------------------------------------

    /**
     * @return
     */
    public static boolean isLogError() {
        return implGetLogger().isErrorEnabled();
    }

    /**
     * @param msg
     */
    public static void logError(final String msg) {
        if (isNotEmpty(msg)) {
            implGetLogger().error(msg);
        }
    }

    /**
     * @return
     */
    public static boolean isLogWarn() {
        return implGetLogger().isWarnEnabled();
    }

    /**
     * @param msg
     */
    public static void logWarn(final String msg) {
        if (isNotEmpty(msg)) {
            implGetLogger().warn(msg);
        }
    }

    /**
     * @return
     */
    public static boolean isLogInfo() {
        return implGetLogger().isInfoEnabled();
    }

    /**
     * @param msg
     */
    public static void logInfo(final String msg) {
        if (isNotEmpty(msg)) {
            implGetLogger().info(msg);
        }
    }

    /**
     * @return
     */
    public static boolean isLogDebug() {
        return implGetLogger().isDebugEnabled();
    }

    /**
     * @param msg
     */
    public static void logDebug(final String msg) {
        if (isNotEmpty(msg)) {
            implGetLogger().debug(msg);
        }
    }

    /**
     * @return
     */
    public static boolean isLogTrace() {
        return implGetLogger().isTraceEnabled();
    }

    /**
     * @param msg
     */
    public static void logTrace(final String msg) {
        if (isNotEmpty(msg)) {
            implGetLogger().trace(msg);
        }
    }

    /**
     * @param e
     */
    public static void logExcp(Throwable e) {
        if (implGetLogger().isErrorEnabled()) {
            String msg = null;

            if (null != e) {
                final Throwable cause = e.getCause();

                msg = ((null != cause) ? cause : e).getMessage();

                if (null != msg) {
                    if (msg.length() > ERROR_MESSAGE_TOTAL_LENGTH) {
                        final int beginLength = ERROR_MESSAGE_TOTAL_LENGTH - ERROR_MESSAGE_END_LENGTH - ERROR_MESSAGE_FILL_STRING.length();
                        final int endLength = Math.min(msg.length() - beginLength, ERROR_MESSAGE_END_LENGTH);
                        msg = msg.substring(0, beginLength) + ((endLength > 0) ? ("..." + msg.substring(msg.length() - endLength)) : StringUtils.EMPTY);
                    }
                } else {
                    msg= e.getClass().getName();
                }
            } else {
                msg = DEF_EXCEPTION_MSG;
            }

            implGetLogger().error(msg);
        }
    }

    /**
     * @param message
     */
    public static void logTest(final String message) {
        if (StringUtils.isNotBlank(message)) {
            final String output = "[ImageConverter Test Output]: " + message;

           logInfo(output);

           if (null != TEST_WRITER) {
               TEST_WRITER.println(output);
               TEST_WRITER.flush();
           }
        }
    }

    // - Implementation --------------------------------------------------------

    protected static Logger implGetLogger() {
        return LOG;
    }

    // - static members --------------------------------------------------------

    final private static Logger LOG = LoggerFactory.getLogger(TestUtils.class);

    final private static  String DEF_EXCEPTION_MSG = "Unspecified exception occured";

    final private static  String ERROR_MESSAGE_FILL_STRING = "...";

    final private static int ERROR_MESSAGE_TOTAL_LENGTH = 2048;

    final private static int ERROR_MESSAGE_END_LENGTH = 256;

    private static PrintWriter TEST_WRITER = null;
}
