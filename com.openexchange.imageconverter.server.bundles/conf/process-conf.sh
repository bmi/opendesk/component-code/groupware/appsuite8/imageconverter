
# Defines the Java options for the ImageConverter server Java virtual machine.
JAVA_XTRAOPTS="-Xmx512M -XX:MaxPermSize=256m -XX:+UseConcMarkSweepGC -XX:+CMSPermGenSweepingEnabled -XX:+CMSClassUnloadingEnabled"
