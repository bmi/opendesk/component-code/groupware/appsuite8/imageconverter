/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imagemetadata.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.drew.metadata.Directory;
import com.drew.metadata.Tag;
import com.google.common.base.Throwables;
import com.openexchange.annotation.NonNull;
import com.openexchange.imageconverter.api.IMetadataGroup;
import com.openexchange.imageconverter.api.MetadataGroup;
import com.openexchange.imageconverter.api.MetadataKey;


/**
 * {@link MetadataImpl}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
/**
 * {@link MetadataGroupImpl}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public class MetadataGroupImpl implements IMetadataGroup {

    /**
     * Initializes a new {@link MetadataImpl}.
     */
    @SuppressWarnings("unused")
    private MetadataGroupImpl() {
        super();
    }

    /**
     * Initializes a new {@link MetadataGroupImpl}.
     * @param metadataGroup
     */
    public MetadataGroupImpl(@NonNull final MetadataGroup metadataGroup) {
        super();
        m_metadataGroup = metadataGroup;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.api.IMetadataGroup#getGroupName()
     */
    @Override
    public String getName() {
        return m_metadataGroup.getName();
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.api.IMetadataGroup#getGroup()
     */
    @Override
    public MetadataGroup getDataGroup() {
        return m_metadataGroup;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.api.IMetadataGroup#getKeys()
     */
    @Override
    public Set<MetadataKey> getDataKeys() {
        return m_data.keySet();
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.api.IMetadataGroup#getDataValueDescriptiopn(com.openexchange.imageconverter.api.api.MetadataKey)
     */
    @Override
    public Object getDataValue(final MetadataKey MetadataKey) {
        final Pair<Object, String> keyValue = m_data.get(MetadataKey);

        return ((null != keyValue) ? keyValue.getLeft() : null);
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.api.IMetadataGroup#getDataValueDescriptiopn(com.openexchange.imageconverter.api.api.MetadataKey)
     */
    @Override
    public String getDataValueDescriptiopn(final MetadataKey MetadataKey) {
        final Pair<Object, String> keyValue = m_data.get(MetadataKey);
        return ((null != keyValue) ? keyValue.getRight() : null);
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.api.IMetadataGroup#getJSONObject()
     */
    @Override
    public JSONObject getJSONObject() {
        final JSONObject ret = new JSONObject();

        for (final MetadataKey curDataKey : getDataKeys()) {
            try {
                ret.put(curDataKey.getName(), getDataValueDescriptiopn(curDataKey));
            } catch (JSONException e) {
                LOG.error(Throwables.getRootCause(e).getMessage());
            }
        }

        return ret;
    }

    // - API -------------------------------------------------------------------

    /**
     * @param directory
     */
    public void addDirectory(@NonNull final Directory directory) {
        final Collection<Tag> tagCollection = directory.getTags();

        for (final Tag curTag : tagCollection) {
            final int tagType = curTag.getTagType();
            final MetadataKey metadataKey = MetadataUtils.getMetadataKeyFromGroupTagType(m_metadataGroup, tagType);

            if (null != metadataKey) {
                m_data.put(metadataKey, new ImmutablePair<>(directory.getObject(tagType), directory.getDescription(tagType)));
            }
        }
    }

    /**
     * @param directory
     */
    public void addJSONObject(@NonNull final JSONObject jsonObject) {
        MetadataKey metadataKey = null;
        String stringValue = null;

        for (final String curKey : jsonObject.keySet()) {
            try {
                if ((null != (metadataKey = MetadataUtils.getDataKeyFromGroupName(m_metadataGroup, curKey))) &&
                    StringUtils.isNotBlank(stringValue = jsonObject.getString(curKey))) {
                        m_data.put(metadataKey, new ImmutablePair<>(jsonObject.get(curKey), stringValue));
                }
            } catch (Exception e) {
                LOG.error(Throwables.getRootCause(e).getMessage());
            }
        }
    }

    /**
     * @param map
     */
    public void addMap(@NonNull final Map<MetadataKey, Object> map) {
        for (final MetadataKey curKey : map.keySet()) {
            if (null != curKey) {
                final Object mapValue = map.get(curKey);

                if (null != mapValue) {
                    m_data.put(curKey, new ImmutablePair<>(mapValue, mapValue.toString()));
                }
            }
        }
    }

    // - Members ---------------------------------------------------------------

    final private static Logger LOG = LoggerFactory.getLogger(MetadataGroupImpl.class);

    protected MetadataGroup m_metadataGroup = null;

    protected Map<MetadataKey, Pair<Object, String>> m_data = new HashMap<>();
}
