/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imagemetadata.impl;

import java.awt.Dimension;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import com.drew.metadata.Directory;
import com.google.common.collect.ImmutableMap;
import com.openexchange.annotation.NonNull;
import com.openexchange.imageconverter.api.IMetadataGroup;
import com.openexchange.imageconverter.api.MetadataGroup ;
import com.openexchange.imageconverter.api.MetadataKey;
import com.openexchange.imagetransformation.ImageMetadata;
import com.openexchange.imagetransformation.ImageMetadataOptions;
import com.openexchange.imagetransformation.ImageMetadataService;

/**
 * {@link MetadataUtils}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public class MetadataUtils {

    /**
     * Initializes a new {@link MetadataUtils}.
     */
    private MetadataUtils() {
        super();
    }

    /**
     * @param directory
     * @return
     */
    public static MetadataGroup  getDataGroupFromDirectory(final Directory directory) {
        MetadataGroup  ret = null;

        if (null != directory) {
            final String directoryName = directory.getName().toLowerCase();

            if (directoryName.indexOf("exif") > -1) {
                ret = MetadataGroup .EXIF;
            } else if (directoryName.indexOf("file") > -1) {
                ret = MetadataGroup .FILE;
            } else if (directoryName.indexOf("gps") > -1) {
                ret = MetadataGroup .GPS;
            } else if (directoryName.indexOf("thumbnail") > -1) {
                ret = MetadataGroup .THUMBNAIL;
            }
        }

        return ret;
    }

    /**
     * @param directory
     * @return
     */
    public static boolean isDirectorySupported(final Directory directory) {
        return (null != getDataGroupFromDirectory(directory));
    }

    /**
     * @param dataGroup
     * @param tagType
     * @return
     */
    public static MetadataKey getMetadataKeyFromGroupTagType(final MetadataGroup  dataGroup, final int tagType) {
        MetadataKey ret = null;

        if (null != dataGroup) {
            ret = DATAKEY_MAP_GROUP_ID.get(new ImmutablePair<>(dataGroup, Integer.valueOf(tagType)));
        }

        return ret;
    }

    /**
     * @param dataGroup
     * @param name
     * @return
     */
    public static MetadataKey getDataKeyFromGroupName(final MetadataGroup  dataGroup, final String name) {
        MetadataKey ret = null;

        if ((null != dataGroup) && StringUtils.isNotBlank(name)) {
            ret = DATAKEY_MAP_GROUP_NAME.get(new ImmutablePair<>(dataGroup, name));
        }

        return ret;
    }

    /**
     * @param metadataGroup
     * @param metadataKey
     * @return
     */
    protected static Integer getIntegerValue(@NonNull final IMetadataGroup metadataGroup, @NonNull final MetadataKey metadataKey) {
        final Object curValue = metadataGroup.getDataValue(metadataKey);

        return ((curValue instanceof Number) ?
            Integer.valueOf(((Number) curValue).intValue()) :
                Integer.valueOf((null != curValue) ?
                    Integer.parseInt(curValue.toString()) :
                        -1));

    }

    /**
     * @param imageInputStm
     * @return
     * @throws Exception
     */
    public static ImageMetadata getImageInformation(@NonNull final InputStream imageInputStm) throws IOException {
        final ImageMetadataService metadataService = Services.getService(ImageMetadataService.class);

        return (null != metadataService) ?
            metadataService.getMetadataFor(imageInputStm, null, null, ImageMetadataOptions.builder().withDimension().withFormatName().build()) :
                ImageMetadata.builder().withDimension(new Dimension(-1, -1)).withFormatName("png").build();
    }

    /**
     * DATAKEY_MAP
     */
    public final static ImmutableMap<ImmutablePair<MetadataGroup , Integer>, MetadataKey> DATAKEY_MAP_GROUP_ID;
    public final static ImmutableMap<ImmutablePair<MetadataGroup , String>, MetadataKey> DATAKEY_MAP_GROUP_NAME;

    static {
        // static initializing of DATAKEY_MAPs with all available enums
        final ImmutableMap.Builder<ImmutablePair<MetadataGroup ,Integer>, MetadataKey> builderGroupId = ImmutableMap.<ImmutablePair<MetadataGroup , Integer>, MetadataKey>builder();
        final ImmutableMap.Builder<ImmutablePair<MetadataGroup ,String>, MetadataKey> builderGroupName = ImmutableMap.<ImmutablePair<MetadataGroup , String>, MetadataKey>builder();

        for (final MetadataKey curDataKey : MetadataKey.values()) {
            if (null != curDataKey) {
                builderGroupId.put(new ImmutablePair<>(curDataKey.getDataGroup(), Integer.valueOf(curDataKey.getId())), curDataKey);
                builderGroupName.put(new ImmutablePair<>(curDataKey.getDataGroup(), curDataKey.getName()), curDataKey);
            }
        }

        DATAKEY_MAP_GROUP_ID = builderGroupId.build();
        DATAKEY_MAP_GROUP_NAME = builderGroupName.build();
    }
}
