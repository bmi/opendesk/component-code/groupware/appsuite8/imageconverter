/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imagemetadata.impl;

import java.awt.Dimension;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.google.common.base.Throwables;
import com.openexchange.imageconverter.api.IMetadata;
import com.openexchange.imageconverter.api.IMetadataGroup;
import com.openexchange.imageconverter.api.MetadataGroup ;
import com.openexchange.imageconverter.api.MetadataKey;

/**
 * {@link MetadataImpl}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public class MetadataImpl implements IMetadata {

    /**
     * Initializes a new {@link MetadataImpl}.
     */
    public MetadataImpl() {
        super();

        implInit();
    }

    /**
     * Initializes a new {@link MetadataImpl}.
     */
    public MetadataImpl(final Metadata metadata) {
        super();

        if (null != metadata) {
            for (final Directory curDirectory : metadata.getDirectories()) {
                final MetadataGroupImpl metadataGroupImpl = getOrCreateMetadataGroup(curDirectory);

                if (null != metadataGroupImpl) {
                    metadataGroupImpl.addDirectory(curDirectory);
                }
            }
        }

        implInit();
    }

    /**
     * Initializes a new {@link MetadataImpl}.
     */
    public MetadataImpl(final JSONObject jsonObject) {
        super();

        if (null != jsonObject) {
            for (final MetadataGroup  curDataGroup : MetadataGroup.values()) {
                JSONObject jsonMetadataGroupObj = jsonObject.optJSONObject(curDataGroup.getName());

                if (null != jsonMetadataGroupObj) {
                    getOrCreateMetadataGroup(curDataGroup).addJSONObject(jsonMetadataGroupObj);
                }
            }
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IMetadata#getImageDimension()
     */
    @Override
    public Dimension getImageDimension() {
        // The Header group is available at every instance
        final IMetadataGroup headerGroup = getMetadataGroup(MetadataGroup.HEADER);
        final Integer width = MetadataUtils.getIntegerValue(headerGroup, MetadataKey.HEADER_PIXEL_WIDTH);
        final Integer height = MetadataUtils.getIntegerValue(headerGroup, MetadataKey.HEADER_PIXEL_HEIGHT);

        return new Dimension(width.intValue(), height.intValue());
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IMetadata#getImageFormatShortName()
     */
    @Override
    public String getImageFormatName() {
        return getMetadataGroup(MetadataGroup.HEADER).getDataValue(MetadataKey.HEADER_FORMAT_NAME).toString();
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.api.IMetadata#getGroup(com.openexchange.imageconverter.api.api.MetadataGroup )
     */
    @Override
    public IMetadataGroup getMetadataGroup(MetadataGroup  metadataGroup) {
        return m_metadatGroupMap.get(metadataGroup);
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.api.IMetadata#iterator()
     */
    @Override
    public Collection<IMetadataGroup> getMetadataGroups() {
        return m_metadatGroupMap.values();
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.api.IMetadata#getJSONObject()
     */
    @Override
    public JSONObject getJSONObject() {
        final JSONObject ret = new JSONObject();

        try {
            for (final MetadataGroup  curGroup : m_metadatGroupMap.keySet()) {
                if (null != curGroup) {
                    ret.put(curGroup.getName(), m_metadatGroupMap.get(curGroup).getJSONObject());
                }
            }
        } catch (JSONException e) {
            LOG.error(Throwables.getRootCause(e).getMessage());
        }

        return ret;
    }

    // - Interface -------------------------------------------------------------

    /**
     * @param directory
     * @return
     */
    MetadataGroupImpl getOrCreateMetadataGroup(final Directory directory) {
        MetadataGroupImpl ret = null;

        if (MetadataUtils.isDirectorySupported(directory)) {
            ret = getOrCreateMetadataGroup(MetadataUtils.getDataGroupFromDirectory(directory));
        }

        return ret;
    }

    /**
     * @param directory
     * @return
     */
    MetadataGroupImpl getOrCreateMetadataGroup(final MetadataGroup metadataGroup) {
        MetadataGroupImpl ret = null;

        if (null != metadataGroup) {
            if (null == (ret = (MetadataGroupImpl) m_metadatGroupMap.get(metadataGroup))) {
                m_metadatGroupMap.put(metadataGroup, ret = new MetadataGroupImpl(metadataGroup));
            }
        }

        return ret;
    }

    // - Implementation --------------------------------------------------------

    private void implInit() {
        // at least the HEADER group ust exist in every case
        getOrCreateMetadataGroup(MetadataGroup.HEADER);
    }

    // - Members ---------------------------------------------------------------

    private final static Logger LOG = LoggerFactory.getLogger(MetadataImpl.class);

    private Map<MetadataGroup , IMetadataGroup> m_metadatGroupMap = new HashMap<>();
}