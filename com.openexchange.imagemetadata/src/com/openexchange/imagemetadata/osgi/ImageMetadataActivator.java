/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imagemetadata.osgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.imageconverter.api.IMetadataReader;
import com.openexchange.imagemetadata.impl.MetadataReaderImpl;
import com.openexchange.imagemetadata.impl.Services;
import com.openexchange.imagetransformation.ImageMetadataService;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link ImageMetadataActivator}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public class ImageMetadataActivator extends HousekeepingActivator {

    /**
     * Initializes a new {@link ImageMetadataActivator}.
     */
    public ImageMetadataActivator() {
        super();
    }

    //-------------------------------------------------------------------------

    /* (non-Javadoc)
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] {
            ConfigurationService.class,
            ImageMetadataService.class
        };
    }

    //-------------------------------------------------------------------------

    /* (non-Javadoc)
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    public void startBundle() throws Exception {
        LOG.info("starting bundle: {}", SERVICE_NAME);

        Services.setServiceLookup(this);

        try {
            openTrackers();

            m_metadataReader = new MetadataReaderImpl();
            registerService(IMetadataReader.class, m_metadataReader);

            LOG.info("successfully started bundle: {}", SERVICE_NAME);
        } catch (Throwable e) {
            ExceptionUtils.handleThrowable(e);
            LOG.error("... starting bundle: {}", SERVICE_NAME + " failed", e);
            throw new RuntimeException(e);
        }
    }

    //-------------------------------------------------------------------------

    /* (non-Javadoc)
     * @see com.openexchange.osgi.HousekeepingActivator#stopBundle()
     */
    @Override
    public void stopBundle() throws Exception {
        LOG.info("stopping bundle: {}", SERVICE_NAME);

        closeTrackers();
        unregisterServices();
        Services.setServiceLookup(null);

        m_metadataReader = null;

        LOG.info("successfully stopped bundle: {}", SERVICE_NAME);
    }

    // - Static Members --------------------------------------------------------

    final private static String SERVICE_NAME = "Open-Xchange IMetadataReader";

    final private static Logger LOG = LoggerFactory.getLogger(ImageMetadataActivator.class);

    // - Members ---------------------------------------------------------------

    private IMetadataReader m_metadataReader = null;
}
