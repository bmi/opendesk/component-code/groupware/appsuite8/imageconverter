/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.osgi;

import static com.openexchange.imageconverter.impl.ImageConverterUtils.LOG;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import org.osgi.framework.ServiceReference;
import com.google.common.base.Throwables;
import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.exception.OXException;
import com.openexchange.imageconverter.api.IImageConverter;
import com.openexchange.imageconverter.api.IMetadataReader;
import com.openexchange.imageconverter.api.ImageConverterException;
import com.openexchange.imageconverter.impl.ImageConverter;
import com.openexchange.imageconverter.impl.ImageConverterConfig;
import com.openexchange.imageconverter.impl.ImageConverterMBean;
import com.openexchange.imageconverter.impl.ImageConverterMetrics;
import com.openexchange.imageconverter.impl.ImageConverterUtils;
import com.openexchange.imageconverter.impl.Services;
import com.openexchange.management.ManagementService;
import com.openexchange.objectcache.api.IObjectCache;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.osgi.SimpleRegistryListener;

//=============================================================================
public class ImageConverterActivator extends HousekeepingActivator {

    public ImageConverterActivator() {
        super();
    }

    //-------------------------------------------------------------------------

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] {
            ConfigurationService.class,
            IObjectCache.class,
            IMetadataReader.class
        };
    }

    //-------------------------------------------------------------------------

    @Override
    protected void startBundle() throws Exception {
        try {
            LOG.info("starting bundle: {}", SERVICE_NAME);

            Services.setServiceLookup(this);

            final ConfigurationService configService = getService(ConfigurationService.class);

            if (null == configService) {
                throw new ImageConverterException("Could not get Configuration service");
            }

            // the IObjectCache interface is optional for ImageConverter class;
            // in case it is not availale, the cacheAndGet... methods will use on demand
            // conversion without caching to convert images
            final IObjectCache objectCache = getService(IObjectCache.class);
            final IMetadataReader metadataReader = getService(IMetadataReader.class);

            if (null == objectCache) {
                throw new ImageConverterException("Could not get IObjectCache service");
            }

            if (null == metadataReader) {
                throw new ImageConverterException("Could not get IMetadataReader service");
            }

            m_imageConverterConfig = new ImageConverterConfig(configService);
            m_imageConverter = new ImageConverter(objectCache, metadataReader, m_imageConverterConfig);
            m_imageConverterMetrics = new ImageConverterMetrics(ImageConverterUtils.IC_MONITOR);
            m_imageConverterMetrics.registerMetrics();

            registerService(IImageConverter.class, m_imageConverter);

            implRegisterMBean();

            openTrackers();

            LOG.info("successfully started bundle: {}", SERVICE_NAME);
        } catch (Throwable e) {
            ExceptionUtils.handleThrowable(e);
            LOG.error("... starting bundle: {}", SERVICE_NAME + " failed: {}", Throwables.getRootCause(e).getMessage());
            throw new RuntimeException(e);
        }
    }

    //-------------------------------------------------------------------------

    @Override
    protected void stopBundle() throws Exception {
        LOG.info("stopping bundle: {}", SERVICE_NAME);

        closeTrackers();
        unregisterServices();
        Services.setServiceLookup(null);

        if (null != m_imageConverter) {
            ImageConverterUtils.IC_MONITOR.shutdown();

            m_imageConverter.shutdown();
            m_imageConverter = null;
        }

        if (null != m_imageConverterConfig) {
            ImageConverterUtils.close(m_imageConverterConfig);
        }

        LOG.info("successfully stopped bundle: {}", SERVICE_NAME);
    }

    // - Implementation --------------------------------------------------------

    /**
     * Registering the monitoring MBean once
     */
    private void implRegisterMBean() {
        if (null == m_imageConverterMBean) {
            final ObjectName mbeanObjectName = ImageConverterMBean.getObjectName();

            if (null != mbeanObjectName) {
                try {
                    m_imageConverterMBean = new ImageConverterMBean(ImageConverterUtils.IC_MONITOR);
                    ImageConverterUtils.IC_MBEAN = m_imageConverterMBean;

                    track(ManagementService.class, new SimpleRegistryListener<ManagementService>() {
                        @Override
                        public void added(ServiceReference<ManagementService> ref, ManagementService service) {
                            try {
                                service.registerMBean(mbeanObjectName, m_imageConverterMBean);
                                LOG.info("IC monitoring successfull registered");
                            } catch (final OXException e) {
                                LOG.error(e.getMessage(), Throwables.getRootCause(e));
                            }
                        }

                        @Override
                        public void removed(ServiceReference<ManagementService> ref, ManagementService service) {
                            try {
                                service.unregisterMBean(mbeanObjectName);
                                LOG.info("IC monitoring successfully unregistered");
                            } catch (final OXException e) {
                                LOG.error(e.getMessage(), Throwables.getRootCause(e));
                            }
                        }
                    });
                } catch (NotCompliantMBeanException e) {
                    LOG.error(e.getMessage(), Throwables.getRootCause(e));
                }
            } else {
                LOG.error("IC error when initializing monitoring MBean: invalid ObjectName");
            }
        } else {
            LOG.warn("IC monitoring MBean already inititalized");
        }
    }

    // - Static Members --------------------------------------------------------

    private static final String SERVICE_NAME = "Open-Xchange ImageConverter";

    // - Members ---------------------------------------------------------------

    protected ImageConverterConfig m_imageConverterConfig = null;

    protected ImageConverter m_imageConverter = null;

    protected ImageConverterMBean m_imageConverterMBean = null;

    protected ImageConverterMetrics m_imageConverterMetrics = null;
}
