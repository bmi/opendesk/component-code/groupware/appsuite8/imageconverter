/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicLong;
import com.google.common.base.Throwables;
import com.openexchange.imageconverter.api.ImageConverterException;
import com.openexchange.imageconverter.api.ImageConverterPriority;
import com.openexchange.imageconverter.api.ImageFormat;
import com.openexchange.metrics.micrometer.Micrometer;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.Nullable;
import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tags;

/**
 * {@link ImageConverterMonitoring}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public class ImageConverterMonitoring {

    // set ahead time to periodically retrieve results this time
    final private static long REFRESH_AHEAD_TIME_MILLIS = 30 * 1000;

    /**
     * MONITORING_REFRESH_PERIOD_MILLIS is set to 5 minutes (equals Munin default refresh period)
     */
    final private static long MONITORING_REFRESH_PERIOD_MILLIS = 5 * 60 * 1000;

    /**
     * REQUESTED_FORMAT_REFRESH_PERIOD_MILLIS is set to 10 minutes,
     *
     * Registered requested format Micrometer gauges will be deleted after this period if
     * more than REQUESTED_FORMAT_MAX_LIST_ENTRIES gauges are registered.
     */
    final private static long REQUESTED_FORMAT_REFRESH_PERIOD_MILLIS = 10 * 60 * 1000;

    final private static long REQUESTED_FORMAT_MAX_LIST_ENTRIES = 30;

    /**
     * {@link RequestType}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.0
     */
    public enum RequestType {
        GET,
        CACHE,
        CACHE_AND_GET,
        ADMIN
    }

    /**
     * {@link Counter}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.0
     */
    public class Counter implements Comparable<Counter> {

        /* (non-Javadoc)
         * @see java.lang.Comparable#compareTo(java.lang.Object)
         */
        @Override
        public int compareTo(Counter other) {
            return (null != other) ?
                Long.compare(get(), other.get()) :
                    1;
        }

        /**
         * Initializes a new {@link Counter}.
         */
        public Counter() {
            super();
        }

        /**
         * Initializes a new {@link Counter}.
         * @param count
         */
        public Counter(final long count) {
            super();
            m_count = count;
        }

        /**
         * @return The count
         */
        public long get() {
            return m_count;
        }

        /**
         * @param count
         */
        public void  set(final long count) {
            m_count = count;
        }

        /**
         *
         */
        public void increment() {
            ++m_count;
        }

        /**
         *
         */
        public void decrement() {
            --m_count;
        }

        /**
         * @param counter2
         * @return
         */
        public long add(final Counter counter2) {
            return ((null != counter2) ?
                (m_count += counter2.get()) :
                    m_count);
        }

        // - Members -----------------------------------------------------------

        private long m_count = 0;
    }

    /**
     * Initializes a new {@link ImageConverterMonitoring}.
     */
    public ImageConverterMonitoring(@NonNull final ImageConverter imageConverter) {
        this(imageConverter, MONITORING_REFRESH_PERIOD_MILLIS, REQUESTED_FORMAT_REFRESH_PERIOD_MILLIS);
    }

    /**
     * Initializes a new {@link ImageConverterMonitoring}.
     */
    public ImageConverterMonitoring(@NonNull final ImageConverter imageConverter, final long refreshPeriodMillis, final long requestFormatPeriodMillis) {
        super();

        m_imageConverter = imageConverter;

        // initialize all periodic members
        for (final ImageConverterPriority curImageConverterPriority : ImageConverterPriority.values()) {
            synchronized (m_peakQueueCounts) {
                m_peakQueueCounts.put(curImageConverterPriority, new Counter(0));
                m_peakQueueCountsResult.put(curImageConverterPriority, new Counter(0));
            }

            synchronized (m_queueTimesMillis) {
                m_queueTimesMillis.put(curImageConverterPriority, new ArrayList<Counter>());
                m_medianQueueTimesMillis.put(curImageConverterPriority, new Counter(0));
            }
        }

        for (final RequestType curRequestType : RequestType.values()) {
            synchronized (m_requestCount) {
                m_requestCount.put(curRequestType, new Counter(0));
            }

            synchronized (m_requestLimitReachedCount) {
                m_requestLimitReachedCount.put(curRequestType, new Counter(0));
            }

            synchronized (m_requestTimesMillis) {
                m_requestTimesMillis.put(curRequestType, new ArrayList<Counter>());
                m_medianRequestTimesMillis.put(curRequestType, new Counter(0));
            }
        }

        // the first values are retrieved => call reset period to retrieve latest results
        implResetMeasurementPeriod();

        // start the timer to periodically retrieve further result sets
        // a short amount of time before the next results will be requested
        // with a fixed period from external tools, e.g. 300s with munin
        m_resetTimer.scheduleAtFixedRate(m_resetTask, refreshPeriodMillis - REFRESH_AHEAD_TIME_MILLIS, refreshPeriodMillis);

        m_RequestFormatResetTimer.scheduleAtFixedRate(m_RequestFormatResetTask, requestFormatPeriodMillis, requestFormatPeriodMillis);
    }

    // - Public API ------------------------------------------------------------

    /**
     * @param processTime
     */
    public void incrementProcessedKeys(final long processTime) {
        synchronized (m_processTimesMillis) {
            m_processTimesMillis.add(new Counter(processTime));
            m_processedCount.increment();
        }
    }

    /**
     * @param requestType
     * @param requestTime
     * @param imageConverterStatus
     */
    public void incrementRequests(@NonNull final RequestType requestType, final long requestTime, @Nullable ImageConverterStatus imageConverterStatus) {
        final ImageConverterStatus finalStatus = (null == imageConverterStatus) ? ImageConverterStatus.OK : imageConverterStatus;

        synchronized (m_requestCount) {
            m_requestCount.get(requestType).increment();

            if ((RequestType.GET == requestType) && (ImageConverterStatus.OK == finalStatus)) {
                m_succeededGetRequestCount.increment();
            }
        }

        synchronized (m_requestTimesMillis) {
            m_requestTimesMillis.get(requestType).add(new Counter(requestTime));
        }
    }

    /**
     * @param requestType
     */
    public void incrementRequestLimitReachedCount(@NonNull final RequestType requestType) {
        synchronized (m_requestLimitReachedCount) {
            m_requestLimitReachedCount.get(requestType).increment();
        }
    }

    /**
     * @param imageConverterPriority
     * @param queueCount
     */
    public void setQueueCount(@NonNull final ImageConverterPriority imageConverterPriority, final long queueCount) {
        synchronized (m_peakQueueCounts) {
            final Counter peakQueueCount = m_peakQueueCounts.get(imageConverterPriority);

            if (queueCount > peakQueueCount.get()) {
                peakQueueCount.set(queueCount);
            }
        }
    }

    /**
     * @param queueTimeMillis
     * @param imageConverterPriority
     */
    public void addQueueTimeMillis(final long queueTimeMillis, @NonNull final ImageConverterPriority imageConverterPriority) {
        synchronized (m_queueTimesMillis) {
            m_queueTimesMillis.get(imageConverterPriority).add(new Counter(queueTimeMillis));
        }
    }

    /**
    *
    */
    public void shutdown() {
        m_resetTask.cancel();
        m_resetTimer.cancel();
        m_RequestFormatResetTimer.cancel();
    }

    /**
     * @return
     */
    public double getCacheHitRatio() {
        synchronized (m_requestCount) {
            final long getRequestCount = m_requestCount.get(RequestType.GET).get();

            return (getRequestCount > 0) ?
                ((double) m_succeededGetRequestCount.get() / getRequestCount) :
                    1.0;
        }
    }

    /**
     * @return
     */
    public long getKeysProcessedCount() {
        synchronized (m_processedCount) {
            return m_processedCount.get();
        }
    }

    /**
     * @return
     */
    public long getMedianProcessTimeMillis() {
        synchronized (m_processTimesMillis) {
            return implGetMedianTimeMillis(m_processTimesMillis);
        }
    }

    /**
     * @param imageConverterPriority
     * @return
     */
    public long getRequestCount(@NonNull final RequestType requestType) {
        synchronized (m_requestCount) {
            if (null == requestType) {
                final Counter tmpCounter = new Counter();

                m_requestCount.values().forEach(p -> tmpCounter.add(p));
                return tmpCounter.get();
            }

            return m_requestCount.get(requestType).get();
        }
    }

    /**
     * @param imageConverterPriority
     * @return
     */
    public long getRequestLimitReachedCount(@NonNull final RequestType requestType) {
        synchronized (m_requestLimitReachedCount) {
            if (null == requestType) {
                final Counter tmpCounter = new Counter();

                m_requestLimitReachedCount.values().forEach(p -> tmpCounter.add(p));
                return tmpCounter.get();
            }

            return m_requestLimitReachedCount.get(requestType).get();
        }
    }


    /**
     * @param requestType
     * @return
     */
    public long getMedianRequestTimeMillis(@Nullable final RequestType requestType) {
        synchronized (m_requestTimesMillis) {
            return (null == requestType) ?
                implGetMedianTimeMillis(m_medianRequestTimesMillis.values()) :
                    m_medianRequestTimesMillis.get(requestType).get();
        }
    }

    /**
     * @param imageConverterPriority
     * @return
     */
    public long getPeakKeyCountInQueue(@NonNull final ImageConverterPriority imageConverterPriority) {
        synchronized (m_peakQueueCounts) {
            return m_peakQueueCountsResult.get(imageConverterPriority).get();
        }
    }

    /**
     * @return
     */
    public long getMedianKeyQueueTimeMillis(@Nullable final ImageConverterPriority imageConverterPriority) {
        synchronized (m_queueTimesMillis) {
            return (null == imageConverterPriority) ?
                implGetMedianTimeMillis(m_medianQueueTimesMillis.values()) :
                    m_medianQueueTimesMillis.get(imageConverterPriority).get();
        }
    }

    /**
     * @return
     */
    public long getCacheKeyCount() {
        if (null != m_imageConverter) {
            try {
                return m_imageConverter.getKeyCount();
            } catch (ImageConverterException e) {
                ImageConverterUtils.LOG.error("IC monitoring exception when getting key count: {}", Throwables.getRootCause(e).getMessage());
            }
        }

        return 0;
    }

    /**
     * @return
     */
    public long getCacheSize() {
        try {
            return m_imageConverter.getTotalImagesSize();
        } catch (ImageConverterException e) {
            ImageConverterUtils.LOG.error("IC monitoring exception when getting key count: {}", Throwables.getRootCause(e).getMessage());
        }
        return 0;
    }

    /**
     * @return
     */

    private static String REQUEST_FORMAT_METRIC_NAME = "imageconverter" + ".convert.dimensions";

    private Tags getTagsFromImageFormat(ImageFormat imageRequestFormat, int targetWidth, int targetHeight, String sourceFormat) {
        return Tags.of(
            "request", imageRequestFormat.getWidth() + "x" + imageRequestFormat.getHeight(),
            "scaleType", imageRequestFormat.getScaleType().getKeyword(),
            "target", targetWidth + "x" + targetHeight,
            "format", sourceFormat);
    }

    public long getImageFormatRequestCount(Tags tags) {
        final AtomicLong formatCount = m_RequestFormatCounts.get(tags);
        return formatCount != null ? formatCount.longValue() : 0;
    }

    public void registerAndIncreaseImageFormatRequestCount(ImageFormat imageFormat, int targetWidth, int targetHeight, String sourceFormat) {
        final Tags tags = getTagsFromImageFormat(imageFormat, targetWidth, targetHeight, sourceFormat);
        synchronized(m_RequestFormatMeters) {
            Meter meter = m_RequestFormatMeters.get(tags);
            if(meter == null) {
                m_RequestFormatMeters.put(tags, registerRequestFormat(tags));
            }
            AtomicLong count = m_RequestFormatCounts.get(tags);
            if(count == null) {
                count = new AtomicLong(0);
                m_RequestFormatCounts.put(tags, count);
            }
            count.incrementAndGet();
        }
    }

    protected void implResetRequestFormatPeriod() {
        synchronized (m_RequestFormatMeters) {
            final Iterator<Meter> meterIter = m_RequestFormatMeters.values().iterator();
            while(meterIter.hasNext()) {
                Metrics.globalRegistry.remove(meterIter.next());
            }
            m_RequestFormatMeters.clear();
            m_RequestFormatCounts.clear();
        }
    }

    private Meter registerRequestFormat(Tags tags) {
        return Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, REQUEST_FORMAT_METRIC_NAME, tags, "The total number the format was requested.", null, this,
            (m) -> getImageFormatRequestCount(tags));
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param timesCollection
     * @return
     */
    private static long implGetMedianTimeMillis(@NonNull final Collection<Counter> timesCollection) {
        long[] longArray;

        synchronized (timesCollection) {
            longArray = timesCollection.stream().
                sorted().
                mapToLong(Counter::get).
                toArray();
        }

        final int medianIndex = longArray.length >> 1;
        final int length = longArray.length;

        return (length > 0) ?
            Math.round(((longArray.length & 1) == 1) ? longArray[medianIndex] : ((longArray[medianIndex - 1] + longArray[medianIndex]) * 0.5)) :
                0;
    }

    /**
     * set result of all periodic members to a dedicated result
     * object and reset all counting members for the next cycle
     */
    protected void implResetMeasurementPeriod() {
        synchronized (m_processTimesMillis) {
            m_medianProcessTimesMillis.set(implGetMedianTimeMillis(m_processTimesMillis));
            m_processTimesMillis.clear();
        }

        for (final ImageConverterPriority curImageConverterPriority : ImageConverterPriority.values()) {
            synchronized (m_peakQueueCounts) {
                final Counter curCounter = m_peakQueueCounts.get(curImageConverterPriority);

                m_peakQueueCountsResult.put(curImageConverterPriority, curCounter);
                m_peakQueueCounts.put(curImageConverterPriority, new Counter(curCounter.get()));
            }

            synchronized (m_queueTimesMillis) {
                m_medianQueueTimesMillis.get(curImageConverterPriority).set(implGetMedianTimeMillis(m_queueTimesMillis.get(curImageConverterPriority)));
                m_queueTimesMillis.get(curImageConverterPriority).clear();
            }
        }

        for (final RequestType curRequestType : RequestType.values()) {
            synchronized (m_requestTimesMillis) {
                m_medianRequestTimesMillis.get(curRequestType).set(implGetMedianTimeMillis(m_requestTimesMillis.get(curRequestType)));
                m_requestTimesMillis.get(curRequestType).clear();
            }
        }
    }

    // - Members ---------------------------------------------------------------

    final private Timer m_resetTimer = new Timer("ImageConverter Monitoring", true);

    final private TimerTask m_resetTask = new TimerTask() {
        @Override
        public void run() {
            implResetMeasurementPeriod();
        }
    };

    final private Timer m_RequestFormatResetTimer = new Timer("ImageConverter Monitoring - Request Format Timer", true);

    final private TimerTask m_RequestFormatResetTask = new TimerTask() {
        @Override
        public void run() {
            if(m_RequestFormatMeters.size() > REQUESTED_FORMAT_MAX_LIST_ENTRIES) {
                implResetRequestFormatPeriod();
            }
        }
    };

    final private ImageConverter m_imageConverter;

    final private Counter m_processedCount = new Counter();

    final private Map<RequestType, Counter> m_requestCount = new EnumMap<>(RequestType.class);

    final private Map<RequestType, Counter> m_requestLimitReachedCount = new EnumMap<>(RequestType.class);

    final private Counter m_succeededGetRequestCount = new Counter();

    final private List<Counter> m_processTimesMillis = new ArrayList<>();
    final private Counter m_medianProcessTimesMillis = new Counter();

    final private Map<ImageConverterPriority, Counter> m_peakQueueCounts = new EnumMap<>(ImageConverterPriority.class);
    final private Map<ImageConverterPriority, Counter> m_peakQueueCountsResult = new EnumMap<>(ImageConverterPriority.class);

    final private Map<RequestType, List<Counter>> m_requestTimesMillis = new EnumMap<>(RequestType.class);
    final private Map<RequestType, Counter> m_medianRequestTimesMillis = new EnumMap<>(RequestType.class);

    final private Map<ImageConverterPriority, List<Counter>> m_queueTimesMillis = new EnumMap<>(ImageConverterPriority.class);
    final private Map<ImageConverterPriority, Counter> m_medianQueueTimesMillis = new EnumMap<>(ImageConverterPriority.class);

    final private Map<Tags, Meter> m_RequestFormatMeters = new HashMap<Tags, Meter>();
    final private Map<Tags, AtomicLong> m_RequestFormatCounts = new HashMap<Tags, AtomicLong>();
}
