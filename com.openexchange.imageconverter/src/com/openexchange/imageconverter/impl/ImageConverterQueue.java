/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.impl;

import static com.openexchange.imageconverter.impl.ImageConverterUtils.IMAGECONVERTER_GROUPID;
import static com.openexchange.imageconverter.impl.ImageConverterUtils.LOG;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.commons.lang3.ArrayUtils;
import com.google.common.base.Throwables;
import com.openexchange.imageconverter.api.IMetadata;
import com.openexchange.imageconverter.api.IMetadataReader;
import com.openexchange.imageconverter.api.ImageConverterException;
import com.openexchange.imageconverter.api.ImageConverterPriority;
import com.openexchange.imageconverter.api.ImageFormat;
import com.openexchange.imageconverter.api.MetadataImage;
import com.openexchange.imageconverter.impl.ImageProcessor.ImageProcessorStatus;
import com.openexchange.objectcache.api.ICacheObject;
import com.openexchange.objectcache.api.IObjectCache;
import com.openexchange.objectcache.api.IdLocker;
import com.openexchange.objectcache.api.IdLocker.Mode;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.Nullable;
import com.openexchange.objectcache.api.ObjectCacheException;

/**
 * {@link ImageConverterQueue}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
class ImageConverterQueue extends Thread {

    /**
     * {@link ImageConverterJobExecutor}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.2
     */
    private class ImageConverterJobExecutor {

        /**
         * Initializes a new {@link ImageConverterJobExecutor}.
         * @param maxThreadCount
         */
        ImageConverterJobExecutor(final int maxThreadCount) {
            super();

            m_jobExecutor = Executors.newFixedThreadPool(maxThreadCount);
            m_executorJobQueue = new LinkedBlockingQueue<>(maxThreadCount);

            for (int i = 0; i < maxThreadCount; ++i) {
                m_jobExecutor.execute(() -> {
                    while (isRunning()) {
                        try {
                            // Resource warning checked:
                            // job is closed after it has been run
                            final ImageConverterJob curJob = m_executorJobQueue.take();

                            if (null != curJob) {
                                final String imageKey = curJob.getImageKey();

                                IdLocker.lock(imageKey);

                                try {
                                    curJob.run();
                                } finally {
                                    // close job, imageKey will be removed from processingMap as well
                                    implCloseJob(curJob, !isRunning());

                                    // unlock imageKey mit UnlockMode.END_PROCESSING at end of processing
                                    IdLocker.unlock(imageKey, IdLocker.UnlockMode.END_PROCESSING);
                                }
                            }
                        } catch (@SuppressWarnings("unused") InterruptedException e) {
                            // ok
                        }

                    }
                });
            }
        }

        /**
         *
         */
        public @Nullable List<Runnable> shutdown() {
            List<Runnable> ret = null;

            if (m_executorRunning.compareAndSet(true, false)) {
                ret = m_jobExecutor.shutdownNow();

                if (null != ret) {
                    ret.addAll(m_executorJobQueue);
                }
            }

            return ret;
        }

        /**
         * @param job
         */
        public boolean addJob(@NonNull final ImageConverterJob job) throws InterruptedException {
            if (m_executorRunning.get()) {
                m_executorJobQueue.put(job);
                return true;
            }

            return false;
        }

        // - Members -----------------------------------------------------------

        final AtomicBoolean m_executorRunning = new AtomicBoolean(true);

        final private ExecutorService m_jobExecutor;

        final private LinkedBlockingQueue<ImageConverterJob> m_executorJobQueue;
    }

    /**
     * {@link QueueEntry}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.2
     */
    private static class ImageConverterQueueEntry {

        /**
         * Initializes a new {@link ImageConverterQueueEntry}.
         * @param job
         * @param priority
         */
        ImageConverterQueueEntry(@NonNull final ImageConverterJob job, @NonNull final ImageConverterPriority priority) {
            m_job = job;
            m_priority = priority;
        }

        /**
         * @return
         */
        ImageConverterJob getJob() {
            return m_job;
        }

        /**
         * @return
         */
        ImageConverterPriority getPriority() {
            return m_priority;
        }

        // - Members -----------------------------------------------------------

        final private ImageConverterJob m_job;
        final private ImageConverterPriority m_priority;
    }

    /**
     * Initializes a new {@link ImageConverterQueue}.
     */
    ImageConverterQueue(
        @NonNull final ImageConverter imageConverter,
        @NonNull final ImageProcessor imageProcessor,
        @NonNull final IObjectCache objectCache,
        @NonNull final IMetadataReader metadataReader) {

        super("ImageConverter Queue");

        m_imageConverter = imageConverter;
        m_objectCache = objectCache;
        m_metadataReader = metadataReader;

        // create an ordered BidiMap to map ImageFormats to image format strings and vice versa
        m_imageFormats = m_imageConverter.getImageFormats();
        m_imageProcessor = imageProcessor;

        for (final ImageConverterPriority curPriority : ImageConverterPriority.values()) {
            m_queuesImpl.put(curPriority, new LinkedList<ImageConverterJob>());
        }

        m_executor = new ImageConverterJobExecutor(ImageConverterConfig.IMAGECONVERTER_THREAD_COUNT);

        start();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        boolean trace = false;

        LOG.info(ImageConverterUtils.IC_STR_BUILDER().
            append("conversion queue started (thread count: ").
            append(ImageConverterConfig.IMAGECONVERTER_THREAD_COUNT).
            append(", queue entries: ").append(ImageConverterConfig.IMAGECONVERTER_QUEUE_LENGTH).
            append(')').toString());

        while (isRunning()) {
            trace = LOG.isTraceEnabled();

            try {
                // wait until a job is available in queue
                while (isRunning() && !implHasQueuedJob()) {
                    try {
                        m_lock.lock();
                        m_jobAvailableConditon.await();
                    } catch (@SuppressWarnings("unused") InterruptedException e) {
                        // ok
                    } finally {
                        m_lock.unlock();
                    }
                }

                // get next job from queue (prioritized)
                final ImageConverterQueueEntry queueEntry = implGetAndRemoveNextJob();

                if (null != queueEntry) {
                    // Resource warning has been checked:
                    // job is closed when runnble finishes or in case,
                    // the job could not be processed at all
                    final ImageConverterJob job = queueEntry.getJob();

                    ImageConverterUtils.IC_MONITOR.addQueueTimeMillis(
                        job.getCurrentMeasurementTimeMillis(),
                        queueEntry.getPriority());

                    if (!m_executor.addJob(job)) {
                        // close job, imageKey will be removed from processingMap as well
                        implCloseJob(job, true);
                    }
                }
            } catch (Exception e) {
                if (trace) {
                    LOG.trace("IC queue received exception while taking next job from queue and forwarding to executor: {}", Throwables.getRootCause(e).getMessage());
                }
            }
        }

        if (trace) {
            LOG.trace("IC queue worker finished");
        }
    }

    // - Public API ------------------------------------------------------------

    /**
     * @return
     */
    public ImageProcessorStatus getImageProcessorStatus() {
        return m_imageProcessor.getStatus();
    }

    /**
     * @return
     */
    public ImageConverter getImageConverter() {
        return m_imageConverter;
    }

    /**
     * @return
     */
    public IObjectCache getFileItemService() {
        return m_objectCache;
    }

    /**
     * @return
     */
    public IMetadataReader getMetadataReader() {
        return m_metadataReader;
    }

    /**
     * @return
     */
    public ImageProcessor getImageProcessor() {
        return m_imageProcessor;
    }

    /**
     * @param imageKey
     * @return true in case a job is currently scheduled for processing or processed
     */
    public boolean isProcessing(@NonNull final String imageKey) {
        return m_processingMap.containsKey(imageKey);
    }

    /**
     * @param imageKey The image key.
     * @param srcImageStm The image input data.
     * @param priority The priority of job to be queued.
     * @param requestFormat The optional request format for which to return an image synchronously.
     *  This paramter might be null in which case only a cache job is scheduled and null is returned.
     * @param context The optional context data.
     * @param removeEntryInCaseOfError Removes just created job results in case of an error.
     * @return The requested MetadataImage or null if requestFormat is null.
     * @throws ImageConverterException
     */
    MetadataImage cacheAndGetMetadataImage(
        @NonNull final String imageKey,
        @NonNull final InputStream srcImageStm,
        @NonNull final ImageConverterPriority priority,
        @Nullable final String requestFormat,
        @Nullable final String context,
        boolean removeEntryInCaseOfError) throws ImageConverterException {

        final boolean hasRequestFormat = (null != requestFormat);
        MetadataImage ret = null;

        // don't create a new job with same imageKey if such a job has been already created or is currently processed
        IdLocker.lock(imageKey, IdLocker.Mode.WAIT_IF_PROCESSED);

        try {
            // When a previous job is already created or finished, but waiting in a queue to be processed
            // it is not locked during queue time => we don't want to create a new job in these cases, so
            // that we check here if already finished
            if (!m_imageConverter.isAvailable(imageKey)) {
                // begin processing out of the lock state of the previous lock
                IdLocker.lock(imageKey, IdLocker.Mode.BEGIN_PROCESSING);

                ImageConverterJob job = null;

                try {
                    job = new ImageConverterJob(imageKey, srcImageStm, m_imageFormats, this, context);

                    if (hasRequestFormat) {
                        try {
                            ImageConverterUtils.IC_MONITOR.setQueueCount(ImageConverterPriority.INSTANT, m_instantProcessingCount.incrementAndGet());

                            if (null == (ret = job.processRequestFormat(requestFormat))) {
                                // resource warning has been checked => job is closed in catch block
                                throw new ImageConverterException("IC error while creating/processing new ImageConverterJob");
                            }
                        } finally {
                            ImageConverterUtils.IC_MONITOR.setQueueCount(ImageConverterPriority.INSTANT, m_instantProcessingCount.decrementAndGet());
                        }
                    }

                    int updatedQueueSize = 0;

                    synchronized (m_queuesImpl) {
                        final LinkedList<ImageConverterJob> queue = m_queuesImpl.get(priority);

                        // add job to appropriate, prioritized queue
                        // reset measurement time of job in order to retrieve the
                        // queue time, after which the job is taken from queue
                        job.initMeasurementTime();

                        queue.add(job);
                        updatedQueueSize = queue.size();
                    }

                    ImageConverterUtils.IC_MONITOR.setQueueCount(priority, updatedQueueSize);

                    if (LOG.isTraceEnabled()) {
                        LOG.trace(ImageConverterUtils.IC_STR_BUILDER().
                            append("entry count after put to ").
                            append(priority.name().toUpperCase()).
                            append(" queue: ").append(updatedQueueSize).toString());
                    }

                    m_processingMap.put(imageKey, job);
                    implSignalAvailability();

                    // job has just been added to queue => unlock job now, it will be locked
                    // again when taken from queue for processing; final unlocking of imageKey
                    // with UnlockMode.END_PROCESSING will happen when job is closed
                    IdLocker.unlock(imageKey);
                } catch (ImageConverterException e) {
                    LOG.warn("IC removing entry after occurence of an error: {}", imageKey);

                    if (null != job) {
                        implCloseJob(job, removeEntryInCaseOfError);
                    }

                    // unlock the imageKey with UnlockMode.END_PROCESSING in case of an error
                    IdLocker.unlock(imageKey, IdLocker.UnlockMode.END_PROCESSING);

                    throw e;
                }
            }

            if (hasRequestFormat) {
                try {
                    // if requestFormat is given, return any result we already have so far
                    return (null != ret) ? ret : implGetMetadataImage(imageKey, requestFormat);
                } catch (ImageConverterException e) {
                    LOG.warn(ImageConverterUtils.IC_STR_BUILDER().
                        append("queue could not get MetadataImage for ").
                        append(imageKey).append(": ").append(requestFormat).toString(),
                        Throwables.getRootCause(e));
                }
            }
        } finally {
            IdLocker.unlock(imageKey);
        }

        return null;
    }

    /**
     * @param imageKey
     * @param srcImageStm
     * @param context
     * @return
     */
    MetadataImage getMetadataImage(@NonNull final String imageKey, @NonNull final String requestFormat) throws ImageConverterException {
        ImageFormat requestTargetFormat = null;

        try {
            final boolean trace = LOG.isTraceEnabled();

            // try to lock key and get result from already queued job
            if (IdLocker.lock(imageKey, Mode.TRY_LOCK)) {
                try {
                    ImageConverterQueueEntry queueEntry = null;

                    // try to get job from queue
                    synchronized (m_queuesImpl) {
                        ImageConverterJob curJob = null;

                        // iterate over all prioritzed queues to check if there's a waiting job
                        for (final ImageConverterPriority curPriority : PRIORITY_ORDER_HIGH_TO_LOW) {
                            final LinkedList<ImageConverterJob> implQueue = m_queuesImpl.get(curPriority);
                            final Iterator<ImageConverterJob> iter = implQueue.iterator();

                            while (iter.hasNext()) {
                                curJob = iter.next();

                                if ((null != curJob) && imageKey.equals(curJob.getImageKey())) {
                                    // move job to highest priority queue
                                    if (PRIORITY_HIGHEST != curPriority) {
                                        iter.remove();
                                        m_queuesImpl.get(PRIORITY_HIGHEST).add(curJob);
                                    }

                                    queueEntry = new ImageConverterQueueEntry(curJob, curPriority);
                                    break;
                                }
                            }

                            if (null != queueEntry) {
                                break;
                            }
                        }
                    }

                    // job is queued => return requested format immediately;
                    // the job is asynchronously processed afterwards
                    if (null != queueEntry) {
                        if (trace) {
                            LOG.trace("IC queue synchronously processes job in getMetadataImage user request: {}", imageKey);
                        }

                        // get MetadataImage from job directly
                        final MetadataImage ret = queueEntry.getJob().processRequestFormat(requestFormat);

                        implSignalAvailability();

                        return ret;
                    }
                } finally {
                    IdLocker.unlock(imageKey);
                }
            }

            // image key is available in general, but not queued anymore =>
            // wait until outstanding job has been processed completely, if necessary
            if (trace) {
                LOG.trace("IC queue gets image/waits for finish of asynchronous job processing in getMetadataImage user request: {}", imageKey);
            }

            IdLocker.lock(imageKey, IdLocker.Mode.WAIT_IF_PROCESSED);

            try {
                if (m_imageConverter.isAvailable(imageKey)) {
                    return implGetMetadataImage(imageKey, requestFormat);
                }
            } finally {
                IdLocker.unlock(imageKey);
            }
        } catch (Exception e) {
            LOG.warn(ImageConverterUtils.IC_STR_BUILDER().
                append("queue could not get MetadataImage for ").
                append(imageKey).append(": ").
                append(requestFormat).append(" / ").
                append((null != requestTargetFormat) ? requestTargetFormat.getFormatString() : "n/a => Removing key!").toString(),
                Throwables.getRootCause(e));
        }

        return null;
    }

    /**
     * @param imageKey
    * @param srcImageStm
     * @param context
     * @return
     */
    IMetadata getMetadata(@NonNull final String imageKey) throws ImageConverterException {
        // if job is currently processed => get metadadata from still running job
        final ImageConverterJob job = m_processingMap.get(imageKey);

        if (null != job) {
            return job.getImageMetadata();
        }

        // try to read metada from existing entry
        IdLocker.lock(imageKey, IdLocker.Mode.WAIT_IF_PROCESSED);

        try {
            if (m_imageConverter.isAvailable(imageKey)) {
                return ImageConverterUtils.readMetadata(m_objectCache, m_metadataReader, imageKey, true);
            }
        } finally {
            IdLocker.unlock(imageKey);
        }

        return null;
    }

    /**
     *
     */
    void shutdown() {
        if (m_running.compareAndSet(true, false)) {
            final long shutdownStartTimeMillis = System.currentTimeMillis();
            final boolean trace = LOG.isTraceEnabled();

            if (trace) {
                LOG.trace("IC queue starting shutdown");
            }

            try {
                final List<Runnable> openJobList = m_executor.shutdown();

                if (null != openJobList) {
                    if (trace) {
                        LOG.trace("IC is closing already queued entries in shutdown: {}", openJobList.size());
                    }

                    // cleanup/close of all pending Runnables
                    openJobList.forEach((curJob) -> {
                        // close job, imageKey will be removed from processingMap as well
                        implCloseJob(((ImageConverterJob) curJob), true);
                    });
                }

                interrupt();
                m_imageProcessor.shutdown();
            } catch (@SuppressWarnings("unused") Exception e) {
                Thread.currentThread().interrupt();
            }

            LOG.trace("IC queue shutdown open Runnable count after shutdown: {}{}", m_processingMap.size());
            LOG.trace("IC queue shutdown finished in {}ms", Long.valueOf((System.currentTimeMillis() - shutdownStartTimeMillis)));
        }
    }

    /**
     * @return
     */
    boolean isRunning() {
        return m_running.get() && !interrupted();
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param imageKey
     * @return
     * @throws ImageConverterException
     */
    List<ImageFormat> implGetAvailableImageKeyFormats(@NonNull final String imageKey) throws ImageConverterException {
        ICacheObject[] fileItems = null;

        try {
            fileItems = m_objectCache.get(IMAGECONVERTER_GROUPID, imageKey);
        } catch (ObjectCacheException e) {
            throw new ImageConverterException(e);
        }

        if (null != fileItems) {
            final List<ImageFormat> imageFormats = new ArrayList<>(fileItems.length);

            for (final ICacheObject curFileItem : fileItems) {
                final String fileId = curFileItem.getFileId();

                if ((null != fileId) && !fileId.startsWith(ImageConverterUtils.IMAGECONVERTER_METADATA_FILEID)) {
                    imageFormats.add(ImageFormat.parseImageFormat(fileId));
                }
            }

            return imageFormats;
        }

        return EMPTY_IMAGEFORMAT_LIST;
    }

    /**
     * @return
     * @throws ImageConverterException
     */
    void implCloseJob(@NonNull final ImageConverterJob job, final boolean removePersistentData) {
        final String imageKey = job.getImageKey();

        try {
            if (removePersistentData) {
                m_objectCache.removeKey(IMAGECONVERTER_GROUPID, imageKey);
            }
        } catch (ObjectCacheException e) {
            LOG.trace("IC error while removing imageKey when closing Runnable: {}", imageKey, e);
        } finally {
            // close job itself
            ImageConverterUtils.close(job);

            // remove imageKey from processing map
            if (null != imageKey) {
                m_processingMap.remove(imageKey);
            }
        }

        LOG.trace("IC queue closed job: {}", imageKey);
    }

    // - Implementation --------------------------------------------------------

    private MetadataImage implGetMetadataImage(@NonNull final String imageKey, @NonNull final String requestFormat) throws ImageConverterException {
        // get all processed image formats and determine target request format
        final List<ImageFormat> availableImageFormats = implGetAvailableImageKeyFormats(imageKey);

        // if there're no available formats, the entry must be invalid
        if (availableImageFormats.size() > 0) {
            final IMetadata imageMetadata = ImageConverterUtils.readMetadata(m_objectCache, m_metadataReader, imageKey, true);

            if (null == imageMetadata) {
                return null;
            }

            // determine target format to retrieve
            final ImageFormat requestTargetFormat = ImageConverterUtils.getBestMatchingFormat(
                availableImageFormats,
                imageMetadata,
                ImageFormat.parseImageFormat(requestFormat));

            if (null != requestTargetFormat) {
                return new MetadataImage(ImageConverterUtils.readImageByTargetFormat(
                    m_objectCache, imageKey, requestTargetFormat, true), imageMetadata);
            }
        }

        throw new ImageConverterException("IC could not determine any valid MetadataImage based on given request format");
    }

    /**
     * @return
     */
    private boolean implHasQueuedJob() {
        synchronized (m_queuesImpl) {
            // iterate over all prioritzed queues to check if there's a waiting job
            for (final ImageConverterPriority curPriority : PRIORITY_ORDER_HIGH_TO_LOW) {
                if (!m_queuesImpl.get(curPriority).isEmpty()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return
     */
    protected ImageConverterQueueEntry implGetAndRemoveNextJob() {
        synchronized (m_queuesImpl) {
            ImageConverterJob job = null;

            // iterate over all prioritzed queues from highest to lowest
            // priority, in order to retrieve the next job candidate
            for (final ImageConverterPriority curPriority : PRIORITY_ORDER_HIGH_TO_LOW) {
                final LinkedList<ImageConverterJob> curQueue = m_queuesImpl.get(curPriority);

                if (!curQueue.isEmpty() && (null != (job = curQueue.remove()))) {
                    if (LOG.isTraceEnabled()) {
                        LOG.trace(ImageConverterUtils.IC_STR_BUILDER().
                                append("entry count after take from ").
                                append(curPriority.name().toUpperCase()).
                                append(" queue: ").append(curQueue.size()).toString());
                    }

                    // resource warning has been checked, job is closed
                    // after it will have been executed as FutureTask
                    return new ImageConverterQueueEntry(job, curPriority);
                }
            }
        }

        return null;
    }

    /**
     * Signal thread waiting on this condition to continue processing
     *
     */
    void implSignalAvailability() {
        try {
            m_lock.lock();
            m_jobAvailableConditon.signal();
        } finally {
            m_lock.unlock();
        }
    }

    // - Static members --------------------------------------------------------

    /**
     * EMPTY_IMAGEFORMAT_LIST
     */
    final private static List<ImageFormat> EMPTY_IMAGEFORMAT_LIST = new ArrayList<>();

    /**
     * PRIORITY_ORDER_HIGH_TO_LOW
     */
    final private static ImageConverterPriority[] PRIORITY_ORDER_HIGH_TO_LOW = {
        ImageConverterPriority.INSTANT,
        ImageConverterPriority.MEDIUM,
        ImageConverterPriority.BACKGROUND
    };

    /**
     * PRIORITY_LOW_TO_HIGH
     * clone PRIORITY_ORDER_HIGH_TO_LOW and reverse in static init.
     */
    final private static ImageConverterPriority[] PRIORITY_LOW_TO_HIGH = ArrayUtils.clone(PRIORITY_ORDER_HIGH_TO_LOW);

    /**
     * PRIORITY_HIGHEST
     */
    final private static ImageConverterPriority PRIORITY_HIGHEST = ImageConverterPriority.highest();

    static {
        ArrayUtils.reverse(PRIORITY_LOW_TO_HIGH);
    }

    // - Members ---------------------------------------------------------------

    final protected ConcurrentMap<String, ImageConverterJob> m_processingMap = new ConcurrentHashMap<>();

    final private ImageConverter m_imageConverter;

    final private ReentrantLock m_lock = new ReentrantLock(true);

    final private Condition m_jobAvailableConditon = m_lock.newCondition();

    final private IObjectCache m_objectCache;

    final private IMetadataReader m_metadataReader;

    final private List<ImageFormat> m_imageFormats;

    final private Map<ImageConverterPriority, LinkedList<ImageConverterJob>> m_queuesImpl = new HashMap<>();

    final private ImageConverterJobExecutor m_executor;

    final private AtomicBoolean m_running = new AtomicBoolean(true);

    final private ImageProcessor m_imageProcessor;

    final private AtomicLong m_instantProcessingCount = new AtomicLong(0);
}
