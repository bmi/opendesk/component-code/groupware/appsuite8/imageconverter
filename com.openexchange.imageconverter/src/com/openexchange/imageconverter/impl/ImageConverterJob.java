/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.impl;

import static com.openexchange.imageconverter.impl.ImageConverterUtils.LOG;
import java.awt.Dimension;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import com.openexchange.imageconverter.api.IMetadata;
import com.openexchange.imageconverter.api.IMetadataReader;
import com.openexchange.imageconverter.api.ImageConverterException;
import com.openexchange.imageconverter.api.ImageFormat;
import com.openexchange.imageconverter.api.MetadataException;
import com.openexchange.imageconverter.api.MetadataImage;
import com.openexchange.imagetransformation.ScaleType;
import com.openexchange.objectcache.api.IObjectCache;
import com.openexchange.objectcache.api.IWriteAccess;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.Nullable;
import com.openexchange.objectcache.api.ObjectCacheException;

/**
 * {@link ImageConverterJob}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.2
 */
class ImageConverterJob implements Runnable, Closeable {

    /**
     * Initializes a new {@link ImageConverterJob}.
     * @param imageKey
     * @param inputStm
     * @param imageFormats
     * @param queue
     * @param context
     * @param metadata
     * @throws ImageConverterException
     */
    ImageConverterJob(
        @NonNull final String imageKey,
        @NonNull final InputStream inputStm,
        @NonNull final List<ImageFormat> imageFormats,
        @NonNull ImageConverterQueue queue,
        @Nullable final String context) throws ImageConverterException {

        super();

        m_queue = queue;
        m_imageKey = imageKey;
        m_context = context;
        m_inputFile = ImageConverterUtils.createTempFile();

        if (null == m_inputFile) {
            throw new ImageConverterException("IC ConversionJob error while creating input file: " + imageKey);
        }

        try {
            // create tmp. input file from input stream, file is removed by calling close method
            FileUtils.copyInputStreamToFile(inputStm, m_inputFile);

            if (m_inputFile.length() < 1) {
                throw new IOException("IC ConversionJob creation of job not possible due to missing or empty input file: " + imageKey);
            }

            // read metadata from input file
            m_imageMetadata = implCreateKeyMetadata(queue.getMetadataReader(), m_imageKey, m_inputFile, m_context);

            if (null == m_imageMetadata) {
                throw new MetadataException("IC ConversionJob error while trying to read image metadata from input file: " + imageKey);
            }
        } catch (IOException | MetadataException e) {
            close();
            throw new ImageConverterException("IC ConversionJob error while creating ImageConverterJob", e);
        }

        m_imageFormats = implCreateImageFormatList(m_imageMetadata, imageFormats);
        implFillProcessMap();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.Closeable#close()
     */
    @Override
    public void close() {
        FileUtils.deleteQuietly(m_inputFile);
        m_inputFile = null;
        m_formatsToProcess.clear();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        final boolean trace = LOG.isTraceEnabled();
        ImageFormat curTargetFormat = null;

        if (trace) {
            LOG.trace("IC ConversionJob run started: {}", m_imageKey);
        }

        try {
            final List<ImageFormat> workingImageFormatsList = new LinkedList<>();

            // use a working list to allow arbitrary removal of entries from m_formatsToProcess
            // map while iterating over all open working List entries, created from map's keySet
            workingImageFormatsList.addAll(m_formatsToProcess.keySet());

            for (final Iterator<ImageFormat> iter = workingImageFormatsList.iterator(); iter.hasNext();) {
                curTargetFormat = iter.next();

                if (trace) {
                    LOG.trace(ImageConverterUtils.IC_STR_BUILDER().
                        append("ConversionJob worker thread asynchronously processing ").
                        append(m_imageKey).append(": ").
                        append(curTargetFormat.getFormatString()).toString());
                }

                implProcessKey(curTargetFormat, true);
            }
        } catch (Exception e) {
            LOG.error(ImageConverterUtils.IC_STR_BUILDER().
                append(m_imageKey).append(": ").
                append((null != curTargetFormat) ? curTargetFormat.getFormatString() :"null").toString(), e);
        } finally {
            ImageConverterUtils.IC_MONITOR.incrementProcessedKeys(m_processingTime);

            if (trace) {
                LOG.trace("IC ConversionJob run finished: {}", m_imageKey);
            }
        }
    }

    /**
     * @return
     */
    protected String getImageKey() {
        return m_imageKey;
    }

    /**
     *
     */
    protected void initMeasurementTime() {
        m_measurementStartTimeMillis = System.currentTimeMillis();
    }

    /**
     * @return
     */
    protected long getCurrentMeasurementTimeMillis() {
        return (System.currentTimeMillis() - m_measurementStartTimeMillis);
    }

    /**
     *
     */
    protected IMetadata getImageMetadata() {
        return m_imageMetadata;
    }

    /**
     * Process requested
     *
     * @param targetFormat
     * @return
     * @throws ImageConverterException
     */
    protected boolean implProcessKey(@NonNull final ImageFormat targetFormat, final boolean isAsync) throws ImageConverterException {
        ImageFormat processingFormat = m_formatsToProcess.get(targetFormat);
        final long subProcessingStartTimeMillis = System.currentTimeMillis();
        boolean ret = false;

        if (null == processingFormat) {
            // check first, if we need to create the reference format entry for the requested ScaleType
            final ImageFormat referenceFormat = m_referenceFormatMap.get(targetFormat.getScaleType());

            if ((null != referenceFormat) && (null != (processingFormat = m_formatsToProcess.get(referenceFormat)))) {
                ret = implCreateThumbnail(m_imageKey, m_inputFile, m_imageMetadata, referenceFormat, processingFormat, m_context, isAsync);

                if (ret) {
                    m_formatsToProcess.remove(referenceFormat);
                }
            } else {
                ret = true;
            }

            if (ret) {
                implCreatReferenceThumbnail(m_imageKey, m_imageMetadata, targetFormat, referenceFormat, m_context, isAsync);
            }
        } else {
            ret = implCreateThumbnail(m_imageKey, m_inputFile, m_imageMetadata, targetFormat, processingFormat, m_context, isAsync);
        }

        if (ret) {
            m_formatsToProcess.remove(targetFormat);
        }

        m_processingTime += (System.currentTimeMillis() - subProcessingStartTimeMillis);

        return ret;
    }

    /**
     * @param targetFormat
     * @return
     * @throws ImageConverterException
     */
    protected MetadataImage processRequestFormat(@NonNull final String requestFormat) throws ImageConverterException {
        ImageFormat requestTargetFormat = null;

        try {
            requestTargetFormat = ImageConverterUtils.getBestMatchingFormat(
                m_imageFormats,
                m_imageMetadata,
                ImageFormat.parseImageFormat(requestFormat));

            if (null == requestTargetFormat) {
                throw new ImageConverterException("IC ConversionJob is not able to parse requested image format: " + requestFormat);
            }

            // no need to do anything, if requested format has already been processed, otherwise process format
            if (!m_formatsToProcess.containsKey(requestTargetFormat) || implProcessKey(requestTargetFormat, false)) {
                return new MetadataImage(ImageConverterUtils.readImageByTargetFormat(
                    m_queue.getFileItemService(), m_imageKey, requestTargetFormat, false),
                    m_imageMetadata);
            }
        } catch (Exception e) {
            throw new ImageConverterException(ImageConverterUtils.IC_STR_BUILDER().
                append(m_imageKey).
                append(" / ").append(requestFormat).
                append(" / ").append((null != requestTargetFormat) ? requestTargetFormat.getFormatString() : "null").
                toString(), e);
        }

        return null;
    }

    // - Implementation --------------------------------------------------------

    /**
    *
    */
    private void implFillProcessMap() {
        // The process map is filled with 3 different kinds of key/value pairs:
        // 1. key and value are (!=null) and same: do a real image processing with the key format and store under key format
        // 2. key and value are (!=null) but different: do real image processing with the value format, but store under key format
        // 3. key is (!=null) and value is (==null): set only the reference property and store under key format without real image processing

        final Dimension imageDimension = m_imageMetadata.getImageDimension();

        for (final ImageFormat curImageFormat : m_imageFormats) {
            final ScaleType curScaleType = curImageFormat.getScaleType();
            final ImageFormat referenceFormat = m_referenceFormatMap.get(curScaleType);

            if (null != referenceFormat) {
                // if a reference format is set, we need to check, if
                // the current format image result matches the
                // reference format result, in order to properly
                // use the reference format
                if (ImageConverterUtils.shouldUseReferenceFormat(imageDimension, curImageFormat)) {
                    m_formatsToProcess.put(curImageFormat, null);
                } else {
                    m_formatsToProcess.put(curImageFormat, curImageFormat);
                }

            } else if (ImageConverterUtils.shouldUseReferenceFormat(imageDimension, curImageFormat)) {
                // the imageFormat at the first reference format position is substituted with the original size format
                final ImageFormat originalFormat = ImageFormat.createFrom(curImageFormat.getFormatShortName(),
                    true,
                    imageDimension.width,
                    imageDimension.height,
                    curImageFormat.getScaleType(),
                    false,
                    curImageFormat.getQuality());

                m_referenceFormatMap.put(curScaleType, curImageFormat);
                m_formatsToProcess.put(curImageFormat, originalFormat);
            } else {
                m_formatsToProcess.put(curImageFormat, curImageFormat);
            }
        }
    }

    /**
     * @return
     * @throws MetadataException
     */
    private IMetadata implCreateKeyMetadata(@NonNull final IMetadataReader metadataReader, @NonNull final String imageKey, @NonNull final File inputFile, @Nullable final String context) throws MetadataException {
        IMetadata ret = metadataReader.readMetadata(inputFile);

        if (null != ret) {
            try (final IWriteAccess fileWriteAcess = m_queue.getFileItemService().getWriteAccess(
                ImageConverterUtils.IMAGECONVERTER_GROUPID,
                imageKey,
                ImageConverterUtils.IMAGECONVERTER_METADATA_FILEID)) {

                if (null != fileWriteAcess) {
                    try (final PrintWriter printWriter = new PrintWriter(fileWriteAcess.getOutputStream())) {
                        printWriter.write(ret.getJSONObject().toString());
                    }

                    implSetProperties(fileWriteAcess, context, null);

                    if (LOG.isTraceEnabled()) {
                        final Dimension imageDimension = ret.getImageDimension();
                        final String formatName = ret.getImageFormatName();

                        LOG.trace(ImageConverterUtils.IC_STR_BUILDER().
                            append("[").append(imageDimension.width).append('x').append(imageDimension.height).append("] ").
                            append(formatName).append(" image: ").
                            append(imageKey).toString());
                    }
                }
            } catch (Exception e) {
                LOG.error("IC error while creating metadata entry: {}", imageKey, e);

                implRemoveFile(imageKey, ImageConverterUtils.IMAGECONVERTER_METADATA_FILEID);
                ret = null;
            }
        }

        return ret;
    }

    /**
     * @param writeAccess
     * @param referencePropertyValue
     * @throws ObjectCacheException
     */
    private static void implSetProperties(@NonNull final IWriteAccess writeAccess, @Nullable final String context, @Nullable final String referencePropertyValue) throws ObjectCacheException {
        if (null != context) {
            writeAccess.setKeyValue(ImageConverterUtils.IMAGECONVERTER_KEY_CONTEXT, context);
        }

        if (null != referencePropertyValue) {
            writeAccess.setKeyValue(ImageConverterUtils.IMAGECONVERTER_KEY_REFERENCE, referencePropertyValue);
        }
    }

    /**
     * @param targetFormat
     * @param processingFormat
     * @throws ImageConverterException
     */
    private boolean implCreateThumbnail(@NonNull final String imageKey, @NonNull final File inputFile,
        @NonNull final IMetadata imageMetadata,
        @NonNull final ImageFormat targetFormat,
        @NonNull final ImageFormat processingFormat,
        @Nullable final String context,
        final boolean isAsync) throws ImageConverterException {

        final IObjectCache objectCache = m_queue.getFileItemService();
        final String targetFormatStr = targetFormat.getFormatString();
        final ImageFormat usedProcessingFormat = (null != processingFormat) ? processingFormat : targetFormat;
        boolean ret = false;

        try (final IWriteAccess fileWriteAccess = objectCache.getWriteAccess(ImageConverterUtils.IMAGECONVERTER_GROUPID, imageKey, targetFormatStr)) {

            if ((null != fileWriteAccess) && m_queue.getImageProcessor().scale(inputFile, fileWriteAccess.getOutputFile(), usedProcessingFormat)) {
                implSetProperties(fileWriteAccess, context, null);
                ret = true;
            }

            if (LOG.isTraceEnabled()) {
                final Dimension imageDimension = imageMetadata.getImageDimension();
                final String imageFormatShortName = imageMetadata.getImageFormatName();
                final StringBuilder traceMsgBuilder = ImageConverterUtils.IC_STR_BUILDER().
                    append("image processor ").
                    append(isAsync ? "a" : "").append("synchronously").
                    append(ret ? " scaled image to create " : " could not scale image to create ").
                    append(imageFormatShortName).append(" thumbnail for ");

                LOG.trace(ImageConverterUtils.IC_STR_BUILDER().
                    append(traceMsgBuilder).
                    append(implGetImageInformationString(imageKey, imageDimension, imageFormatShortName, targetFormatStr, usedProcessingFormat.getFormatString())).toString());
            }
        } catch (@SuppressWarnings("unused") Exception e) {
            final Dimension imageDimension = imageMetadata.getImageDimension();
            final String imageFormatShortName = imageMetadata.getImageFormatName();

            LOG.error(ImageConverterUtils.IC_STR_BUILDER().
                append("error while ").append(isAsync ? "a" : "").
                append("synchronously creating ").append(imageFormatShortName).append(" thumbnail for ").
                append(implGetImageInformationString(imageKey, imageDimension, imageFormatShortName, targetFormatStr, usedProcessingFormat.getFormatString())).toString());
        } finally {
            if (!ret) {
                implRemoveFile(imageKey, targetFormatStr);
            }
        }

        return ret;
    }

    /**
     * @param targetFormat
     * @throws ImageConverterException
     */
    private void implCreatReferenceThumbnail(@NonNull final String imageKey,
        @NonNull final IMetadata imageMetadata,
        @NonNull final ImageFormat targetFormat,
        @NonNull final ImageFormat referenceFormat,
        @Nullable final String context,
        final boolean isAsync) throws ImageConverterException {

        // we create a reference entry in case the shrinkOnly Flag
        // is set and the scaling would expand the source format
        final String targetFormatStr = targetFormat.getFormatString();
        final String referenceFormatStr = referenceFormat.getFormatString();

        try (final IWriteAccess fileWriteAccess = m_queue.
            getFileItemService().
            getWriteAccess(ImageConverterUtils.IMAGECONVERTER_GROUPID, imageKey, targetFormatStr)) {

            implSetProperties(fileWriteAccess, context, referenceFormatStr);

            if (LOG.isTraceEnabled()) {
                final Dimension imageDimension = imageMetadata.getImageDimension();
                final String imageFormatShortName = imageMetadata.getImageFormatName();

                LOG.trace(ImageConverterUtils.IC_STR_BUILDER().
                    append(isAsync ? "a" : "").
                    append("synchronously created reference entry for ").
                    append(implGetImageInformationString(imageKey, imageDimension, imageFormatShortName, targetFormatStr, referenceFormatStr)).toString());
            }
        } catch (@SuppressWarnings("unused") Exception e) {
            implRemoveFile(imageKey, targetFormatStr);

            final Dimension imageDimension = imageMetadata.getImageDimension();
            final String imageFormatShortName = imageMetadata.getImageFormatName();

            LOG.error(ImageConverterUtils.IC_STR_BUILDER().
                append("error while ").append(isAsync ? "a" : "").
                append("synchronously creating reference entry for ").
                append(implGetImageInformationString(imageKey, imageDimension, imageFormatShortName, targetFormatStr, referenceFormatStr)).toString());
        }
    }

    /**
     * @param imageKey
     * @param fileId
     */
    private void implRemoveFile(@NonNull final String imageKey, @NonNull final String fileName) {
        try {
            m_queue.getFileItemService().remove(ImageConverterUtils.IMAGECONVERTER_GROUPID, imageKey, fileName);
        } catch (ObjectCacheException e) {
            if (LOG.isTraceEnabled()) {
                LOG.trace(ImageConverterUtils.IC_STR_BUILDER().
                    append(imageKey).append(": ").
                    append(fileName).toString(), e);
            }
        }
    }

    /**
     * @param metadata
     * @param imageFormats
     * @return
     */
    private static List<ImageFormat> implCreateImageFormatList(@NonNull final IMetadata metadata, @NonNull final List<ImageFormat> imageFormats) {
        final List<ImageFormat> ret = new ArrayList<>();
        final String imageFormatShortName = metadata.getImageFormatName();
        final boolean isAnimated = imageFormatShortName.startsWith("gif");
        final boolean isTransparent = isAnimated || imageFormatShortName.startsWith("png") || imageFormatShortName.startsWith("tif");

        for (final ImageFormat curFormat : imageFormats) {
            ImageFormat.ImageType targetFormatImageType = null;

            switch (curFormat.getFormatShortName()) {
                case "jpg": {
                    targetFormatImageType = ImageFormat.ImageType.JPG;
                    break;
                }

                case "png": {
                    targetFormatImageType = ImageFormat.ImageType.PNG;
                    break;
                }

                case "auto":
                default: {
                    targetFormatImageType = isAnimated ?
                        // TODO (KA) add GIF format to possible target types???
                        ImageFormat.ImageType.PNG :
                            (isTransparent  ? ImageFormat.ImageType.PNG : ImageFormat.ImageType.JPG);
                    break;
                }
            }

            final ImageFormat targetImageFormat = new ImageFormat(targetFormatImageType);

            targetImageFormat.setAutoRotate(curFormat.isAutoRotate());
            targetImageFormat.setWidth(curFormat.getWidth());
            targetImageFormat.setHeight(curFormat.getHeight());
            targetImageFormat.setScaleType(curFormat.getScaleType());
            targetImageFormat.setShrinkOnly(curFormat.isShrinkOnly());

            ret.add(targetImageFormat);
        }

        return ret;
    }

    /**
     * @param imageKey
     * @param imageDimension
     * @param imageFormatShortName
     * @param targetFormatStr
     * @param usedProcessingFormat
     * @return
     */
    private StringBuilder implGetImageInformationString(
        final String imageKey,
        final Dimension imageDimension,
        final String imageFormatShortName,
        final String targetFormatStr,
        final String usedProcessingFormatStr) {

        return ImageConverterUtils.STR_BUILDER().
            append("[").append(imageDimension.width).append('x').append(imageDimension.height).append("] ").
            append(imageFormatShortName).append(" image: ").
            append(imageKey).append(" / ").append(targetFormatStr).
            append(" (Used physical format: ").append(usedProcessingFormatStr).append(')');
    }

    // - Members ---------------------------------------------------------------

    final private String m_imageKey;

    final private List<ImageFormat> m_imageFormats;

    final private ImageConverterQueue m_queue;

    final private String m_context;

    final private IMetadata m_imageMetadata;

    final private Map<ImageFormat, ImageFormat> m_formatsToProcess = Collections.synchronizedMap(new LinkedHashMap<>());

    final private Map<ScaleType, ImageFormat> m_referenceFormatMap = new HashMap<>(ScaleType.values().length);

    private File m_inputFile;

    private long m_measurementStartTimeMillis = 0;

    private long m_processingTime = 0;
}
