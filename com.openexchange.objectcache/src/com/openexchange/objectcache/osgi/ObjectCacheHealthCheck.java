/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.objectcache.osgi;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;
import org.json.JSONObject;
import com.openexchange.objectcache.api.IObjectCache;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.ObjectCacheConfig;

/**
 * {@link ObjectCacheHealthCheck }
 *
 * @since v8.0.0
 */
public class ObjectCacheHealthCheck implements HealthCheck {

    /**
     * OBJECTCACHE_HEALTH_CHECK
     */
    private static final String OBJECTCACHE_HEALTH_CHECK = "ObjectCacheHealthCheck";

    /**
     * Initializes a new {@link DCHealthCheck}.
     * @param client
     */
    public ObjectCacheHealthCheck(
        @NonNull final IObjectCache objectCache,
        @NonNull final ObjectCacheConfig objectCacheConfig) {

        m_objectCache = objectCache;
        m_objectCacheConfig = objectCacheConfig;
    }

    /**
     *
     */
    @Override
    public HealthCheckResponse call() {
        final JSONObject healthJSONObject = m_objectCache.getHealth();
        final HealthCheckResponseBuilder healthResponseBuilder = HealthCheckResponse.builder().
            name(OBJECTCACHE_HEALTH_CHECK).
            withData(IObjectCache.OBJECTCACHE_DATABASE_AVAILABLE,
                healthJSONObject.optBoolean(IObjectCache.OBJECTCACHE_DATABASE_AVAILABLE)).
            withData(IObjectCache.OBJECTCACHE_DATABASE_CONNECTION,
                healthJSONObject.optString(IObjectCache.OBJECTCACHE_DATABASE_CONNECTION)).
            withData(IObjectCache.OBJECTCACHE_OBJECTSTORE_AVAILABLE,
                healthJSONObject.optBoolean(IObjectCache.OBJECTCACHE_OBJECTSTORE_AVAILABLE)).
            withData(IObjectCache.OBJECTCACHE_OBJECTSTORE_IDS,
                healthJSONObject.optString(IObjectCache.OBJECTCACHE_OBJECTSTORE_IDS));

        // UP/DOWN status
        if (m_objectCache.isValid()) {
            healthResponseBuilder.up();
        } else {
            healthResponseBuilder.down();
        }

        return healthResponseBuilder.build();
    }

    // - Members ---------------------------------------------------------------

    @NonNull final private IObjectCache m_objectCache;

    @NonNull final private ObjectCacheConfig m_objectCacheConfig;
}
