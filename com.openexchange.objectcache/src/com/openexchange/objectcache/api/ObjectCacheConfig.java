/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.objectcache.api;

import java.io.File;
import java.util.LinkedHashSet;
import java.util.Properties;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;

/**
 * {@link Config} (thread safe, read only class to be intialized via {@link Builder})
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class ObjectCacheConfig {

    /**
     * {@link Builder} (not thread safe)
     *
     *
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.0.0
     */
    public static class Builder {

        /**
         * Initializes a new {@link Builder}.
         */
        public Builder() {
            super();

            m_dbProperties.putAll(DB_DEFAULT_PROPERTIES);
        }

        /**
         * @return
         */
        public ObjectCacheConfig build() {
            final ObjectCacheConfig config = new ObjectCacheConfig();

            config.m_objectStoreIdSet = m_objectStoreIdSet;
            config.m_dbHost= m_dbHost;
            config.m_dbPort= m_dbPort;
            config.m_dbSchema = m_dbSchema;
            config.m_dbUser = m_dbUser;
            config.m_dbPassword= m_dbPassword;
            config.m_dbProperties = m_dbProperties;
            config.m_dbPoolMaxSize = m_dbPoolMaxSize;
            config.m_dbPoolConnectTimeoutMillis = m_dbPoolConnectTimeoutMillis;
            config.m_dbPoolIdleTimeoutMillis = m_dbPoolIdleTimeoutMillis;
            config.m_dbPoolMaxLifetimeMillis = m_dbPoolMaxLifetimeMillis;
            config.m_spoolPath = m_spoolPath;

            return config;
        }

        /**
         * @param objectStoreIds
         * @return
         */
        public Builder withObjectStoreIds(final Set<String> objectStoreIds) {
            m_objectStoreIdSet.addAll(objectStoreIds);
            return this;
        }

        /**
         * @param dbHost
         * @return
         */
        public Builder withDBHost(final String dbHost) {
            m_dbHost = dbHost;
            return this;
        }

        /**
         * @param dbHost
         * @return
         */
        public Builder withDBPort(final int dbPort) {
            m_dbPort = dbPort;
            return this;
        }

        /**
         * @param dbSchema
         * @return
         */
        public Builder withDBSchema(final String dbSchema) {
            m_dbSchema = dbSchema;
            return this;
        }

        /**
         * @param dbUser
         * @return
         */
        public Builder withDBUser(final String dbUser) {
            m_dbUser = dbUser;
            return this;
        }

        /**
         * @param dbPassword
         * @return
         */
        public Builder withDBPassword(final String dbPassword) {
            m_dbPassword = dbPassword;
            return this;
        }

        /**
         * @param dbProperties
         * @return
         */
        public Builder withDBProperties(final Properties dbProperties) {
            m_dbProperties.putAll(dbProperties);
            return this;
        }

        /**
         * @param dbPoolMaxSize
         * @return
         */
        public Builder withDBPoolMaxSize(final int dbPoolMaxSize) {
            m_dbPoolMaxSize = dbPoolMaxSize;
            return this;
        }

        /**
         * @param dbPoolConnectTimeoutMillis
         * @return
         */
        public Builder withDBPoolConnectTimeoutMillis(final int dbPoolConnectTimeoutMillis) {
            m_dbPoolConnectTimeoutMillis = dbPoolConnectTimeoutMillis;
            return this;
        }

        /**
         * @param dbPoolIdleTimeoutMillis
         * @return
         */
        public Builder withDBPoolIdleTimeoutMillis(final int dbPoolIdleTimeoutMillis) {
            m_dbPoolIdleTimeoutMillis = dbPoolIdleTimeoutMillis;
            return this;
        }

        /**
         * @param dbPoolMaxLifetimeMillis
         * @return
         */
        public Builder withDBPoolMaxLifetimeMillis(final int dbPoolMaxLifetimeMillis) {
            m_dbPoolMaxLifetimeMillis = dbPoolMaxLifetimeMillis;
            return this;
        }

        /**
         * @param spoolPath
         * @return
         */
        public Builder withSpoolPath(final File spoolPath) {
            m_spoolPath = spoolPath;
            return this;
        }

        // - Members ---------------------------------------------------------------

        private Set<String> m_objectStoreIdSet = new LinkedHashSet<>();

        private String m_dbHost = null;

        private int m_dbPort = 3306;

        private String m_dbSchema = "objectcachedb";

        private String m_dbUser = null;

        private String m_dbPassword = null;

        private Properties m_dbProperties = new Properties();

        private int m_dbPoolMaxSize = 30;

        private int m_dbPoolConnectTimeoutMillis = 10000;

        private int m_dbPoolIdleTimeoutMillis = 300000;

        private int m_dbPoolMaxLifetimeMillis = 600000;

        private File m_spoolPath = new File("/var/spool/open-xchange/objectcache");

        // - Static members ----------------------------------------------------

        final private static Properties DB_DEFAULT_PROPERTIES = new Properties();

        static {
            // initialization of static members
            DB_DEFAULT_PROPERTIES.put("useUnicode", "true");
            DB_DEFAULT_PROPERTIES.put("characterEncoding", "UTF-8");
            DB_DEFAULT_PROPERTIES.put("autoReconnect","false");
            DB_DEFAULT_PROPERTIES.put("useServerPrepStmts", "true");
            DB_DEFAULT_PROPERTIES.put("useTimezone", "true");
            DB_DEFAULT_PROPERTIES.put("serverTimezone", "UTC");
            DB_DEFAULT_PROPERTIES.put("connectTimeout", "10000");
            DB_DEFAULT_PROPERTIES.put("socketTimeout", "300000");
            DB_DEFAULT_PROPERTIES.put("prepStmtCacheSize", "250");
            DB_DEFAULT_PROPERTIES.put("prepStmtCacheSqlLimit", "4096");
            DB_DEFAULT_PROPERTIES.put("cachePrepStmts", "true");
            DB_DEFAULT_PROPERTIES.put("useSSL", "false");
            DB_DEFAULT_PROPERTIES.put("requireSSL", "false");
            DB_DEFAULT_PROPERTIES.put("verifyServerCertificate", "false");
            DB_DEFAULT_PROPERTIES.put("enabledTLSProtocols", "TLSv1,TLSv1.1,TLSv1.2");
            DB_DEFAULT_PROPERTIES.put("clientCertificateKeyStoreUrl", StringUtils.EMPTY);
            DB_DEFAULT_PROPERTIES.put("clientCertificateKeyStorePassword", StringUtils.EMPTY);
            DB_DEFAULT_PROPERTIES.put("clientCertificateKeyStoreType", StringUtils.EMPTY);
            DB_DEFAULT_PROPERTIES.put("trustCertificateKeyStoreUrl", StringUtils.EMPTY);
            DB_DEFAULT_PROPERTIES.put("trustCertificateKeyStorePassword", StringUtils.EMPTY);
            DB_DEFAULT_PROPERTIES.put("trustCertificateKeyStoreType", StringUtils.EMPTY);
        }
    }

    /**
     * Initializes a new {@link Config}.
     */
    private ObjectCacheConfig() {
        super();
    }

    /**
     * @return
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * @return <code>true</code> if all necessary config values are set, <code>false</code> otherwise
     */
    public boolean isValid() {
        return ((null != m_objectStoreIdSet) && (m_objectStoreIdSet.size() > 0)) &&
            StringUtils.isNotBlank(m_dbHost) &&
            StringUtils.isNotBlank(m_dbSchema) &&
            StringUtils.isNotBlank(m_dbUser) &&
            StringUtils.isNotBlank(m_dbPassword);
    }

    /**
     * @return
     */
    public Set<String> getObjectStoreIds() {
        return m_objectStoreIdSet;
    }

    /**
     * @return
     */
    public String getDBHost() {
        return m_dbHost;
    }

    /**
     * @return
     */
    public int getDBPort() {
        return m_dbPort;
    }

    /**
     * @return
     */
    public String getDBSchema() {
        return m_dbSchema;
    }

    /**
     * @return
     */
    public String getDBUser() {
        return m_dbUser;
    }

    /**
     * @return
     */
    public String getDBPassword() {
        return m_dbPassword;
    }

    /**
     * @return
     */
    public Properties getDBProperties() {
        return m_dbProperties;
    }

    /**
     * @return
     */
    public int getDBPoolMaxSize() {
        return m_dbPoolMaxSize;
    }

    /**
     * @return
     */
    public int getDBPoolConnectTimeoutMillis() {
        return m_dbPoolConnectTimeoutMillis;
    }

    /**
     * @return
     */
    public int getDBPoolIdleTimeoutMillis() {
        return m_dbPoolIdleTimeoutMillis;
    }

    /**
     * @return
     */
    public int getDBPoolMaxLifetimeMillis() {
        return m_dbPoolMaxLifetimeMillis;
    }

    /**
     * @return
     */
    public File getSpoolPath() {
        return m_spoolPath;
    }

    // - Members ---------------------------------------------------------------

    private Set<String> m_objectStoreIdSet;

    private String m_dbHost;

    private int m_dbPort;

    private String m_dbSchema;

    private String m_dbUser;

    private String m_dbPassword;

    private Properties m_dbProperties;

    private int m_dbPoolMaxSize;

    private int m_dbPoolConnectTimeoutMillis;

    private int m_dbPoolIdleTimeoutMillis;

    private int m_dbPoolMaxLifetimeMillis;

    private File m_spoolPath;
}
