/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.objectcache.api;

import java.io.File;
import java.util.Set;

/**
 * {@link IObjectStore}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public interface IObjectStore {

    /**
     * @return
     */
    int getId();

    /**
     * @param inputFile The content of the object to create from as file.
     *  Might be null or not readable in which case no object is created.
     * @return The id of the created object or <code>null</code> if input file is <code>null</code>
     *  or not readable.
     * @throws ObjectCacheException
     */
    String createObject(File inputFile) throws ObjectCacheException;

    /**
     * @param objectId The id of the object to update.
     * @param inputFile The new content of the object that replaces the old content as file.
     *  Might be <code>null</code> or nor readable in which case no update is performed.
     * @throws ObjectCacheException
     */
    void updateObject(String objectId, File inputFile) throws ObjectCacheException;

    /**
     * @param objectId The id of the object to retrieve data from.
     * @param outputFile The content outputfile of the given object to write to.
     * @throws ObjectCacheException
     */
    void getObject(String objectId, File outputFile) throws ObjectCacheException;

    /**
     * @param objectId The id of the object to delete.
     * @throws ObjectCacheException
     */
    void deleteObject(String objectId) throws ObjectCacheException;

    /**
     * @param objectIds The ids of the objects to delete.
     * @return The {@link Set} of object ids that could not be deleted.
     * @throws ObjectCacheException
     */
    Set<String> deleteObjects(String[] objectIds) throws ObjectCacheException;
}
