/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.objectcache.api;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * {@link IReadAccess}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public interface IReadAccess extends IBaseAccess {

    /**
     * Returns the {@link InputStream} to read content from.</br>
     * The returned {@link InputStream} instance is owned by the caller and
     * needs to be closed appropriately after final usage.
     *
     * @return The {@link InputStream} to read content from.
     */
    InputStream getInputStream() throws IOException;

    /**
     * Returns the {@link File} to read content from.
     * The ownership of the returned {@link File} does not (!)
     * change and as such the file is automatically deleted when
     * closing the current {@link IReadAccess} instance.
     *
     * @return The {@link File} to read content from.
     * @throws IOException
     */
    File getInputFile() throws IOException;
}
