/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.objectcache.api;

import java.io.Closeable;
import java.util.Properties;
import java.util.function.Consumer;
import org.json.JSONObject;
import com.openexchange.osgi.annotation.SingletonService;

/**
 * {@link IObjectCache}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
/**
 * {@link IObjectCache}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
@SingletonService
public interface IObjectCache {

    /**
     * OBJECTCACHE_DATABASE_AVAILABLE
     */
    String OBJECTCACHE_DATABASE_AVAILABLE = "databaseAvailable";

    /**
     * OBJECTCACHE_DATABASE_CONNECTION
     */
    String OBJECTCACHE_DATABASE_CONNECTION = "databaseConnection";

    /**
     * OBJECTCACHE_OBJECTSTORE_AVAILABLE
     */
    String OBJECTCACHE_OBJECTSTORE_AVAILABLE = "objectStoreAvailable";

    /**
     * OBJECTCACHE_OBJECTSTORE_IDS
     */
    String OBJECTCACHE_OBJECTSTORE_IDS = "objectStoreIds";


    /**
     * {@link DatabaseType}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.0
     */
    public enum DatabaseType {
        /**
         * No database is available
         */
        NONE,

        /**
         * A database, that supports only standard SQL queries.
         */
        STANDARD_SQL,

        /**
         * A database, that supports standard SQL queries as well as MySQL specific queries.
         */
        MYSQL
    }

    /**
     * Getting the current status of the implementing service.
     *
     * @return <code>true</code> in case the implementing service is valid,
     *  <code>false</code> otherwise.
     */
    default boolean isValid() {
        return true;
    }

    /**
     * @return
     */
    boolean hasObjectStore();

    /**
     * @return
     */
    boolean hasDatabase();

    /**
     * Retrieving the type of database, internally used to store and retrieve file metadata.
     *
     * @return The {@link DatabaseType}.
     */
    DatabaseType getDatabaseType();

    /**
     * @return
     */
    JSONObject getHealth();

    /**
     * Registering a group with possible custom keys, that can be used as user
     * defined properties of a CacheObject for the group.
     * The registering of a group with its custom keys needs to be done prior to the usage of
     * that group and key(s) when accessing the {@link IReadAccess#getKeyValue(String)}
     *
     * @param objectCacheConfig The configuration to register the group with
     * @return The updated  {@link GroupConfig} after registration
     * @throws ObjectCacheException
     */
    GroupConfig registerGroup(GroupConfig groupConfig) throws ObjectCacheException;

    /**
     * @param groupId The groupId for which the group configuration is to be retrieved
     * @return The {@link GroupConfig} for the requestes groupId
     */
    GroupConfig getGroupConfig(String groupId);

    /**
     * Shutting down a group when it is not to be used anymore for the current instance
     *
     * @param groupId The group id of the group to shutdown
     */
    void shutdownGroup(String groupId);

    /**
     * Getting the array of custom keys, registered for a specific group.
     *
     * @param groupId The groupId for which all custom keys are to be retrieved
     * @return The array of custom keys for the given group
     * @throws ObjectCacheException
     */
    String[] getCustomKeys(String groupId) throws ObjectCacheException;

    /**
     * Querying, if a custom key is already registered for the group and thus can be used
     *
     * @param groupId The groupId to query for the given custom key
     * @param customKey The custom key to query the group for.
     * @return <code>true</code>, if the given key has been registered for the group,
     *  <code>false</code> otherwise
     */
    boolean hasCustomKey(String groupId, String customKey) throws ObjectCacheException;

    /**
     * Querying the user data for the group.
     *
     * @param groupId The groupId of the group to query for user data
     * @return The user data as {@link JSONObject}.
     *  If no user data is set, an empty object will be returned.
     */
    JSONObject getUserData(String groupId) throws ObjectCacheException;

    // -------------------------------------------------------------------------

    /**
     * Querying, if a group is contained in the collection.
     *
     * @param groupId
     * @return true, if the collections contains the group with the given id
     * @throws ObjectCacheException
     */
    boolean containsGroup(String groupId) throws ObjectCacheException;

    /**
     * Querying, if a key is contained in the collection.
     *
     * @param groupId
     * @param keyId
     * @return true, if the collections contains the group-key with the given ids
     * @throws ObjectCacheException
     */
    boolean containsKey(String groupId, String keyId) throws ObjectCacheException;

    /**
     * Querying, if a file is contained in the collection.
     *
     * @param groupId
     * @param keyId
     * @return true, if the collections contains the group-key with the given id
     * @throws ObjectCacheException
     */
    boolean contains(String groupId, String keyId, String fileId) throws ObjectCacheException;

    // -------------------------------------------------------------------------

    /**
     * Getting the {@link ICacheObject} interface for the given file item.
     * If no such item exists, <code>null</code> is returned.
     *
     * @return The {@link ICacheObject} interface or <code>null</code>.
     * @throws ObjectCacheException
     */
    ICacheObject get(String groupId, String keyId, String fileId) throws ObjectCacheException;

    /**
     * Getting all {@link ICacheObject} interfaces of the given group-key as an array.
     *
     * @param groupId
     * @param keyId
     * @return The array of {@link ICacheObject} interfaces
     * @throws ObjectCacheException
     */
    ICacheObject[] get(String groupId, String keyId) throws ObjectCacheException;

    /**
     * Getting all {@link ICacheObject} interfaces of the given group,
     * that match the given search properties, as an array.
     *
     * @param groupId
     * @return The array of key ids
     * @throws ObjectCacheException
     */
    ICacheObject[] get(String groupId, Properties properties) throws ObjectCacheException;

    /**
     * Getting the number of distinct key ids.
     *
     * @return The number of distinct key ids.
     * @throws ObjectCacheException
     */
    long getKeyCount(String groupId) throws ObjectCacheException;

    /**
     * Getting all key ids of the given group
     *
     * @param groupId
     * @return The array of key ids
     * @throws ObjectCacheException
     */
    String[] getKeys(String groupId) throws ObjectCacheException;

    /**
     * Iterate over all key ids, satisfying the optionally given WHERE and/or LIMIT clauses
     * The clauses are to be given as SQL expression without (!) the keywords WHERE and LIMIT.
     * The iteration is done in descending order of the age of the keys (=> oldest keys first)
     *
     * @param groupId The GroupId for which the query is to be performed.
     * @param whereClause The SQL 'WHERE' clause without the 'WHERE keyword. This parameter can be {@code null}.
     * @param whereClause The SQL 'LIMIT' clause without the 'LIMIT' keyword. This parameter can be {@code null}.
     * @param keyConsumer The {@link Consumer}, receiving each single {@link KeyObject} instance interface.
     * @return The number of found key items.
     * @throws ObjectCacheException
     */
    long iterateKeysByDescendingAge(String groupId,
        @Nullable String whereClause,
        @Nullable String limitClause,
        Consumer<KeyObject> keyConsumer) throws ObjectCacheException;

    /**
     * Getting the number of distinct group ids.
     *
     * @return The number of distinct group ids.
     * @throws ObjectCacheException
     */
    long getGroupCount() throws ObjectCacheException;

    /**
     * Getting all group ids of the collection
     *
     * @return The array of group ids
     * @throws ObjectCacheException
     */
    String[] getGroups() throws ObjectCacheException;

    /**
     * Getting the summed up length of all items within the given group
     *
     * @return The total length
     * @throws ObjectCacheException
     */
    long getGroupSize(String groupId) throws ObjectCacheException;

    /**
     * Getting the summed up length of all items within the given group,
     * satisfying the given properties
     *
     * @return The total length
     * @throws ObjectCacheException
     */
    long getGroupSize(String groupId, Properties properties) throws ObjectCacheException;

    /**
     * Getting the summed up length of all items within the given group and key,
     *
     * @return The total length
     * @throws ObjectCacheException
     */
    long getKeySize(String groupId, String keyId) throws ObjectCacheException;

    /**
     * Getting the summed up length of all items within the given group and key,
     * satisfying the given properties
     *
     * @return The total length
     * @throws ObjectCacheException
     */
    long getKeySize(String groupId, String keyId, Properties properties) throws ObjectCacheException;

    /**
     * Returns the age in milliseconds of the oldest key by modification date within the group.
     *
     * @param groupId
     * @return The oldest key age of the given group in milliseconds, based on its modification date.<br>
     *  Returns <code>-1</code> in case no entry is availble
     * @throws ObjectCacheException
     */
    long getMaxKeyAgeMillis(String groupId) throws ObjectCacheException;

    // -------------------------------------------------------------------------

    /**
     * removing a single file item
     *
     * @param cacheObject
     * @return true, if the file item was removed; false otherwise
     * @throws ObjectCacheException
     */
    boolean remove(ICacheObject cacheObject) throws ObjectCacheException;

    /**
     * removing a single file item
     *
     * @param groupId
     * @param keyId
     * @param fileId
     * @return true, if the file item was removed; false otherwise
     * @throws ObjectCacheException
     */
    boolean remove(String groupId, String keyId, String fileId) throws ObjectCacheException;

    /**
     * removing an array of file items
     *
     * @param fileElements
     * @return The number of removed file items
     * @throws ObjectCacheException
     */
    int remove(ICacheObject[] fileElements) throws ObjectCacheException;

    /**
     * removing all file item, whose properties satisfy the search properties
     *
     * @param groupId
     * @param properties
     * @return The number of removed file items
     * @throws ObjectCacheException
     */
    int remove(String groupId, Properties properties) throws ObjectCacheException;

    /**
     * Removing all file items with the given group-subgruop
     *
     * @param groupId
     * @param keyId
     * @return The number of removed file items
     * @throws ObjectCacheException
     */
    int removeKey(String groupId, String keyId) throws ObjectCacheException;

    /**
     * Removing all file items with the given group-key ids
     *
     * @param groupId
     * @param keyId
     * @return The number of removed file items
     * @throws ObjectCacheException
     */
    int removeKeys(String groupId, String[] keyIds) throws ObjectCacheException;

    /**
     * Removing all file items with the given group
     *
     * @param groupId
     * @return The number of removed file items
     * @throws ObjectCacheException
     */
    int removeGroup(String groupId) throws ObjectCacheException;

    // -------------------------------------------------------------------------

    /**
     * Acquiring read access to a single file item.
     * The  file item must exist, otherwise a {@link ObjectCacheException}
     * is thrown.</br>
     * The {@link IReadAccess} interface inherits from {@link Closeable},
     * so that care has to be taken to properly call close when finished with
     * accessing the file item in order to ensure correct behaviour and to prevent memory leaks.
     *
     * @param cacheObject The identifier of the file, created via {@link IObjectCache#implCreateFileItem(String, String, String)}
     * @return The {@link IReadAccess} interface to access the physical file item.
     * @throws ObjectCacheException
     */
    IReadAccess getReadAccess(ICacheObject cacheObject) throws ObjectCacheException;

    /**
     * Convenience method for {@link IObjectCache#getReadAccess(ICacheObject)}
     * to save a prior call to {@link IObjectCache#implCreateFileItem(String, String, String)}</br>
     * The {@link IReadAccess} interface inherits from {@link Closeable},
     * so that care has to be taken to properly call close when finished with
     * accessing the file item in order to ensure correct behaviour and to prevent memory leaks.
     *
     * @param groupId
     * @param keyId
     * @param fileId
     * @param accessOption The list of {@link AccessOption}s
     * @return The {@link IReadAccess} interface to access the physical file item.
     * @throws ObjectCacheException
     */
    IReadAccess getReadAccess(String groupId, String keyId, String fileId) throws ObjectCacheException;

    /**
     * Acquiring write access to a single file item.
     * If the  file item does not yet exist, it will be created.</br>
     * The {@link IWriteAccess} interface inherits from {@link Closeable},
     * so that care has to be taken to properly call close when finished with
     * accessing the file item in order to ensure correct behaviour and to prevent memory leaks.
     *
     * @param cacheObject The identifier of the file, created via {@link IObjectCache#implCreateFileItem(String, String, String)}
     * @return The {@link IWriteAccess} interface to access the physical file item.
     * @throws ObjectCacheException
     */
    IWriteAccess getWriteAccess(ICacheObject cacheObject) throws ObjectCacheException;

    /**
     * Convenience method for {@link IObjectCache#getWriteAccess(ICacheObject)}
     * to save a prior call to {@link IObjectCache#implCreateFileItem(String, String, String)}</br>
     * The {@link IWriteAccess} interface inherits from {@link Closeable},
     * so that care has to be taken to properly call close when finished with
     * accessing the file item in order to ensure correct behaviour and to prevent memory leaks.
     *
     * @param groupId
     * @param keyId
     * @param fileId
     * @return The {@link IWriteAccess} interface to access the physical file item.
     * @throws ObjectCacheException
     */
    IWriteAccess getWriteAccess(String groupId, String keyId, String fileId) throws ObjectCacheException;
}
