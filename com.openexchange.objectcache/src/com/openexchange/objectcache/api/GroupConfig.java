/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.objectcache.api;

import java.util.LinkedHashSet;
import java.util.Set;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.objectcache.impl.ObjectCacheUtils;

/**
 * {@link Config}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
/**
 * {@link GroupConfig}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.4
 */
public class GroupConfig {

    /**
     * KEY_CREATEDATE
     */
    final public static String KEY_CREATEDATE = "CreateDate";

    /**
     * KEY_MODIFICATIONDATE
     */
    final public static String KEY_MODIFICATIONDATE = "ModificationDate";

    /**
     * KEY_LENGTH
     */
    final public static String KEY_LENGTH = "Length";

    /**
     * DEFAULT_KEYS
     */
    final public static String[] DEFAULT_KEYS = {
        GroupConfig.KEY_CREATEDATE,
        GroupConfig.KEY_MODIFICATIONDATE,
        GroupConfig.KEY_LENGTH
    };

    /**
     * JSON_GROUPID
     */
    final public static String JSON_GROUPID = "groupId";

    /**
     * JSON_KEYS
     */
    final public static String JSON_KEYS = "keys";

    /**
     * JSON_CUSTOM_KEYS
     */
    final public static String JSON_CUSTOM_KEYS = "customKeys";

    /**
     * JSON_MAX_GROUP_SIZE
     */
    final public static String JSON_MAX_GROUP_SIZE = "maxGroupSize";

    /**
     * JSON_MAX_KEY_COUNT
     */
    final public static String JSON_MAX_KEY_COUNT = "maxKeyCount";

    /**
     * JSON_KEY_TIMEOUT_MILLIS
     */
    final public static String JSON_KEY_TIMEOUT_MILLIS = "keyTimeout";

    /**
     * JSON_CLEANUP_PERIOD_MILLIS
     */
    final public static String JSON_CLEANUP_PERIOD_MILLIS = "cleanupPeriod";

    /**
     * JSON_CLEANUP_THRESHOLD_UPPER_PERCENTAGE
     */
    final public static String JSON_CLEANUP_THRESHOLD_UPPER_PERCENTAGE = "cleanupThresholdUpper";

    /**
     * JSON_CLEANUP_THRESHOLD_LOWER_PERCENTAGE
     */
    final public static String JSON_CLEANUP_THRESHOLD_LOWER_PERCENTAGE = "cleanupThresholdLower";

    /**
     * JSON_USER_DATA
     */
    final public static String JSON_USER_DATA = "userData";

    /**
     * JSON_USE_COUNTTABLE
     */
    final public static String JSON_USE_COUNTTABLE = "useCountTable";

    /**
     * {@link Builder}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.0.0
     */
    public static class Builder {

        /**
         * Initializes a new {@link Builder}.
         */
        public Builder() {
            super();

            // The standard keys are always predefined and cannot be overridden
            m_keys.add(KEY_CREATEDATE);
            m_keys.add(KEY_MODIFICATIONDATE);
            m_keys.add(KEY_LENGTH);

            // predefined to true;
            // needs to be set to false, it count tables are not available
            m_useCountTable = true;
        }

        /**
         * @return The configured {@link Config}
         */
        public GroupConfig build() {
            final GroupConfig config = new GroupConfig();

            config.m_groupId = m_groupId;
            config.m_keys = m_keys;
            config.m_customKeys = m_customKeys;
            config.m_maxGroupSize = m_maxGroupSize;
            config.m_maxKeyCount = m_maxKeyCount;
            config.m_keyTimeoutMillis = m_keyTimeoutMillis;
            config.m_cleanupPeriodMillis = m_cleanupPeriodMillis;
            config.m_cleanupThresholdUpperPercentage = m_cleanupThresholdUpperPercentage;
            config.m_cleanupThresholdLowerPercentage = m_cleanupThresholdLowerPercentage;
            config.m_jsonUserData = m_jsonUserData;
            config.m_useCountTable = m_useCountTable;

            return config;
        }

        /**
         * @param config
         * @return
         */
        public Builder withConfig(final GroupConfig config) {
            if (null != config) {
                m_groupId = config.m_groupId;
                m_customKeys.addAll(config.m_customKeys);
                m_maxGroupSize = config.m_maxGroupSize;
                m_maxKeyCount = config.m_maxKeyCount;
                m_keyTimeoutMillis = config.m_keyTimeoutMillis;
                m_cleanupPeriodMillis = config.m_cleanupPeriodMillis;
                m_cleanupThresholdUpperPercentage = config.m_cleanupThresholdUpperPercentage;
                m_cleanupThresholdLowerPercentage = config.m_cleanupThresholdLowerPercentage;

                for (final String curKey : config.m_jsonUserData.keySet()) {
                    final Object curObject = config.m_jsonUserData.opt(curKey);

                    if (null != curObject) {
                        try {
                            m_jsonUserData.put(curKey, curObject);
                        } catch (JSONException e) {
                            ObjectCacheUtils.logExcp(e);
                        }
                    }
                }

                m_useCountTable = config.m_useCountTable;
            }

            return this;
        }

        /**
         * @param jsonObject
         * @return
         */
        public Builder withJSON(final JSONObject jsonObject) {
            if (null != jsonObject) {
                if (jsonObject.has(JSON_GROUPID)) {
                    final String groupId = jsonObject.optString(JSON_GROUPID);

                    if (StringUtils.isNotBlank(groupId)) {
                        m_groupId = groupId;
                    }
                }

                if (jsonObject.has(JSON_CUSTOM_KEYS)) {
                    final JSONArray jsonArray = jsonObject.optJSONArray(JSON_CUSTOM_KEYS);

                    if (null != jsonArray) {
                        jsonArray.forEach((curCustomKey) -> m_customKeys.add(curCustomKey.toString()));
                    }
                }

                if (jsonObject.has(JSON_MAX_GROUP_SIZE)) {
                    m_maxGroupSize = jsonObject.optLong(JSON_MAX_GROUP_SIZE);
                }

                if (jsonObject.has(JSON_MAX_KEY_COUNT)) {
                    m_maxKeyCount = jsonObject.optLong(JSON_MAX_KEY_COUNT);
                }

                if (jsonObject.has(JSON_KEY_TIMEOUT_MILLIS)) {
                    m_keyTimeoutMillis = jsonObject.optLong(JSON_KEY_TIMEOUT_MILLIS);
                }

                if (jsonObject.has(JSON_CLEANUP_PERIOD_MILLIS)) {
                    m_cleanupPeriodMillis = jsonObject.optLong(JSON_CLEANUP_PERIOD_MILLIS);
                }

                if (jsonObject.has(JSON_CLEANUP_THRESHOLD_UPPER_PERCENTAGE)) {
                    m_cleanupThresholdUpperPercentage = jsonObject.optInt(JSON_CLEANUP_THRESHOLD_UPPER_PERCENTAGE, 100);
                    m_cleanupThresholdLowerPercentage = Math.min(m_cleanupThresholdLowerPercentage, m_cleanupThresholdUpperPercentage);
                }

                if (jsonObject.has(JSON_CLEANUP_THRESHOLD_LOWER_PERCENTAGE)) {
                    m_cleanupThresholdLowerPercentage = jsonObject.optInt(JSON_CLEANUP_THRESHOLD_LOWER_PERCENTAGE, 100);
                    m_cleanupThresholdUpperPercentage = Math.max(m_cleanupThresholdLowerPercentage, m_cleanupThresholdUpperPercentage);
                }

                if (jsonObject.has(JSON_USER_DATA)) {
                    final Object object = jsonObject.opt(JSON_USER_DATA);

                    if (object instanceof JSONObject) {
                        final JSONObject jsonUserData = (JSONObject) object;

                        for (final String curKey : jsonUserData.keySet()) {
                            final Object curObject = jsonUserData.opt(curKey);

                            if (null != curObject) {
                                try {
                                    m_jsonUserData.put(curKey, curObject);
                                } catch (JSONException e) {
                                    ObjectCacheUtils.logExcp(e);
                                }
                            }
                        }
                    }
                }

                if (jsonObject.has(JSON_USE_COUNTTABLE)) {
                    m_useCountTable = jsonObject.optBoolean(JSON_USE_COUNTTABLE);
                }
            }

            return this;
        }

        /**
         * @param groupId
         * @return
         */
        public Builder withGroupId(final String groupId) {
            m_groupId = groupId;
            return this;
        }

        /**
         * @param customKeys
         * @return
         */
        public Builder withCustomKeys(final String[] customKeys) {
            if (ArrayUtils.isNotEmpty(customKeys)) {
                for (final String curCustomKey : customKeys) {
                    if (StringUtils.isNotBlank(curCustomKey) && !m_keys.contains(curCustomKey)) {
                        m_customKeys.add(curCustomKey);
                    }
                }
            }

            return this;
        }

        /**
         * @param maxGroupSize
         * @return
         */
        public Builder withMaxGroupSize(final long maxGroupSize) {
            m_maxGroupSize = Math.max(-1L, maxGroupSize);
            return this;
        }

        /**
         * @param maxKeyCount
         * @return
         */
        public Builder withMaxKeyCount(final long maxKeyCount) {
            m_maxKeyCount = Math.max(-1L, maxKeyCount);
            return this;
        }

        /**
         * @param keyTimeoutMillis
         * @return
         */
        public Builder withKeyTimeoutMillis(final long keyTimeoutMillis) {
            m_keyTimeoutMillis = Math.max(-1L, keyTimeoutMillis);
            return this;
        }

        /**
         * @param cleanupPeriodMillis
         * @return
         */
        public Builder withCleanupPeriodMillis(final long cleanupPeriodMillis) {
            m_cleanupPeriodMillis = Math.max(1000, cleanupPeriodMillis);
            return this;
        }

        /**
         * @param cleanupThresholdUpperPercentage
         * @return
         */
        public Builder withCleanupThresholdUpperPercentage(final int cleanupThresholdUpperPercentage) {
            m_cleanupThresholdUpperPercentage = Math.min(Math.max(cleanupThresholdUpperPercentage, 0), 100);
            m_cleanupThresholdLowerPercentage = Math.min(m_cleanupThresholdLowerPercentage, m_cleanupThresholdUpperPercentage);

            return this;
        }

        /**
         * @param cleanupThresholdLowerPercentage
         * @return
         */
        public Builder withCleanupThresholdLowerPercentage(final int cleanupThresholdLowerPercentage) {
            m_cleanupThresholdLowerPercentage = Math.min(Math.max(cleanupThresholdLowerPercentage, 0), 100);
            m_cleanupThresholdUpperPercentage = Math.max(m_cleanupThresholdLowerPercentage, m_cleanupThresholdUpperPercentage);

            return this;
        }

        /**
         * @param jsonUserData
         * @return
         */
        public Builder withUserData(final JSONObject jsonUserData) {
            if (null != jsonUserData) {
                for (final String curKey : jsonUserData.keySet()) {
                    final Object curObject = jsonUserData.opt(curKey);

                    if (null != curObject) {
                        try {
                            m_jsonUserData.put(curKey, curObject);
                        } catch (JSONException e) {
                            ObjectCacheUtils.logExcp(e);
                        }
                    }
                }
            }

            return this;
        }

        /**
         * @param useCountTable
         * @return
         */
        public Builder withUseCountTable(final boolean useCountTable) {
            m_useCountTable = useCountTable;
            return this;
        }

        // - Members -------------------------------------------------------

        // required to be set
        private String m_groupId = StringUtils.EMPTY;

        // predefined in Ctor and not to be changed
        final private Set<String> m_keys = new LinkedHashSet<>();

        // optional
        private Set<String> m_customKeys = new LinkedHashSet<>();

        // no upper limit
        private long m_maxGroupSize = -1;

        // no upper limit
        private long m_maxKeyCount = 250000;

        // 30 days default
        private long m_keyTimeoutMillis = 30L * 24L * 60L * 60L * 1000L;

        // 5 minutes default, minimum is 1s
        private long m_cleanupPeriodMillis = 5L * 60L * 1000L;

        // 100% as default for immediate removal during clenup at maxKeyCount
        private int m_cleanupThresholdUpperPercentage = 100;

        // 100% as default for immediate removal stop during clenaup
        private int m_cleanupThresholdLowerPercentage = 100;

        private JSONObject m_jsonUserData = new JSONObject();

        // predefind to 'true'
        private boolean m_useCountTable = true;
}


    /**
     * Initializes a new {@link Config}.
     */
    private GroupConfig() {
        super();
    }

    /**
     * @return
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * @return <code>true</code> if all necessary config values are set, <code>false</code> otherwise
     */
    public boolean isValid() {
        return StringUtils.isNotBlank(m_groupId);
    }

    /**
     * @return
     */
    public JSONObject toJSON() {
        final JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.
                put(JSON_GROUPID, m_groupId).
                put(JSON_KEYS, m_keys).
                put(JSON_CUSTOM_KEYS, m_customKeys).
                put(JSON_MAX_GROUP_SIZE, m_maxGroupSize).
                put(JSON_MAX_KEY_COUNT, m_maxKeyCount).
                put(JSON_KEY_TIMEOUT_MILLIS, m_keyTimeoutMillis).
                put(JSON_CLEANUP_PERIOD_MILLIS, m_cleanupPeriodMillis).
                put(JSON_CLEANUP_THRESHOLD_UPPER_PERCENTAGE, m_cleanupThresholdUpperPercentage).
                put(JSON_CLEANUP_THRESHOLD_LOWER_PERCENTAGE, m_cleanupThresholdLowerPercentage).
                put(JSON_USER_DATA, m_jsonUserData).
                put(JSON_USE_COUNTTABLE, m_useCountTable);
        } catch (JSONException e) {
            ObjectCacheUtils.logExcp(e);
        }

        return jsonObject;
    }

    /**
     * @return
     */
    public String getGroupId() {
        return m_groupId;
    }

    /**
     * @return
     */
    final public Set<String> getKeys() {
        return m_keys;
    }

    /**
     * @return
     */
    public Set<String> getCustomKeys() {
        return m_customKeys;
    }

    /**
     * @return
     */
    public long getMaxGroupSize() {
        return m_maxGroupSize;
    }

    /**
     * @return
     */
    public long getMaxKeyCount() {
        return m_maxKeyCount;
    }

    /**
     * @return
     */
    public long getKeyTimeoutMillis() {
        return m_keyTimeoutMillis;
    }

    /**
     * @return
     */
    public long getCleanupPeriodMillis() {
        return m_cleanupPeriodMillis;
    }

    /**
     * @return
     */
    public long getCleanupThresholdUpperPercentage() {
        return m_cleanupThresholdUpperPercentage;
    }

    /**
     * @return
     */
    public long getCleanupThresholdLowerPercentage() {
        return m_cleanupThresholdLowerPercentage;
    }

    /**
     * @return
     */
    public JSONObject getUserData() {
        return m_jsonUserData;
    }

    /**
     * @return
     */
    public boolean isUseCountTable() {
        return m_useCountTable;
    }

    // - Members ---------------------------------------------------------------

    private String m_groupId;

    private Set<String> m_keys;

    private Set<String> m_customKeys;

    private long m_maxGroupSize;

    private long m_maxKeyCount;

    private long m_keyTimeoutMillis;

    private long m_cleanupPeriodMillis;

    private int m_cleanupThresholdUpperPercentage;

    private int m_cleanupThresholdLowerPercentage;

    private JSONObject m_jsonUserData;

    private transient boolean m_useCountTable;
}
