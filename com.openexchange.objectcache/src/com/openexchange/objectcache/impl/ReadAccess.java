/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.objectcache.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.NoSuchFileException;
import java.util.NoSuchElementException;
import org.apache.commons.io.FileUtils;
import com.openexchange.objectcache.api.IReadAccess;
import com.openexchange.objectcache.api.IdLocker;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.ObjectCacheException;

/**
 * {@link FileAccess}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class ReadAccess extends BaseAccess implements IReadAccess {

    /**
     * Initializes a new {@link ReadAccess}.
     * @param cacheObject
     * @param cacheObject
     * @throws ObjectCacheException
     */
    protected ReadAccess(@NonNull ObjectCache objectCache,
        @NonNull final ObjectCacheDatabase database,
        @NonNull CacheObject cacheObject) {

        super(objectCache, database, cacheObject);
    }

    /* (non-Javadoc)
     * @see java.io.Closeable#close()
     */
    @Override
    public void close() throws IOException {
        try {
            FileUtils.deleteQuietly(m_fileContentInputFile);
            m_fileContentInputFile = null;
        } finally {
            // unlock access time lock (from open to close), that was
            // previously acquired in ReadAccess#open method
            IdLocker.unlock(m_cacheObjectKey);
        }
    }

    // - IReadAccess -------------------------------------------------------

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IFileItemReadAccess#getInputStream()
     */
    @Override
    public InputStream getInputStream() throws IOException {
        return (null != m_fileContentInputFile) ?
            FileUtils.openInputStream(m_fileContentInputFile) :
                null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IFileItemReadAccess#getInputFile()
     */
    @Override
    public File getInputFile() throws IOException {
        return m_fileContentInputFile;
    }

    // - Implementation --------------------------------------------------------

    /**
     * @return
     * @throws ObjectCacheException
     * @throws NoSuchElementException
     * @throws IOException
     */
    @Override
    public void open() throws IOException {
        // Global lock for the lifetime of this access object => will finally be unlocked in #close call
        // As such, an open call must be followed by a close call in order to ensure correct unlocking
        IdLocker.lock(m_cacheObjectKey);

        try {
            if (!tryOpen()) {
                throw new NoSuchFileException("CacheObject is not available: " + m_cacheObject.toString());
            }

            m_cacheObjectProperties.reset();

            // acquire CacheObject access
            if (null != (m_fileContentInputFile = ObjectCacheUtils.createTempFile())) {
                m_objectCache.getObjectStoreObject(m_cacheObject, m_fileContentInputFile);
            }
        } catch (Exception e) {
            IdLocker.unlock(m_cacheObjectKey);
            throw new IOException("Reading ObjectStore file not possible although file item is valid: " + m_cacheObject.toString(), e);
        }
    }

    /**
     * @return
     */
    protected boolean isValid() {
        return (null != m_cacheObjectProperties);
    }

    /**
     * @return
     */
    protected CacheObject getFileItem() {
        return m_cacheObject;
    }

    // - Members ---------------------------------------------------------------

    protected File m_fileContentInputFile = null;
}
