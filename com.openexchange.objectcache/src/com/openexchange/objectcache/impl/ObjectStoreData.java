/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.objectcache.impl;

import java.io.Serializable;

/**
 * {@link ObjectStoreData}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class ObjectStoreData implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1181641698407844928L;

    /**
     * Initializes a new {@link ObjectStoreData}.
     */
    public ObjectStoreData(final int objectStoreNumber, final String objectStoreId) {
        super();

        m_objectStorageNumber = objectStoreNumber;
        m_objectStorageId = objectStoreId;
    }

    @Override
    public String toString() {
        return new StringBuilder(256).
            append("ObjectStoreData").
            append(" [").
            append("objectStorageNumber: ").append(m_objectStorageNumber).
            append(" , objectStorageId: ").append(m_objectStorageId).
            append(']').toString();
    }

    /**
     * @return
     */
    boolean isValid() {
        return (ObjectCacheUtils.isValid(m_objectStorageId) && (m_objectStorageNumber > 0));
    }

    /**
     * @return
     */
    boolean isInvalid() {
        return (ObjectCacheUtils.isInvalid(m_objectStorageId) || (m_objectStorageNumber < 1));
    }

    /**
     * @return
     */
    int getObjectStoreNumber() {
        return m_objectStorageNumber;
    }

    /**
     * @return
     */
    String getObjectStoreId() {
        return m_objectStorageId;
    }

    // - Members ---------------------------------------------------------------

    private int m_objectStorageNumber = -1;

    private String m_objectStorageId = null;
}
