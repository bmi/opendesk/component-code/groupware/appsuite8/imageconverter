/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.objectcache.impl;

import static com.openexchange.objectcache.impl.ObjectCache.LOG;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.apache.commons.io.FileUtils;
import com.google.common.base.Throwables;
import com.openexchange.objectcache.api.IWriteAccess;
import com.openexchange.objectcache.api.IdLocker;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.ObjectCacheException;


/**
 * {@link WriteAccess}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class WriteAccess extends BaseAccess implements IWriteAccess  {

    /**
     * Initializes a new {@link WriteAccess}.
     * @param cacheObject
     * @param cacheObject
     * @throws ObjectCacheException
     */
    protected WriteAccess(@NonNull ObjectCache objectCache,
        @NonNull final ObjectCacheDatabase database,
        @NonNull final CacheObject cacheObject) {

        super(objectCache, database, cacheObject);
    }

    /* (non-Javadoc)
     * @see java.io.Closeable#close()
     */
    @Override
    public void close() throws IOException {
        try {
            final ObjectStoreData newObjectStoreData = (null != m_cacheObjectOutputFile) ?
                m_objectCache.createNewObjectStoreObject(m_cacheObject, m_cacheObjectOutputFile) :
                    null;

            if (null != newObjectStoreData) {
                m_cacheObjectProperties.setLength(m_cacheObjectOutputFile.length());
                m_cacheObjectProperties.setModificationDateMillis(m_cacheObjectOutputFile.lastModified());

                if (m_updateExistingObject) {
                    // remove old fileItem entry as well as old FileStore file in update
                    // case first since we cannot add new entries with same PRIMARY keys twice
                    final ObjectStoreData oldObjectStoreData = m_cacheObject.getOjectStoreData();

                    try {
                        // delete old entry in case of an update
                        final List<ObjectStoreData> deletedObjectStoreDataList = new ArrayList<>();
                        m_database.deleteByObjectStoreData(m_cacheObject.getGroupId(), oldObjectStoreData, deletedObjectStoreDataList);
                    } catch (ObjectCacheException e) {
                        LOG.warn("Unable to remove old FileStoreData from database after updating fileItem: {}", Throwables.getRootCause(e).getMessage());
                        throw new IOException("Error while updating database entry", e);
                    } finally {
                        try {
                            ObjectCacheUtils.deleteObjectStoreObject(m_objectCache, oldObjectStoreData);
                        } catch (ObjectCacheException e) {
                            LOG.warn("Unable to remove FileStore file after error occured: {}", Throwables.getRootCause(e).getMessage());
                            // ok, there's no more, we can do
                        }
                    }
                }

                m_cacheObject.setObjectStoreData(newObjectStoreData);

                try {
                    m_database.createEntry(m_cacheObject, newObjectStoreData, m_cacheObjectProperties);
                } catch (ObjectCacheException e) {
                    try {
                        // delete new ObjectStore object in case of an database error
                        ObjectCacheUtils.deleteObjectStoreObject(m_objectCache, newObjectStoreData);
                    } catch (ObjectCacheException e1) {
                        LOG.warn("Unable to remove FileStore file after error occured: {}", Throwables.getRootCause(e1).getMessage());
                        // ok, there's no more, we can do
                    } finally {
                        // reset FileStoreData in case of failure in every case
                        m_cacheObject.setObjectStoreData(null);
                    }

                    throw new IOException("Error while creating database entry", e);
                }
            }
        } finally {
            // get rid of temp. output file
            FileUtils.deleteQuietly(m_cacheObjectOutputFile);
            m_cacheObjectOutputFile = null;

            // unlock access time lock (from open to close), that was
            // previously acquired in WriteAccess#open method
            IdLocker.unlock(m_cacheObjectKey);
        }
    }

    // - IWriteAccess ------------------------------------------------------

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IObjectCacheWriteAccess#getOutputStream()
     */
    @Override
    public OutputStream getOutputStream() throws IOException {
        return (null != m_cacheObjectOutputFile) ?
            FileUtils.openOutputStream(m_cacheObjectOutputFile) :
                null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IObjectCacheWriteAccess#getOutputFile()
     */
    @Override
    public File getOutputFile() throws IOException {
        return m_cacheObjectOutputFile;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IObjectCacheWriteAccess#setKeyValue(java.lang.String, java.lang.String)
     */
    @Override
    public void setKeyValue(final String key, final String value) {
        if (null != m_cacheObjectProperties) {
            m_cacheObjectProperties.setCustomKeyValue(key, value);
        }
    }

    // - Implementation --------------------------------------------------------

    /**
     * @return
     * @throws ObjectCacheException
     * @throws NoSuchElementException
     * @throws IOException
     */
    @Override
    public  void open() throws IOException {
        // Global lock for the lifetime of this access object => will finally be unlocked in #close call
        // As such, an open call must be followed by a close call in order to ensure correct unlocking
        IdLocker.lock(m_cacheObjectKey);

        // create output file/stream objects to work with
        m_cacheObjectOutputFile = ObjectCacheUtils.createTempFile();

        if (!tryOpen()) {
            m_updateExistingObject = false;
            m_cacheObjectProperties = new CacheObjectProperties(m_database.getCustomPropertyKeys(m_cacheObject.getGroupId()), System.currentTimeMillis());
        }
    }

    // - Members ---------------------------------------------------------------

    protected File m_cacheObjectOutputFile = null;

    protected boolean m_updateExistingObject = true;
 }
