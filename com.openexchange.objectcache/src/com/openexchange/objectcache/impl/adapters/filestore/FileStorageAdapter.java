/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.objectcache.impl.adapters.filestore;

import java.io.File;
import java.io.InputStream;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import com.openexchange.exception.OXException;
import com.openexchange.filestore.FileStorage;
import com.openexchange.objectcache.api.IObjectStore;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.ObjectCacheException;

/**
 * {@link FileStorageAdapter}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class FileStorageAdapter implements IObjectStore {

    /**
     * STR_NULL
     */
    private final static String STR_NULL = "null";

    @SuppressWarnings("unused")
    private FileStorageAdapter() {
        // Unused
    }

    /**
     * Initializes a new {@link FileStorageAdapter}.
     */
    public FileStorageAdapter(@NonNull FileStorage fileStorage, int fileStorageId) {
        super();

        m_fileStorage = fileStorage;
        m_fileStorageId = fileStorageId;
    }

    /**
     *
     */
    @Override
    public int getId() {
        return m_fileStorageId;
    }

    /**
     *
     */
    @Override
    public String createObject(File inputFile) throws ObjectCacheException {
        if ((null == inputFile) || !inputFile.canRead()) {
            throw new ObjectCacheException("OC cannot create object with invalid or unreadable input file: " +
                ((null != inputFile) ? inputFile.getAbsolutePath() : STR_NULL));

        }

        try (final InputStream inputStm = FileUtils.openInputStream(inputFile)) {
            return m_fileStorage.saveNewFile(inputStm);
        } catch (Exception e) {
            throw new ObjectCacheException(e);
        }
    }

    /**
     *
     */
    @Override
    public void updateObject(String objectId, File inputFile) throws ObjectCacheException {
        if ((null == inputFile) || !inputFile.canRead()) {
            throw new ObjectCacheException("OC cannot update object with invalid or unreadable input file: " +
                ((null != inputFile) ? inputFile.getAbsolutePath() : STR_NULL));

        }

        try (final InputStream inputStm = FileUtils.openInputStream(inputFile)) {
            m_fileStorage.setFileLength(0, objectId);
            m_fileStorage.appendToFile(inputStm, objectId, 0);
        } catch (Exception e) {
            throw new ObjectCacheException(e);
        }
    }

    /**
    *
    */
    @Override
    public void getObject(String objectId, File outputFile) throws ObjectCacheException {
        if (null == outputFile) {
            throw new ObjectCacheException("OC cannot write object to invalid output file: " + STR_NULL);
        }

        try (final InputStream inputStm = m_fileStorage.getFile(objectId)) {
            FileUtils.copyInputStreamToFile(inputStm, outputFile);
        } catch (Exception e) {
            throw new ObjectCacheException(e);
        }
    }

    /**
     *
     */
    @Override
    public void deleteObject(String objectId) throws ObjectCacheException {
        try {
            m_fileStorage.deleteFile(objectId);
        } catch (OXException e) {
            throw new ObjectCacheException(e);
        }
    }

    /**
     *
     */
    @Override
    public Set<String> deleteObjects(String[] objectIds) throws ObjectCacheException {
        try {
            return m_fileStorage.deleteFiles(objectIds);
        } catch (OXException e) {
            throw new ObjectCacheException(e);
        }
    }

    // - Members ---------------------------------------------------------------

    private FileStorage m_fileStorage;

    private int m_fileStorageId;
}
