/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.objectcache.impl.adapters.s3;

import static com.openexchange.objectcache.impl.ObjectCache.LOG;
import java.io.File;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.objectcache.api.IObjectStore;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.ObjectCacheException;
import io.minio.BucketExistsArgs;
import io.minio.DownloadObjectArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.RemoveObjectArgs;
import io.minio.RemoveObjectsArgs;
import io.minio.Result;
import io.minio.UploadObjectArgs;
import io.minio.messages.DeleteError;
import io.minio.messages.DeleteObject;

/**
 * {@link S3Adapter}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class S3Adapter implements IObjectStore {

    /**
     * S3STORE_PREFIX_NUMBER
     */
    final private static int S3STORE_PREFIX_NUMBER = 8351;

    /**
     * DELIMITER
     */
    final private static String DELIMITER = "/";

    /**
     * Unused
     */
    @SuppressWarnings("unused")
    private S3Adapter() {
        super();

        m_client = null;
        m_config = null;
        m_storeId = 0;
    }

    /**
     * Initializes a new {@link S3Adapter}.
     * @param config
     * @param storeId
     * @throws ObjectCacheException
     */
    public S3Adapter(@NonNull final S3AdapterConfig config, final int storeId) throws ObjectCacheException {
        super();

        if (null == config) {
            throw new ObjectCacheException("OC given Config must not be empty");
        }

        if ((storeId < 1) || (Integer.toString(storeId).length() > 5)) {
            throw new ObjectCacheException("OC given S3 store id must be a number between 1 and 99999");
        }

        if (StringUtils.isBlank(config.getEndpoint())) {
            throw new ObjectCacheException("OC given S3 store endpoint in Config must not be empty");
        }

        if (StringUtils.isBlank(config.getRegion())) {
            throw new ObjectCacheException("OC given S3 store region in Config must not be empty");
        }

        final String bucketName = config.getBucketName();

        if (StringUtils.isBlank(bucketName)) {
            throw new ObjectCacheException("OC given S3 store bucket name in Config must not be empty");
        }

        m_config = config;
        m_storeId = Integer.parseInt(("" + S3STORE_PREFIX_NUMBER) + storeId);

        try {
            m_client = MinioClient.builder().
                endpoint(config.getEndpoint()).
                region(config.getRegion()).
                credentials(config.getAccessKey(), config.getSecretKey()).build();

            if (null == m_client) {
                throw new ObjectCacheException("OC S3 store client could not be initialized");
            }

            final BucketExistsArgs bucketExistsArgs = BucketExistsArgs.builder().
                bucket(bucketName).build();

            if (!m_client.bucketExists(bucketExistsArgs)) {
                m_client.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());

                if (!m_client.bucketExists(bucketExistsArgs)) {
                    throw new ObjectCacheException("OC S3 store client is not able to create bucket: " + bucketName);
                }
            }

            if (LOG.isInfoEnabled()) {
                LOG.info("OC S3 object store successfully initialized [endpoint: {}, bucket: {}]", config.getEndpoint(), bucketName);
            }
        } catch (final Throwable e) {
            ExceptionUtils.handleThrowable(e);
            throw new ObjectCacheException(e);
        }
    }

    /**
     *
     */
    @Override
    public int getId() {
        return m_storeId;
    }

    /**
     *
     */
    @Override
    public String createObject(File inputFile) throws ObjectCacheException {
        if (null == inputFile) {
            throw new ObjectCacheException("OC input file for object to create must not be null");
        }

        final String objectId = StringUtils.remove(UUID.randomUUID().toString(), '-');

        updateObject(objectId, inputFile);

        return objectId;
    }

    /**
     *
     */
    @Override
    public void updateObject(String objectId, File inputFile) throws ObjectCacheException {
        if (null == objectId) {
            throw new ObjectCacheException("OC object Id for object to update must not be null");
        }

        if (null == inputFile) {
            throw new ObjectCacheException("OC input file for object to update must not be null");
        }

        try {
            final UploadObjectArgs uploadObjectArgs = UploadObjectArgs.builder().
                bucket(m_config.getBucketName()).
                object(objectId).
                filename(inputFile.getAbsolutePath()).build();

            m_client.uploadObject(uploadObjectArgs);
        } catch (final Exception e) {
            throw new ObjectCacheException(e);
        }
    }

    /**
     *
     */
    @Override
    public void getObject(String objectId, File outputFile) throws ObjectCacheException {
        if (null == objectId) {
            throw new ObjectCacheException("OC object id for object to read must not be null");
        }

        if (null == outputFile) {
            throw new ObjectCacheException("OC object content output file for object to read must not be null and writable");
        }

        final DownloadObjectArgs downloadObjectArgs = DownloadObjectArgs.builder().
            bucket(m_config.getBucketName()).
            object(objectId).
            filename(outputFile.getPath()).build();

        try {
            m_client.downloadObject(downloadObjectArgs);
        } catch (final Exception e) {
            throw new ObjectCacheException(e);
        }
    }

    /**
     *
     */
    @Override
    public void deleteObject(String objectId) throws ObjectCacheException {
        if (null == objectId) {
            throw new ObjectCacheException("OC object id for object to delete must not be null");
        }

        final RemoveObjectArgs removeObjectArgs = RemoveObjectArgs.builder().
            bucket(m_config.getBucketName()).
            object(objectId).build();

        try {
            m_client.removeObject(removeObjectArgs);
        } catch (final Exception e) {
            throw new ObjectCacheException(e);
        }
    }

    /**
     *
     */
    @Override
    public Set<String> deleteObjects(String[] objectIds) throws ObjectCacheException {
        if (ArrayUtils.isEmpty(objectIds)) {
            throw new ObjectCacheException("OC object id array for objects to delete must not be null");
        }

        final List<DeleteObject> deleteObjectsList = new LinkedList<>();

        for (final String curObjectId : objectIds) {
            deleteObjectsList.add(new DeleteObject(curObjectId));
        }

        final RemoveObjectsArgs removeObjectsArgs = RemoveObjectsArgs.builder().
            bucket(m_config.getBucketName()).
            objects(deleteObjectsList).build();

        final Set<String> leftOverObjectIdSet = new HashSet<>();
        final int initialSize = objectIds.length;
        final boolean debug = LOG.isDebugEnabled();
        int deleteErrorCount = 0;

        for (final Result<DeleteError> curDeleteResult : m_client.removeObjects(removeObjectsArgs)) {
            try {
                final DeleteError curDeleteError = curDeleteResult.get();
                leftOverObjectIdSet.add(curDeleteError.objectName());
                ++deleteErrorCount;

                if (debug) {
                    LOG.debug("OC could not delete object: {} ({})", curDeleteError.objectName(), curDeleteError.message());
                }
            } catch (final Exception e) {
                LOG.error(e.getMessage());
            }
        }

        if (debug) {
            LOG.debug("OC removed {}/{} object(s), {} object(s) could not be removed", initialSize - deleteErrorCount, initialSize, deleteErrorCount);
        }

        return leftOverObjectIdSet;
    }

    // - Members ---------------------------------------------------------------

    final private MinioClient m_client;

    final private S3AdapterConfig m_config;

    final private int m_storeId;
}
