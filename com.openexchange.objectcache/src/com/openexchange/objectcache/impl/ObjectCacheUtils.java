/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.objectcache.impl;

import static com.openexchange.objectcache.impl.ObjectCache.LOG;
import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.common.base.Throwables;
import com.openexchange.objectcache.api.IObjectStore;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.ObjectCacheException;

/**
 * {@link ObjectCacheUtils}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class ObjectCacheUtils {

    /**
     * !!! Do not change at any time as clients will check for this id !!!
     *
     * OBJECTCACHE_SERVER_ID
     */
    final public static String OBJECTCACHE_SERVER_ID = "7ec74ed2-c928-11eb-bd36-6fd14434831d";

    /**
     * FILEITEM_TEMP_DIRNAME
     */
    final private static String FILEITEM_TEMP_DIRNAME = "oxfitmp";

    /**
     * FILEITEM_DATA_DIRNAME
     */
    final private static String FILEITEM_DATA_DIRNAME = "oxfidata";

    /**
     * Initializes a new {@link ObjectCacheUtils}.
     */
    private ObjectCacheUtils() {
        super();

    }

    // - Misc ------------------------------------------------------------------

    /**
     *
     */
    public static void shutdown() {
        if (IS_TERMINATED.compareAndSet(false, true)) {
            FileUtils.deleteQuietly(TEMP_DIR);
        }
    }

    /**
     * @param spoolDir
     */
    public static void setSpoolPath(final File spoolDir) {
       if (null != spoolDir) {
           TEMP_DIR = implCreateDirectory(spoolDir, FILEITEM_TEMP_DIRNAME, true);
           DATA_DIR = implCreateDirectory(spoolDir, FILEITEM_DATA_DIRNAME, false);
       }
    }

    /**
     * @return
     */
    public static File getDataDir() {
        return DATA_DIR;
    }

    /**
     * @param baseDirectory
     * @param directoryName
     * @param clean
     * @return
     */
    private static File implCreateDirectory(@NonNull final File baseDirectory, @NonNull final String directoryName, boolean clean) {
        File targetDirectory = new File(baseDirectory, directoryName);

        // use or create given spool directory, use /tmp as fallback parent directory
        if ((targetDirectory.exists() && targetDirectory.canWrite() && targetDirectory.isDirectory()) ||
            targetDirectory.mkdirs() || (targetDirectory = new File("/tmp", directoryName)).mkdirs()) {

            if (clean) {
                try {
                    FileUtils.cleanDirectory(targetDirectory);
                } catch (IOException e) {
                    logExcp(e);
                }
            }
        }

        return targetDirectory;
    }

    /**
     * @param id
     * @return
     */
    public static boolean isValid(final String id) {
        return isNotEmpty(id);
    }

    /**
     * @param id
     * @return
     */
    public static boolean isValid(final String[] ids) {
        return isNotEmpty(ids);
    }

    /**
     * @param id
     * @return
     */
    public static boolean isInvalid(final String id) {
        return StringUtils.isEmpty(id);
    }

    /**
     * @param id
     * @return
     */
    public static boolean isInvalid(final String[] ids) {
        return isEmpty(ids);
    }

    /**
     * @param cacheObject
     * @return
     */
    public static boolean isValid(final CacheObject cacheObject) {
        return ((null != cacheObject) && cacheObject.isValid());
    }

    /**
     * @param cacheObject
     * @return
     */
    public static boolean isInvalid(final CacheObject cacheObject) {
        return ((null == cacheObject) || cacheObject.isInvalid());
    }

    /**
     * @param objectStoreData
     * @return
     */
    public static boolean isValid(final ObjectStoreData objectStoreData) {
        return ((null != objectStoreData) && objectStoreData.isValid());
    }

    /**
     * @param objectStoreData
     * @return
     */
    public static boolean isValid(final ObjectStoreData[] objectStoreData) {
        return isNotEmpty(objectStoreData);
    }

    /**
     * @param objectStoreData
     * @return
     */
    public static boolean isInvalid(final ObjectStoreData objectStoreData) {
        return ((null == objectStoreData) || objectStoreData.isInvalid());
    }

    /**
     * @param objectStoreData
     * @return
     */
    public static boolean isInvalid(final ObjectStoreData[] objectStoreData) {
        return isEmpty(objectStoreData);
    }

    /**
     * @param list
     * @param T
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] listToArray(@NonNull final List<T> list, Class<?> type) {
        return list.toArray((T[]) Array.newInstance(type, list.size()));
    }

    /**
     * @return
     * @throws IOException
     */
    public static File createTempFile() throws IOException {
        final int maxAttempts = 8192;
        final File tempDir = !IS_TERMINATED.get() ? TEMP_DIR : DEFAULT_TEMP_DIR;
        final StringBuilder tempFilePrefixBuilder = new StringBuilder(256);

        for (int i = 0; i < maxAttempts; ++i) {
            tempFilePrefixBuilder.setLength(0);
            tempFilePrefixBuilder.
                append("oxfi-").
                append(TEMP_COUNTER.incrementAndGet()).append('-');

            final File tmpFile = File.createTempFile(tempFilePrefixBuilder.toString(), ".tmp", tempDir);

            if ((null != tmpFile) && tmpFile.exists()) {
                return tmpFile;
            }

            Thread.yield();
        }

        return null;
    }

    /**
     * @param startString
     * @param aliasPrefix
     * @param queryProperties
     * @param end
     * @return
     */
    public static String createSqlWithPropertyVariables(@NonNull final String startString,
        @NonNull Properties properties, @NonNull String combineString, final String... appendStrings) {

        final Set<Object> keySet = properties.keySet();
        final StringBuilder sql = new StringBuilder(256 + startString.length()).append(startString);
        boolean following = false;

        for(final Object curKey : keySet) {
            if (isNotEmpty(curKey.toString())) {
                if (following) {
                    sql.append(combineString);
                } else {
                    following = true;
                }

                sql.append(' ').append(curKey).append("=?");
            }
        }

        if (null != appendStrings) {
            for (final String curStr : appendStrings) {
                if (isNotEmpty(curStr)) {
                    sql.append(' ').append(curStr);
                }
            }
        }

        return sql.toString();
    }

    /**
     * @param stmt
     * @param values
     * @param startIndex
     * @return The updated index after setting all given values
     * @throws SQLException
     */
    public static int setStatementValues(@NonNull final PreparedStatement stmt, final Object[] values, final int startIndex) throws SQLException {
        int curIndex = startIndex;

        if (isNotEmpty(values) && (startIndex > 0)) {
            for(final Object curValue : values) {
                if (null != curValue) {
                    if (curValue instanceof String) {
                        stmt.setString(curIndex++, curValue.toString());
                    } else if (curValue instanceof Long) {
                        stmt.setLong(curIndex++, ((Long) curValue).longValue());
                    } else if (curValue instanceof Integer) {
                        stmt.setInt(curIndex++, ((Integer) curValue).intValue());
                    } else if (curValue instanceof String) {
                        stmt.setString(curIndex++, curValue.toString());
                    } else if (curValue instanceof Double) {
                        stmt.setDouble(curIndex++, ((Double) curValue).doubleValue());
                    } else if (curValue instanceof Float) {
                        stmt.setFloat(curIndex++, ((Float) curValue).floatValue());
                    } else if (curValue instanceof Boolean) {
                        stmt.setBoolean(curIndex++, ((Boolean) curValue).booleanValue());
                    } else  {
                        stmt.setString(curIndex++, curValue.toString());
                    }
                } else {
                    stmt.setNull(curIndex++, Types.NULL);
                }
            }
        }

        return curIndex;
    }

    /**
     * @param objectStoreData
     * @throws IOException
     */
    public static void deleteObjectStoreObject(@NonNull ObjectCache objectCache, @NonNull final ObjectStoreData objectStoreData) throws ObjectCacheException {
        if (isValid(objectStoreData)) {
            try {
                final IObjectStore objectStore = objectCache.getObjectStore(objectStoreData);

                if (null != objectStore) {
                    objectStore.deleteObject(objectStoreData.getObjectStoreId());
                }
            } catch (Exception e) {
                throw new ObjectCacheException("Delete of FileStore file failed: " + objectStoreData.toString(), e);
            }
        }
    }

    /**
     * @param fileStoreDatas
     * @return
     * @throws ObjectCacheException
     */
    public static int deleteObjectStoreObjects(@NonNull ObjectCache objectCache, @NonNull final List<ObjectStoreData> objectStoreDataList) throws ObjectCacheException {
        ObjectCacheException rethrowable = null;
        int ret = 0;

        final LinkedList<ObjectStoreData> objectStoreDataWorkList = new LinkedList<>();
        final List<String> workList = new ArrayList<>(objectStoreDataList.size());

        objectStoreDataWorkList.addAll(objectStoreDataList);

        while (!objectStoreDataWorkList.isEmpty()) {
            final ObjectStoreData firstData = objectStoreDataWorkList.get(0);

            if (null != firstData) {
                final int curObjectStoreNumber = firstData.getObjectStoreNumber();

                workList.clear();

                // collect all items with current fileStore number in worklist and remove from source list
                for (final ListIterator<ObjectStoreData> iter = objectStoreDataWorkList.listIterator(); iter.hasNext();) {
                    final ObjectStoreData curData = iter.next();

                    if (curData.getObjectStoreNumber() == curObjectStoreNumber) {
                        workList.add(curData.getObjectStoreId());
                        iter.remove();
                    }
                }

                // remove collected files and iuncrease delete count
                final int workListSize = workList.size();

                if (workListSize > 0) {
                    try {
                        final IObjectStore objectStore = objectCache.getObjectStore(firstData);

                        final Set<String> leftOverObjects = (null != objectStore) ?
                            objectStore.deleteObjects(workList.toArray(new String[workListSize])) :
                                new HashSet<>();

                        ret += (workListSize - leftOverObjects.size());

                        if (!leftOverObjects.isEmpty()) {
                            LOG.warn("ObjectStore was not able to delete {} files", leftOverObjects.size());
                        }
                    } catch (Exception e) {
                        rethrowable = new ObjectCacheException("Error while removing multiple files from ObjectStore: " + Throwables.getRootCause(e).getMessage());
                    }
                }
            } else {
                objectStoreDataWorkList.remove();
            }
        }

        if (null != rethrowable) {
            throw rethrowable;
        }

        return ret;
    }

    /**
     * @param sleepTimeMillis
     */
    public static void sleepThread(final long sleepTimeMillis) {
        try {
            Thread.sleep(sleepTimeMillis);
        } catch (@SuppressWarnings("unused") InterruptedException e) {
            // ok
        }
    }

    /**
     * @param closeable
     */
    public static void close(final Closeable closeable) {
        if (null != closeable) {
            try {
                closeable.close();
            } catch (IOException e) {
                logExcp(e);
            }
        }
    }

    // - Logging ---------------------------------------------------------------

    /**
     * @param e
     */
    public static void logExcp(Throwable e) {
        if (LOG.isErrorEnabled()) {
            String msg = null;

            if (null != e) {
                final Throwable cause = Throwables.getRootCause(e);

                msg = (null != cause) ? cause.getMessage() : null;

                if (null != msg) {
                    if (msg.length() > ERROR_MESSAGE_TOTAL_LENGTH) {
                        final int beginLength = ERROR_MESSAGE_TOTAL_LENGTH - ERROR_MESSAGE_END_LENGTH - ERROR_MESSAGE_FILL_STRING.length();
                        final int endLength = Math.min(msg.length() - beginLength, ERROR_MESSAGE_END_LENGTH);
                        msg = msg.substring(0, beginLength) + ((endLength > 0) ? ("..." + msg.substring(msg.length() - endLength)) : StringUtils.EMPTY);
                    }
                } else {
                    msg = ((null != cause) ? cause : e).getClass().getName();
                }
            } else {
                msg = DEF_EXCEPTION_MSG;
            }

            LOG.error(msg);
        }
    }

    // - Statics ---------------------------------------------------------------

    final private static AtomicBoolean IS_TERMINATED = new AtomicBoolean(false);

    final private static  String DEF_EXCEPTION_MSG = "Unspecified ObjectCache exception occured";

    final private static  String ERROR_MESSAGE_FILL_STRING = "...";

    final private static int ERROR_MESSAGE_TOTAL_LENGTH = 2048;

    final private static int ERROR_MESSAGE_END_LENGTH = 256;

    final private static File DEFAULT_TEMP_DIR = new File("/tmp");

    final private static AtomicLong TEMP_COUNTER = new AtomicLong(0);

    private static File TEMP_DIR = new File("/tmp");

    private static File DATA_DIR = TEMP_DIR;
}
