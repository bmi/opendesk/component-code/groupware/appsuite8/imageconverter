/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.objectcache.impl;

import java.util.Properties;
import com.openexchange.objectcache.api.NonNull;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.pool.HikariPool;

/**
 * {@link HikariSetup}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
class HikariSetup {

    final public static int DEFAULT_POOL_MAXSIZE = 30;
    final public static long DEFAULT_POOL_CONNECTTIMEOUTMILLIS = 10000;
    final public static long DEFAULT_POOL_IDLETIMEOUTMILLIS = 300000;
    final public static long DEFAULT_POOL_MAX_LIFETIMEMILLIS = 600000;

    /**
     * Initializes a new {@link HikariSetup}.
     */
    @SuppressWarnings("unused")
    private HikariSetup() {
        super();
    }

    /**
     * Initializes a new {@link HikariSetup}.
     */
    HikariSetup(@NonNull final String driverClassName,
                @NonNull final String jdbcUrl,
                @NonNull final String userName,
                @NonNull final String password,
                @NonNull final Properties dbProperties,
                final int poolMaxSize,
                final long poolConnectTimeoutMillis,
                final long poolIdleTimeoutMillis,
                final long poolMaxLifetime) {
        super();

        m_driverClassName = driverClassName;
        m_jdbcUrl = jdbcUrl;
        m_username = userName;
        m_password = password;
        m_dbProperties = dbProperties;

        m_poolMaxSize = poolMaxSize;
        m_poolConnectTimeoutMillis = poolConnectTimeoutMillis;
        m_poolIdleTimeoutMillis = poolIdleTimeoutMillis;
        m_poolMaxLifetimeMillis = poolMaxLifetime;
    }

    /**
     * @param readOnly
     * @return
     */
    HikariPool getConnectionPool(final boolean readOnly) {
        final HikariConfig poolConfig = new HikariConfig();

        // DB related Hikari properties
        poolConfig.setDriverClassName(m_driverClassName);
        poolConfig.setJdbcUrl(m_jdbcUrl);
        poolConfig.setUsername(m_username);
        poolConfig.setPassword(m_password);
        poolConfig.setAutoCommit(false);
        poolConfig.setReadOnly(readOnly);

        // common DB properties
        for (final String curPropKey : m_dbProperties.stringPropertyNames()) {
            poolConfig.addDataSourceProperty(curPropKey, m_dbProperties.getProperty(curPropKey));
        }

        // Hikari pool properties
        poolConfig.setMaximumPoolSize(m_poolMaxSize);
        poolConfig.setIdleTimeout(m_poolIdleTimeoutMillis);
        poolConfig.setConnectionTimeout(m_poolConnectTimeoutMillis);
        poolConfig.setMaxLifetime(m_poolMaxLifetimeMillis);

        return new HikariPool(poolConfig);
    }

    // - Members ---------------------------------------------------------------

    private String m_driverClassName = null;

    private String m_jdbcUrl = null;

    private String m_username = null;

    private String m_password = null;

    private Properties m_dbProperties = null;

    private int m_poolMaxSize = DEFAULT_POOL_MAXSIZE;

    private long m_poolConnectTimeoutMillis = DEFAULT_POOL_CONNECTTIMEOUTMILLIS;

    private long m_poolIdleTimeoutMillis = DEFAULT_POOL_IDLETIMEOUTMILLIS;

    private long m_poolMaxLifetimeMillis = DEFAULT_POOL_MAX_LIFETIMEMILLIS;
}
