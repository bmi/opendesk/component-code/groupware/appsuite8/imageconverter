/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.objectcache.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.Enumeration;
import liquibase.resource.ResourceAccessor;

/**
 * {@link LiquibaseResourceAccessor}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class LiquibaseResourceAccessor implements ResourceAccessor {

    /**
     * Initializes a new {@link ImplLiquibaseResourceAccessor}.
     */
    public LiquibaseResourceAccessor() {
        super();
    }

    /*
     * (non-Javadoc)
     *
     * @see liquibase.resource.ResourceAccessor#getResourceAsStream(java.lang.String)
     */
    @Override
    public InputStream getResourceAsStream(String file) throws IOException {
        ClassLoader loader = toClassLoader();

        final URL url = ((null != loader) ? loader : ObjectCacheDatabase.class.getClassLoader()).getResource(file);

        if (null != url) {
            try {
                final URLConnection con = url.openConnection();

                if (null != con) {
                    con.connect();

                    try (final InputStream resourceInputStm = con.getInputStream()) {
                        if (null != resourceInputStm) {
                            return new ByteArrayInputStream(org.apache.commons.io.IOUtils.toByteArray(resourceInputStm));
                        }
                    }
                }
            } catch (Exception e) {
                ObjectCacheUtils.logExcp(e);
            }
        }

        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see liquibase.resource.ResourceAccessor#getResources(java.lang.String)
     */
    @Override
    public Enumeration<URL> getResources(String packageName) throws IOException {
        return Collections.emptyEnumeration();
    }

    /*
     * (non-Javadoc)
     *
     * @see liquibase.resource.ResourceAccessor#toClassLoader()
     */
    @Override
    public ClassLoader toClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }
}
