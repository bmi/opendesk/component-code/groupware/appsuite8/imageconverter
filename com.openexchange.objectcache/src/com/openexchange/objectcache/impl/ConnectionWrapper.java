/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.objectcache.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * {@link ConnectionWrapper}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class ConnectionWrapper implements AutoCloseable {

    /**
     * {@link ConnectionClosedHandler}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.3
     */
    public interface ConnectionClosedHandler {
        void closed(Connection con);
    }

    /**
     * Initializes a new {@link ConnectionWrapper}.
     * @param connection
     */
    ConnectionWrapper(final Connection connection) {
        m_connection = connection;
    }

    /**
     * Initializes a new {@link ConnectionWrapper}.
     * @param connection
     */
    ConnectionWrapper(final Connection connection, final ConnectionClosedHandler connectionClosedHandler) {
        m_connection = connection;
        m_connectionClosedHandler = connectionClosedHandler;
    }

    /**
     *
     */
    @Override
    public void close() throws Exception {
        if (null != m_connection) {
            m_connection.close();

            synchronized (this) {
                if (null != m_connectionClosedHandler) {
                    m_connectionClosedHandler.closed(m_connection);
                }
            }
        }
    }

    // Public API --------------------------------------------------------------

    /**
     * setConnectionClosedHandler
     *
     * @param connectionClosedHandler
     */
    void setConnectionClosedHandler(final ConnectionClosedHandler connectionClosedHandler) {
        synchronized (this) {
            m_connectionClosedHandler = connectionClosedHandler;
        }
    }

    /**
     * isValid
     *
     * @return
     */
    public boolean isValid() {
        return (null != m_connection);
    }

    /**
     * getConnection
     *
     * @return
     */
    public Connection getConnection() {
        return m_connection;
    }

    /**
     * prepareStatement
     *
     * @param sql
     * @return
     * @throws SQLException
     */
    public PreparedStatement prepareStatement(final String sql) throws SQLException {
        return (null != m_connection) ?
            m_connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY) :
                null;
    }

    /**
     * createStatement
     *
     * @return
     * @throws SQLException
     */
    public Statement createStatement() throws SQLException {
        return (null != m_connection) ?
            m_connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY) :
                null;
    }

    /**
     * commit
     *
     * @throws SQLException
     */
    public void commit() throws SQLException {
        if (null != m_connection) {
            m_connection.commit();
        }
    }

    // - Members ---------------------------------------------------------------

    private final Connection m_connection;

    private ConnectionClosedHandler m_connectionClosedHandler = null;
}
