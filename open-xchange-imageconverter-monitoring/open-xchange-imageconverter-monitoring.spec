
Name:          open-xchange-imageconverter-monitoring
BuildArch:     noarch
BuildRequires: java-1.8.0-openjdk-devel
BuildRequires: ant
Version:       @OXVERSION@
%define        ox_release 3
Release:       %{ox_release}_<CI_CNT>.<B_CNT>
Group:         Applications/Productivity
License:       AGPLv3+
BuildRoot:     %{_tmppath}/%{name}-%{version}-build
URL:           http://www.open-xchange.com/
Source:        %{name}_%{version}.orig.tar.bz2
Summary:       Open-Xchange ImageConverter Munin scripts
Requires:      open-xchange-imageconverter-server >= @OXVERSION@
Requires:      munin-node, perl-JSON, perl-libwww-perl

%description
Munin is a highly flexible and powerful solution used to create graphs of
virtually everything imaginable throughout your network, while still
maintaining a rattling ease of installation and configuration.

This package contains Open-Xchange ImageConverter plugins for the Munin node.

Munin is written in Perl, and relies heavily on Tobi Oetiker's excellent
RRDtool. To see a real example of Munin in action, you can follow a link
from <http://munin.projects.linpro.no/> to a live installation.

Authors:
--------
    Open-Xchange


%prep
%setup -q

%build

%install
export NO_BRP_CHECK_BYTECODE_VERSION=true
ant -lib build/lib -Dbasedir=build -DdestDir=%{buildroot} -DpackageName=%{name} -f build/build.xml clean build

%post
TMPFILE=`mktemp /tmp/munin-node.configure.XXXXXXXXXX`
munin-node-configure --libdir /usr/share/munin/plugins/ --shell > $TMPFILE || :
if [ -f $TMPFILE ] ; then
  sh < $TMPFILE
  rm -f $TMPFILE
fi
/etc/init.d/munin-node restart || :

exit 0

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%dir /usr/share/munin
/usr/share/munin/plugins/

%changelog
* Thu Nov 28 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate of 7.10.3 release
* Thu Nov 21 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate of 7.10.3 release
* Thu Oct 17 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.10.3 release
* Wed Jun 19 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.3 release
* Fri May 10 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate of 7.10.2 release
* Thu May 02 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview of 7.10.2 release
* Thu Mar 28 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.10.2 release
* Mon Mar 11 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.2
* Fri Nov 23 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
RC 1 for 7.10.1 release
* Fri Nov 02 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview for 7.10.1 release
* Thu Oct 11 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview for 7.10.1 release
* Mon Sep 10 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.1
* Mon Jun 25 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.10.0 release
* Mon Jun 11 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.10.0 release
* Fri May 18 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Sixth preview of 7.10.0 release
* Thu Apr 19 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth preview of 7.10.0 release
* Tue Apr 03 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Third preview of 7.10.0 release
* Tue Feb 20 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview of 7.10.0 release
* Fri Feb 02 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.10.0 release
* Mon Oct 16 2017 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.0 release
