/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.server.osgi;

import java.util.concurrent.Executors;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.eclipsesource.jaxrs.publisher.ApplicationConfiguration;
import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.html.HtmlService;
import com.openexchange.imageconverter.api.IImageConverter;
import com.openexchange.imageconverter.api.ImageConverterException;
import com.openexchange.imageconverter.server.impl.ImageConverterServer;
import com.openexchange.osgi.HousekeepingActivator;

//=============================================================================
public class ImageConverterServerActivator extends HousekeepingActivator {
    public ImageConverterServerActivator() {
        super();
    }

    //-------------------------------------------------------------------------

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] {
            ConfigurationService.class,
            IImageConverter.class,
            HtmlService.class
        };
    }

    //-------------------------------------------------------------------------

    @Override
    protected void startBundle() throws Exception {
        LOG.info("starting bundle: {}", SERVICE_NAME);

        try {
            final ConfigurationService configService = getService(ConfigurationService.class);
            final IImageConverter imageConverter = getService(IImageConverter.class);

            if (null == imageConverter) {
                throw new ImageConverterException("Could not get ImageConverter service");
            }

            if (null == configService) {
                throw new ImageConverterException("Could not get Configuration service");
            }

            // track ApplicationConfiguration to register
            m_applicationConfigurationTracker = new ServiceTracker<>(context, ApplicationConfiguration.class, new ServiceTrackerCustomizer<ApplicationConfiguration, ApplicationConfiguration>() {

                @Override
                public ApplicationConfiguration addingService(ServiceReference<ApplicationConfiguration> reference) {
                    final ApplicationConfiguration monitor = (null != reference) ? context.getService(reference) : null;

                    if (monitor != null) {
                        final int maxRequestCount = Math.max(1, configService.getIntProperty("com.openexchange.imageconverter.queueLength", 512));

                        Executors.newSingleThreadExecutor().execute(() -> {
                            registerService(ImageConverterServer.class,
                                m_imageConverterServer = new ImageConverterServer(self, imageConverter, maxRequestCount));
                            LOG.info(SERVICE_NAME + " registered REST service");
                        });

                        context.ungetService(reference);
                    }

                    return null;
                }

                @Override
                public void modifiedService(ServiceReference<ApplicationConfiguration> reference, ApplicationConfiguration service) {
                    // Ok
                }

                @Override
                public void removedService(ServiceReference<ApplicationConfiguration> reference, ApplicationConfiguration service) {
                    // Ok
                }
            });

            m_applicationConfigurationTracker.open();
            openTrackers();

            LOG.info("successfully started bundle: {}", SERVICE_NAME);
        } catch (Throwable e) {
            ExceptionUtils.handleThrowable(e);
            LOG.error("... starting bundle: {}", SERVICE_NAME + " failed", e);
            throw new RuntimeException(e);
        }
    }

    //-------------------------------------------------------------------------

    @Override
    protected void stopBundle() throws Exception {
        LOG.info("stopping bundle: {}", SERVICE_NAME);

        closeTrackers();
        unregisterServices();

        if (null != m_imageConverterServer) {
            m_imageConverterServer.shutdown();
            m_imageConverterServer = null;
        }

        LOG.info("successfully stopped bundle: {}", SERVICE_NAME);
    }

    // - Static Members --------------------------------------------------------

    private static final String SERVICE_NAME = "Open-Xchange Image Server";

    protected final Logger LOG = LoggerFactory.getLogger(ImageConverterServerActivator.class);

    // - Members ---------------------------------------------------------------

    protected ImageConverterServerActivator self = this;

    private ImageConverterServer m_imageConverterServer = null;

    private ServiceTracker<ApplicationConfiguration, ApplicationConfiguration> m_applicationConfigurationTracker;
}
