/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.server.impl;

import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;
import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.glassfish.jersey.media.multipart.BodyPart;
import org.glassfish.jersey.media.multipart.BodyPartEntity;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Throwables;
import com.openexchange.imageconverter.api.IImageConverter;
import com.openexchange.imageconverter.api.IImageConverterMonitoring;
import com.openexchange.imageconverter.api.IMetadata;
import com.openexchange.imageconverter.api.ImageConverterException;
import com.openexchange.imageconverter.api.ImageFormat;
import com.openexchange.imageconverter.api.ImageServerUtil;
import com.openexchange.imageconverter.api.MetadataImage;
import com.openexchange.imageconverter.impl.ImageConverter;
import com.openexchange.imageconverter.impl.ImageConverterMBean;
import com.openexchange.imageconverter.impl.ImageConverterMonitoring.RequestType;
import com.openexchange.imageconverter.impl.ImageConverterUtils;
import com.openexchange.imageconverter.impl.ImageProcessor;
import com.openexchange.imageconverter.impl.Mutable;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.Nullable;
import com.openexchange.rest.services.annotation.Role;
import com.openexchange.rest.services.annotation.RoleAllowed;
import com.openexchange.server.ServiceLookup;

/**
 * {@link ImageConverterServer}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.4
 */
@Path("/imageconverter")
@Produces("application/json")
public class ImageConverterServer {

    final public static String OX_IMAGECONVERTER_SERVER_REMOTE_API_VERSION = "1";

    final private static JSONObject EMPTY_JSON_OBJECT = new JSONObject();

    final private static int HTTP_CODE_BAD_REQUEST = 400;

    final private static int HTTP_CODE_TOO_MANY_REQUESTS = 429;

    /**
     * Initializes a new {@link ImageConverterServer}.
     * @param serviceLookup
     * @param imageConverter
     * @param maxRequestCount
     */
    public ImageConverterServer(@NonNull final ServiceLookup serviceLookup, @NonNull final IImageConverter imageConverter, final int maxRequestCount) {
        super();

        m_imageConverter = imageConverter;
        m_maxRequestCount = maxRequestCount;
    }

    /**
     * @param metrics
     * @return
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    @PermitAll
    public Response root(@QueryParam("metrics") @DefaultValue("false") String metrics) {
        LOG.trace("IC WebService received #/ request");

        try {
            if (m_statusRequestCount.incrementAndGet() > m_maxRequestCount) {
                implRequestCountLimitReached(RequestType.ADMIN);
                return Response.status(HTTP_CODE_TOO_MANY_REQUESTS).build();
            }

            return Response.ok(implCreateImageConverterServerHtmlPage(m_running.get() ?
                OX_IMAGECONVERTER_SERVER_DEFAULT_RESPONSE_TEXT :
                    OX_IMAGECONVERTER_SERVER_TERMINATED_RESPONSE_TEXT,
                StringUtils.equalsIgnoreCase(metrics, "true") ? ImageConverterUtils.IC_MBEAN : null)).build();
        } finally {
            m_statusRequestCount.decrementAndGet();
        }
    }

    /**
     * @param metrics
     * @return
     */
    @GET
    @Path("/status")
    @Produces("application/json")
    @PermitAll
    public Response status(@QueryParam("metrics") @DefaultValue("false") String metrics) {
        LOG.trace("IC WebService received #status request");

        try {
            if (m_statusRequestCount.incrementAndGet() > m_maxRequestCount) {
                implRequestCountLimitReached(RequestType.ADMIN);
                return Response.status(HTTP_CODE_TOO_MANY_REQUESTS).build();
            }

            try {
                return Response.ok(implGetServerStatus(StringUtils.equalsIgnoreCase(metrics, "true"))).build();
        } catch (JSONException e) {
                LOG.warn("IC WebService JSON error in #status", e);
            }

            return Response.serverError().build();
        } finally {
            m_statusRequestCount.decrementAndGet();
        }
    }

    // - IImageConverter -------------------------------------------------------

    /**
     * @param imageKey
     * @param requestFormat
     * @param requestContext
     * @return
     */
    @GET
    @Path("/getImage/{imageKey}/{requestFormat}")
    @Produces(MediaType.MULTIPART_FORM_DATA)
    @PermitAll
    public Response getImage(
        @PathParam("imageKey") String imageKey,
        @PathParam("requestFormat") String requestFormat,
        @QueryParam("context") @DefaultValue("") String requestContext) {

        LOG.trace("IC WebService received #getImage request: {} / {}", imageKey, requestFormat);

        try {
            // rate limit check
            if (m_functionalRequestCount.incrementAndGet() > m_maxRequestCount) {
                implRequestCountLimitReached(RequestType.GET);
                return Response.status(HTTP_CODE_TOO_MANY_REQUESTS).build();
            }

            // parameter validation
            final String key = implGetValidatedKey(imageKey);
            final String format = implGetValidatedFormat(requestFormat);
            final String context = implGetValidatedContext(requestContext);

            if ((null == key) || (null == format) || (null == context)) {
                return Response.status(HTTP_CODE_BAD_REQUEST).build();
            }

            // Processing
            ResponseBuilder responseBuilder = null;

            if (m_running.get()) {
                try (final InputStream inputStm = m_imageConverter.getImage(key, format, context)) {
                    responseBuilder = implCreateFileResponse(inputStm);
                } catch (Exception e) {
                    responseBuilder = implCreateExceptionResponse(e);
                    LOG.warn("IC WebService error in #getImage", e);
                }
            } else {
                responseBuilder = implCreateImageConverterErrorResponse("ImageConverter server terminated!");
            }

            return responseBuilder.build();
        } finally {
            m_functionalRequestCount.decrementAndGet();
        }
    }

    /**
     * @param imageKey
     * @param requestContext
     * @return
     */
    @GET
    @Path("/getMetadata/{imageKey}")
    @Produces(MediaType.MULTIPART_FORM_DATA)
    @PermitAll
    public Response getMetadata(
        @PathParam("imageKey") String imageKey,
        @QueryParam("context") @DefaultValue("") String requestContext) {

        LOG.trace("IC WebService received #getMetadata request: {}", imageKey);

        try {
            // rate limit check
            if (m_functionalRequestCount.incrementAndGet() > m_maxRequestCount) {
                implRequestCountLimitReached(RequestType.GET);
                return Response.status(HTTP_CODE_TOO_MANY_REQUESTS).build();
            }

            // parameter validation
            final String key = implGetValidatedKey(imageKey);
            final String context = implGetValidatedContext(requestContext);

            if ((null == key) || (null == context)) {
                return Response.status(HTTP_CODE_BAD_REQUEST).build();
            }

            // Processing
            ResponseBuilder responseBuilder = null;

            if (m_running.get()) {
                try {
                    final IMetadata metadata = m_imageConverter.getMetadata(key, context);

                    try (final MetadataImage metadataImage = new MetadataImage(null, metadata)) {
                        responseBuilder = implCreateMetadataImageResponse(metadataImage);
                    }
                } catch (Exception e) {
                    responseBuilder = implCreateExceptionResponse(e);
                    LOG.warn("IC WebService error in #getMetadata", e);
                }
            } else {
                responseBuilder = implCreateImageConverterErrorResponse("ImageConverter server terminated!");
            }

            return responseBuilder.build();
        } finally {
            m_functionalRequestCount.decrementAndGet();
        }
    }

    /**
     * @param imageKey
     * @param requestFormat
     * @param requestContext
     * @return
     */
    @GET
    @Path("/getImageAndMetadata/{imageKey}/{requestFormat}")
    @Produces(MediaType.MULTIPART_FORM_DATA)
    @PermitAll
    public Response getImageAndMetadata(
        @PathParam("imageKey") String imageKey,
        @PathParam("requestFormat") String requestFormat,
        @QueryParam("context") @DefaultValue("") String requestContext) {

        LOG.trace("IC WebService received #getImageAndMetadata request: {} / {}", imageKey, requestFormat);

        try {
            // rate limit check
            if (m_functionalRequestCount.incrementAndGet() > m_maxRequestCount) {
                implRequestCountLimitReached(RequestType.GET);
                return Response.status(HTTP_CODE_TOO_MANY_REQUESTS).build();
            }

            // parameter validation
            final String key = implGetValidatedKey(imageKey);
            final String format = implGetValidatedFormat(requestFormat);
            final String context = implGetValidatedContext(requestContext);

            if ((null == key) || (null == format) || (null == context)) {
                return Response.status(HTTP_CODE_BAD_REQUEST).build();
            }

            // Processing
            ResponseBuilder responseBuilder = null;

            if (m_running.get()) {
                try (final MetadataImage metadataImage = m_imageConverter.getImageAndMetadata(key, format, context)) {
                    responseBuilder = implCreateMetadataImageResponse(metadataImage);
                } catch (Exception e) {
                    responseBuilder = implCreateExceptionResponse(e);
                    LOG.warn("IC WebService error in #getImageAndMetadata", e);
                }
            } else {
                responseBuilder = implCreateImageConverterErrorResponse("ImageConverter server terminated!");
            }

            return responseBuilder.build();
        } finally {
            m_functionalRequestCount.decrementAndGet();
        }
    }

    /**
     * @param multiPart
     * @param imageKey
     * @param requestContext
     * @return
     */
    @PUT
    @Path("/cacheImage/{imageKey}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @PermitAll
    public Response cacheImage(
        MultiPart multiPart,
        @PathParam("imageKey") String imageKey,
        @QueryParam("context") @DefaultValue("") String requestContext) {

        LOG.trace("IC WebService received #cacheImage request: {}", imageKey);

        try {
            // rate limit check
            if (m_functionalRequestCount.incrementAndGet() > m_maxRequestCount) {
                implRequestCountLimitReached(RequestType.CACHE);
                return Response.status(HTTP_CODE_TOO_MANY_REQUESTS).build();
            }

            // parameter validation
            final String key = implGetValidatedKey(imageKey);
            final String context = implGetValidatedContext(requestContext);

            if ((null == key) || (null == context)) {
                return Response.status(HTTP_CODE_BAD_REQUEST).build();
            }

            ResponseBuilder responseBuilder = null;

            if (m_running.get()) {
                try (final InputStream inputStm = implGetInputStream(multiPart)) {
                    if (null != inputStm) {
                        m_imageConverter.cacheImage(key, inputStm, context);
                        responseBuilder = implCreateSuccessResponse();
                    } else {
                        throw new IllegalArgumentException("IC #cacheImage server input file is not available");
                    }
                } catch (Exception e) {
                    responseBuilder = implCreateExceptionResponse(e);
                    LOG.warn("IC WebService error in #cacheImage", e);
                }
            } else {
                responseBuilder = implCreateImageConverterErrorResponse("ImageConverter server terminated!");
            }

            return responseBuilder.build();
        } finally {
            m_functionalRequestCount.decrementAndGet();
        }
    }

    /**
     * @param multiPart
     * @param imageKey
     * @param requestFormat
     * @param requestContext
     * @return
     */
    @POST
    @Path("cacheAndGetImage/{imageKey}/{requestFormat}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.MULTIPART_FORM_DATA)
    @PermitAll
    public Response cacheAndGetImage(MultiPart multiPart,
        @PathParam("imageKey") String imageKey,
        @PathParam("requestFormat") String requestFormat,
        @QueryParam("context") @DefaultValue("") String requestContext) {

        LOG.trace("IC WebService received #cacheAndGetImage request: {} / {}", imageKey, requestFormat);

        try {
            // rate limit check
            if (m_functionalRequestCount.incrementAndGet() > m_maxRequestCount) {
                implRequestCountLimitReached(RequestType.CACHE_AND_GET);
                return Response.status(HTTP_CODE_TOO_MANY_REQUESTS).build();
            }

            // parameter validation
            final String key = implGetValidatedKey(imageKey);
            final String format = implGetValidatedFormat(requestFormat);
            final String context = implGetValidatedContext(requestContext);

            if ((null == key) || (null == format) || (null == context)) {
                return Response.status(HTTP_CODE_BAD_REQUEST).build();
            }

            ResponseBuilder responseBuilder = null;

            if (m_running.get()) {
                try (final InputStream inputStm = implGetInputStream(multiPart)) {
                    if (null != inputStm) {
                        final Mutable<Boolean> usedCache = new Mutable<>(Boolean.TRUE);

                        try (final InputStream resultInputStm = (m_imageConverter instanceof ImageConverter) ?
                                ((ImageConverter) m_imageConverter).cacheAndGetImage(key, format, inputStm, usedCache, context) :
                                    m_imageConverter.cacheAndGetImage(key, format, inputStm, context)) {

                            responseBuilder = implCreateFileResponse(resultInputStm);

                            if (!usedCache.get()) {
                                responseBuilder.header(HEADER_WARNING, MESSAGE_WARNING_NOCACHE);
                            }
                        }
                    } else {
                        throw new IllegalArgumentException("IC #cacheAndGetImage server input file is not available");
                    }
                } catch (Exception e) {
                    responseBuilder = implCreateExceptionResponse(e);
                    LOG.warn("IC WebService error in #cacheAndGetImage", e);
                }
            } else {
                responseBuilder = implCreateImageConverterErrorResponse("ImageConverter server terminated!");
            }

            return responseBuilder.build();
        } finally {
            m_functionalRequestCount.decrementAndGet();
        }
    }

    @POST
    @Path("cacheAndGetImageAndMetadata/{imageKey}/{requestFormat}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.MULTIPART_FORM_DATA)
    @PermitAll
    public Response cacheAndGetImageAndMetadata(MultiPart multiPart,
        @PathParam("imageKey") String imageKey,
        @PathParam("requestFormat") String requestFormat,
        @QueryParam("context") @DefaultValue("") String requestContext) {

        LOG.trace("IC WebService received #cacheAndGetImageAndMetadata request: {} / {}", imageKey, requestFormat);

        try {
            // rate limit check
            if (m_functionalRequestCount.incrementAndGet() > m_maxRequestCount) {
                implRequestCountLimitReached(RequestType.CACHE_AND_GET);
                return Response.status(HTTP_CODE_TOO_MANY_REQUESTS).build();
            }

            // parameter validation
            final String key = implGetValidatedKey(imageKey);
            final String format = implGetValidatedFormat(requestFormat);
            final String context = implGetValidatedContext(requestContext);

            if ((null == key) || (null == format) || (null == context)) {
                return Response.status(HTTP_CODE_BAD_REQUEST).build();
            }

            ResponseBuilder responseBuilder = null;

            if (m_running.get()) {
                try (final InputStream inputStm = implGetInputStream(multiPart)) {
                    if (null != inputStm) {
                        final Mutable<Boolean> usedCache = new Mutable<>(Boolean.TRUE);

                        try (final MetadataImage metadaImage = (m_imageConverter instanceof ImageConverter) ?
                            ((ImageConverter) m_imageConverter).cacheAndGetImageAndMetadata(key, format, inputStm, usedCache, context) :
                                m_imageConverter.cacheAndGetImageAndMetadata(key, format, inputStm, context)) {

                            responseBuilder = implCreateMetadataImageResponse(metadaImage);

                            if (!usedCache.get()) {
                                responseBuilder.header(HEADER_WARNING, MESSAGE_WARNING_NOCACHE);
                            }
                        }
                    } else {
                        throw new IllegalArgumentException("IC WebService input file is not available");
                    }
                } catch (Exception e) {
                    responseBuilder = implCreateExceptionResponse(e);
                    LOG.warn("IC WebService error in #cacheAndGetImageAndMetadata", e);
                }
            } else {
                responseBuilder = implCreateImageConverterErrorResponse("ImageConverter server terminated!");
            }

            return responseBuilder.build();
        } finally {
            m_functionalRequestCount.decrementAndGet();
        }
    }

    /**
     * @param requestContext
     * @return
     */
    @DELETE
    @Path("/clearImages")
    @RoleAllowed(Role.BASIC_AUTHENTICATED)
    public Response clearImages(@QueryParam("context") @DefaultValue("") String requestContext) {
        LOG.trace("IC WebService received #clearImages request");

        try {
            // rate limit check
            if (m_functionalRequestCount.incrementAndGet() > m_maxRequestCount) {
                implRequestCountLimitReached(RequestType.ADMIN);
                return Response.status(HTTP_CODE_TOO_MANY_REQUESTS).build();
            }

            // parameter validation
            final String context = implGetValidatedContext(requestContext);

            if (null == context) {
                return Response.status(HTTP_CODE_BAD_REQUEST).build();
            }

            ResponseBuilder responseBuilder = null;

            if (m_running.get()) {
                try {
                    if (isEmpty(context)) {
                        m_imageConverter.clearImages();
                    } else {
                        m_imageConverter.clearImages(context);
                    }

                    responseBuilder = implCreateSuccessResponse();
                } catch (ImageConverterException e) {
                    responseBuilder = implCreateExceptionResponse(e);
                    LOG.warn("IC WebService error in #clearImages", e);
                }
            } else {
                responseBuilder = implCreateImageConverterErrorResponse("ImageConverter server terminated!");
            }

            return responseBuilder.build();
        } finally {
            m_functionalRequestCount.decrementAndGet();
        }
    }

    /**
     * @param imageKey
     * @return
     */
    @DELETE
    @Path("/clearImagesByKey/{imageKey}")
    @RoleAllowed(Role.BASIC_AUTHENTICATED)
    public Response clearImagesByKey(@PathParam("imageKey") String imageKey) {
        LOG.trace("IC WebService received #clearImagesByKey request: {}", imageKey);

        try {
            // rate limit check
            if (m_functionalRequestCount.incrementAndGet() > m_maxRequestCount) {
                implRequestCountLimitReached(RequestType.ADMIN);
                return Response.status(HTTP_CODE_TOO_MANY_REQUESTS).build();
            }

            // parameter validation
            final String key = implGetValidatedKey(imageKey);

            if (null == key) {
                return Response.status(HTTP_CODE_BAD_REQUEST).build();
            }

            ResponseBuilder responseBuilder = null;

            if (m_running.get()) {
                try {
                    m_imageConverter.clearImagesByKey(key);
                    responseBuilder = implCreateSuccessResponse();
                } catch (ImageConverterException e) {
                    responseBuilder = implCreateExceptionResponse(e);
                    LOG.warn("IC WebService error in #clearImagesByKey", e);
                }
            } else {
                responseBuilder = implCreateImageConverterErrorResponse("ImageConverter server terminated!");
            }

            return responseBuilder.build();
        } finally {
            m_functionalRequestCount.decrementAndGet();
        }
    }

    /**
     * @param requestContext
     * @return
     */
    @GET
    @Path("/getKeyCount")
    @PermitAll
    public Response getKeyCount(@QueryParam("context") @DefaultValue("") String requestContext) {

        LOG.trace("IC WebService received #getKeyCount request");

        try {
            // rate limit check
            if (m_functionalRequestCount.incrementAndGet() > m_maxRequestCount) {
                implRequestCountLimitReached(RequestType.ADMIN);
                return Response.status(HTTP_CODE_TOO_MANY_REQUESTS).build();
            }

            // parameter validation
            final String context = implGetValidatedContext(requestContext);

            if (null == context) {
                return Response.status(HTTP_CODE_BAD_REQUEST).build();
            }

            try {
                return Response.ok(m_running.get() ?
                    new JSONObject().put("count", m_imageConverter.getKeyCount(context)) :
                        new JSONObject(implCreateJSONResponse(CODE_ERROR_TERMINATED, MESSAGE_ERROR_TERMINATED))).build();
            } catch (Exception e) {
                LOG.warn("IC WebService error in #getKeyCount", e);
            }

            return Response.serverError().build();
        } finally {
            m_functionalRequestCount.decrementAndGet();
        }
    }

    /**
     * @param requestContext
     * @return
     */
    @GET
    @Path("/getKeys")
    @RoleAllowed(Role.BASIC_AUTHENTICATED)
    public Response getKeys(@QueryParam("context") @DefaultValue("") String requestContext) {
        LOG.trace("IC WebService received #getKey request");

        try {
            // rate limit check
            if (m_functionalRequestCount.incrementAndGet() > m_maxRequestCount) {
                implRequestCountLimitReached(RequestType.ADMIN);
                return Response.status(HTTP_CODE_TOO_MANY_REQUESTS).build();
            }

            // parameter validation
            final String context = implGetValidatedContext(requestContext);

            if (null == context) {
                return Response.status(HTTP_CODE_BAD_REQUEST).build();
            }

            try {
                return Response.ok(m_running.get() ?
                    new JSONObject().put("keys", Arrays.asList(m_imageConverter.getKeys(context))) :
                        new JSONObject(implCreateJSONResponse(CODE_ERROR_TERMINATED, MESSAGE_ERROR_TERMINATED))).build();
            } catch (Exception e) {
                LOG.warn("IC WebService error in #getKeys", e);
            }

            return Response.serverError().build();
        } finally {
            m_functionalRequestCount.decrementAndGet();
        }
    }

    /**
     *
     */
    public void shutdown() {
        if (m_running.compareAndSet(true, false)) {
            LOG.trace("ImageConverter server shutdown finished");
        }
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param content
     * @param monitoring
     * @return
     */
    private static String implCreateImageConverterServerHtmlPage(
        final String[] content,
        @Nullable final IImageConverterMonitoring monitoring) {

        final StringBuilder htmlBuilder = ImageConverterUtils.STR_BUILDER();

        htmlBuilder.append("<html>").
            append("<head><meta charset=\"UTF-8\"><title>").append(OX_IMAGECONVERTER_SERVER_TITLE).append("</title></head>").
            append("<body><h1 align=\"center\">").append(OX_IMAGECONVERTER_SERVER_TITLE).append("</h1>");

        // print server Id in every case
        htmlBuilder.append("<p>Id: ").append(ImageServerUtil.IMAGE_SERVER_ID).append("</p>");

        // print API version in every case
        htmlBuilder.append("<p>API: v").append(OX_IMAGECONVERTER_SERVER_REMOTE_API_VERSION).append("</p>");

        if (isNotEmpty(content)) {
            for (int i = 0; i < content.length;) {
                htmlBuilder.append("<p>").append(content[i++]).append("</p>");
            }
        }

        if (null != monitoring) {
            htmlBuilder.append("<br />").append("<p><u>Metrics</u></p>");

            for (Method method : monitoring.getClass().getMethods())
                if (null != method) {
                    final String methodName = method.getName();

                    if (isNotEmpty(methodName) && StringUtils.startsWithIgnoreCase(methodName, "get") && (method.getParameterTypes().length == 0)) {
                        final String metricsPropertyName = methodName.substring(3);
                        final boolean isRatioMetric = metricsPropertyName.endsWith("Ratio");

                        try {
                            final String numberString = ImageConverterUtils.STR_BUILDER().
                                append(method.invoke(monitoring)).toString();

                            if (NumberUtils.isParsable(numberString)) {
                                htmlBuilder.append("<p>").append(metricsPropertyName).append(": ");

                                if (isRatioMetric) {
                                    htmlBuilder.append(Double.parseDouble(numberString));
                                } else {
                                    htmlBuilder.append(Long.parseLong(numberString));
                                }

                                htmlBuilder.append("</p>");
                            }
                        } catch (@SuppressWarnings("unused") Exception e) {
                            //
                        }
                    }
                }
        }

        return htmlBuilder.append("</body></html>").toString();
    }

    /**
     * @param includeMetrics
     * @return
     * @throws JSONException
     */
    @NonNull private JSONObject implGetServerStatus(boolean includeMetrics) throws JSONException {
        final JSONObject jsonStatus = new JSONObject();

        // general
        jsonStatus.put("name", "imageconverter");
        jsonStatus.put("id", ImageServerUtil.IMAGE_SERVER_ID);
        jsonStatus.put("api", OX_IMAGECONVERTER_SERVER_REMOTE_API_VERSION);
        jsonStatus.put("apitext", "API: v" + OX_IMAGECONVERTER_SERVER_REMOTE_API_VERSION);
        jsonStatus.put("status", m_running.get() ? "running" : "terminated");

        // engine status
        jsonStatus.put("engine", implGetEngineStatus());

        // metrics on demand
        final ImageConverterMBean imageConverterMBean = includeMetrics ? ImageConverterUtils.IC_MBEAN : null;

        if (null != imageConverterMBean) {
            jsonStatus.put("metrics", implGetMetrics(imageConverterMBean));
        }

        return jsonStatus;
    }

    /**
     * @param <ImageProcessorStatus>
     * @return
     * @throws JSONException
     */
    private <ImageProcessorStatus> JSONObject implGetEngineStatus() throws JSONException  {
        final JSONObject jsonEngineStatus = new JSONObject();

        if (m_imageConverter instanceof ImageConverter) {
            final ImageConverter imageConverter = (ImageConverter) m_imageConverter;

            // image processor
            final ImageProcessor.ImageProcessorStatus imageProcessorStatus = imageConverter.getImageProcessorStatus();
            final JSONObject jsonImageProcessorStatus = new JSONObject();

            jsonImageProcessorStatus.put("description", imageProcessorStatus.getProcessorDescription());
            jsonImageProcessorStatus.put("available", imageProcessorStatus.isAvailable());

            jsonEngineStatus.put("imageProcessor", jsonImageProcessorStatus);

            // image formats
            final List<ImageFormat> usedImageFormats = imageConverter.getImageFormats();

            if (null != usedImageFormats) {
                jsonEngineStatus.put("imageFormats", new JSONArray(usedImageFormats));
            }
        } else {
            jsonEngineStatus.put("status", "n/a");
        }

        return jsonEngineStatus;
    }

    /**
     * @param imageConverterMBean
     * @return
     * @throws JSONException
     */
    private JSONObject implGetMetrics(final ImageConverterMBean imageConverterMBean) throws JSONException  {
        final List<Method> methodList = Arrays.asList(imageConverterMBean.getClass().getMethods());
        final JSONObject jsonMetrics = new JSONObject();

        Collections.sort(methodList, (one, other) -> one.getName().compareTo(other.getName()));

        for (Method curMethod : methodList) {
            if (null != curMethod) {
                final String methodName = curMethod.getName();

                if (isNotEmpty(methodName) && StringUtils.startsWithIgnoreCase(methodName, "get") && (curMethod.getParameterTypes().length == 0)) {
                    final String metricsPropertyName = methodName.substring(3);
                    final boolean isRatioMetric = metricsPropertyName.contains("Ratio");

                    try {
                        final String numberString = new StringBuilder().append(curMethod.invoke(imageConverterMBean)).toString();

                        if (NumberUtils.isParsable(numberString)) {
                            jsonMetrics.put(metricsPropertyName, isRatioMetric ? Double.parseDouble(numberString) : Long.parseLong(numberString));
                        }
                    } catch (@SuppressWarnings("unused") Exception e) {
                        //
                    }
                }
            }
        }

        return jsonMetrics;
    }

    /**
     * @param inputStm
     * @return
     */
    private static ResponseBuilder implCreateFileResponse(final InputStream inputStm) {
        return (null != inputStm) ? Response.ok(inputStm) : Response.status(Status.NO_CONTENT);
    }

    /**
     * @param metadataImage
     * @return
     */
    private static ResponseBuilder implCreateMetadataImageResponse(final MetadataImage metadataImage) {
        ResponseBuilder ret = null;

        if (null != metadataImage) {
            final MultiPart multipart = new MultiPart(MediaType.MULTIPART_FORM_DATA_TYPE);

            try (final InputStream imageInputStm = metadataImage.getImageInputStream()) {
                if (null != imageInputStm) {
                    {
                        final BodyPart imageBodyPart = new BodyPart(MediaType.APPLICATION_OCTET_STREAM_TYPE);

                        imageBodyPart.setEntity(imageInputStm);
                        multipart.bodyPart(imageBodyPart);
                    }
                }
            } catch (IOException e) {
                ret = implCreateExceptionResponse(e);
            }

            if (null == ret) {
                final IMetadata imageMetadata = metadataImage.getMetadata();

                if (null != imageMetadata) {
                    final BodyPart metadataBodyPart = new BodyPart(MediaType.APPLICATION_JSON_TYPE);

                    metadataBodyPart.setEntity(imageMetadata.getJSONObject());
                    multipart.bodyPart(metadataBodyPart);
                }

                ret = Response.ok(multipart);
            }
        } else {
            ret = implCreateImageConverterErrorResponse(null);
        }

        return ret;
    }

    /**
     * @return
     */
    private static ResponseBuilder implCreateSuccessResponse() {
        return Response.ok(implCreateJSONResponse(CODE_SUCCESS, MESSAGE_SUCCESS));
    }

    /**
     * @param message
     * @return
     */
    private static ResponseBuilder implCreateImageConverterErrorResponse(String message) {
        return Response.
            serverError().
            type(MediaType.APPLICATION_JSON).
            entity(implCreateJSONResponse(CODE_ERROR_IMAGECONVERTER, isNotEmpty(message) ? message : MESSAGE_ERROR_IMAGECONVERTER_GENERAL));
    }

    /**
     * @param e
     * @return
     */
    private static ResponseBuilder implCreateExceptionResponse(final Exception e) {
        return Response.
            serverError().
            type(MediaType.APPLICATION_JSON).
            entity(implCreateJSONResponse(CODE_ERROR_IMAGECONVERTER_SERVER, Throwables.getRootCause(e).getMessage()));
    }

    /**
     * @param code
     * @param message
     * @return
     */
    private static String implCreateJSONResponse(@NonNull final Integer code, final String message) {
        try {
            return new JSONObject().
                put("code", code).
                put("message", isNotEmpty(message) ? message : MESSAGE_SUCCESS).toString();
        } catch (Exception e) {
            LOG.warn(Throwables.getRootCause(e).getMessage());
        }

        return EMPTY_JSON_OBJECT.toString();
    }

    /**
     * @param multiPart
     * @return
     * @throws IOException
     */
    private static InputStream implGetInputStream(@NonNull final MultiPart multiPart) throws IOException {
        for (final BodyPart part : multiPart.getBodyParts()) {
            final Object entity = part.getEntity();

            if (entity instanceof BodyPartEntity) {
                return ((BodyPartEntity) entity).getInputStream();
            } else if (entity instanceof File) {
                return new BufferedInputStream(FileUtils.openInputStream((File) entity));
            }
        }

        return null;
    }

    /**
     * @param requestType
     */
    private void implRequestCountLimitReached(@NonNull final RequestType requestType) {
        ImageConverterUtils.IC_MONITOR.incrementRequestLimitReachedCount(requestType);

        LOG.warn("IC WebService reached request count limit: {} (total limit reached count: {})",
            requestType.toString(),
            ImageConverterUtils.IC_MONITOR.getRequestLimitReachedCount(null));
    }

    /**
     * @param keyToValidate
     * @return
     */
    private @Nullable String implGetValidatedKey(@Nullable final String keyToValidate) {
        final String ret = implGetValidated(ALLOWED_KEY_PATTERN, keyToValidate);

        if (null == ret) {
            LOG.error("IC detected suspicious key request parameter '{}' not matching allowed pattern => Rejecting request", keyToValidate);
        }

        return ret;
    }

    /**
     * @param formatToValidate
     * @return
     */
    private @Nullable String implGetValidatedFormat(@Nullable final String formatToValidate) {
        final String ret = implGetValidated(ALLOWED_FORMAT_PATTERN, formatToValidate);

        if (null == ret) {
            LOG.error("IC detected suspicious format request parameter '{}' not matching allowed pattern => Rejecting request", formatToValidate);
        }

        return ret;
    }

    /**
     * @param contextToValidate
     * @return
     */
    private @Nullable String implGetValidatedContext(@Nullable final String contextToValidate) {
        final String ret = implGetValidated(ALLOWED_CONTEXT_PATTERN, contextToValidate);

        if (null == ret) {
            LOG.error("IC detected suspicious context request parameter '{}' not matching allowed pattern => Rejecting request", contextToValidate);
        }

        return ret;
    }

    /**
     * @param allowedPattern
     * @param toValidate
     * @return
     */
    private @Nullable String implGetValidated(@NonNull final Pattern allowedPattern, @Nullable final String toValidate) {
        final String validated = (null != toValidate) ? toValidate.trim() : null;

        // check if given key parameter matches allowed pattern for key
        if ((null == validated) || !allowedPattern.matcher(validated).matches()) {
            return null;
        }

        return validated;
    }

    // - Static Members --------------------------------------------------------

    final private static Logger LOG = LoggerFactory.getLogger(ImageConverterServer.class);

    final private static Pattern ALLOWED_KEY_PATTERN = Pattern.compile("^[a-zA-Z0-9_]+$");

    final private static Pattern ALLOWED_FORMAT_PATTERN = Pattern.compile("^[a-zA-Z0-9_~@#:\\-]+$");

    final private static Pattern ALLOWED_CONTEXT_PATTERN = Pattern.compile("^[a-zA-Z0-9_]*$");

    final private static Integer CODE_SUCCESS = Integer.valueOf(0);

    final private static String MESSAGE_SUCCESS = StringUtils.EMPTY;

    final private static Integer CODE_ERROR_IMAGECONVERTER_SERVER = Integer.valueOf(1);

    final private static Integer CODE_ERROR_IMAGECONVERTER = Integer.valueOf(2);

    final private static Integer CODE_ERROR_TERMINATED = Integer.valueOf(99);

    final private static String MESSAGE_ERROR_TERMINATED = "ImageConverter terminated!";

    final private static String MESSAGE_ERROR_IMAGECONVERTER_GENERAL = "ImageConverter general error";

    final private static String OX_IMAGECONVERTER_SERVER_TITLE = "OX Software GmbH Image Converter Server";

    final private static String[] OX_IMAGECONVERTER_SERVER_DEFAULT_RESPONSE_TEXT = { "Error Code: 0", " Status: running..." };

    final private static String[] OX_IMAGECONVERTER_SERVER_TERMINATED_RESPONSE_TEXT = { "Error Code: 99", " terminated!" };

    final private static String HEADER_WARNING = "Warning";

    final private static String MESSAGE_WARNING_NOCACHE = ImageConverterUtils.STR_BUILDER().
        append(199).append(' ').
        append("ImageConverter").append(' ').
        append("\"Uncached\"").toString();

    // - Members ---------------------------------------------------------------

    final private IImageConverter m_imageConverter;

    final private int m_maxRequestCount;

    final private AtomicBoolean m_running = new AtomicBoolean(true);

    final private AtomicLong m_statusRequestCount = new AtomicLong(0);

    final private AtomicLong m_functionalRequestCount = new AtomicLong(0);
}
